#!/bin/env python3

from pprint import pprint

from owslib.wfs import WebFeatureService

__URL__ = 'https://download.data.grandlyon.com/wfs/grandlyon'

wfs11 = WebFeatureService(url=__URL__, version='1.1.0')
print(wfs11.identification)
print(dir(wfs11.identification))
print(wfs11.identification.title)
print(wfs11.operations)
print(wfs11.operations)

print('--')

for operation in wfs11.operations:
    print('--')
    #print(operation)
    #print(dir(operation))
    print('| Name')
    pprint(operation.name)
    print('| Parameters')
    pprint(operation.parameters)
    #print('Require: %s' % ','.join(operation.requirements))
    print('| Constraints')
    pprint(operation.constraints)
    #print('| FormatOptions')
    #pprint(operation.formatOptions)
    #print('| Methods')
    #pprint(operation.methods)
