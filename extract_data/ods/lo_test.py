#!/bin/env python3

from ezodf import opendoc

ods = opendoc(filename='TestSheet.ods')

print(dir(ods))
print(ods.sheets)
print(dir(ods.sheets))
print(list(ods.sheets.names()))
print(ods.sheets['Feuille1'])
print(dir(ods.sheets['Feuille1']))
print(list((ods.sheets['Feuille1']).columns()))
print('--')
for column in ods.sheets['Feuille1'].columns():
    for cell in column:
        print(cell)
        print(dir(cell))
        print(cell.tail)
        print(cell.formula)
        print(cell.formula.__class__)
        print(cell.value)
