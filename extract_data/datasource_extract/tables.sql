CREATE TABLE norme (
    ID      SERIAL PRIMARY KEY,
    NAME    VARCHAR(20) NOT NULL
);

CREATE TABLE category (
    ID      SERIAL PRIMARY KEY,
    NAME    TEXT NOT NULL UNIQUE
);

CREATE TABLE keyword (
    ID      SERIAL PRIMARY KEY,
    name    TEXT NOT NULL UNIQUE
);

CREATE TABLE license (
    ID      SERIAL PRIMARY KEY,
    name    TEXT NOT NULL UNIQUE
);

CREATE TABLE responsable (
    ID      SERIAL PRIMARY KEY,
    name    TEXT NOT NULL UNIQUE
);

CREATE TABLE update_frequency (
    ID      SERIAL PRIMARY KEY,
    name    TEXT NOT NULL UNIQUE
);

CREATE TABLE date_publication (
    ID                  SERIAL PRIMARY KEY,
    date_publication    DATETIME NOT NULL
);

CREATE TABLE datasource (
    ID                      SERIAL PRIMARY KEY,
    PARENT                  INTEGER REFERENCE datasource(DATASOURCE_UUID),
    DATASOURCE_UUID         UUID NOT NULL,
    TITLE                   TEXT NOT NULL,
    DESCRIPTION             TEXT NOT NULL,
    DATA_TYPE               TEXT NOT NULL,
    PROVENANCE              TEXT NOT NULL,
    COUVERTURE_TERRORIALE   TEXT NOT NULL
);

CREATE OR REPLACE VIEW FULL_DATASOURCE AS
    SELECT
        datasource.PARENT                                   AS PARENT,
        datasource.DATASOURCE_UUID                          AS UUID,
        datasource.TITLE                                    AS TITLE,
        datasource.DESCRIPTION                              AS DESCRIPTION,
        datasource.DATA_TYPE                                AS DATA_TYPE,
        datasource.PROVENANCE                               AS PROVENANCE,
        datasource.COUVERTURE_TERRITORIALE                  AS COUVERTURE_TERRORIALE,
        date_publication.DATE_PUBLICATION                   AS DATE_PUBLICATION,
        LISTAGG(update_frequency.UPDATE_FREQUENCY, ',')     AS UPDATE_FREQUENCY,
        LISTAGG(keyword.name, ',')                          AS KEYWORD_LIST,
        category.name                                       AS CATEGORY,
        responsable.name                                    AS RESPONSABLE,
        license.name                                        AS LICENSE
    ;


CREATE TABLE KEYWORD_DATASOURCE (
    ID_KEYWORD      INTEGER NOT NULL REFERENCES keyword(ID),
    ID_DATASOURCE   INTEGER NOT NULL REFERENCES datasource(ID)
);

CREATE FUNCTION GET_INSERT_KEYWORD(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    KEYWORD%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM keyword;

    IF TORETURN IS NULL THEN
        INSERT INTO KEYWORD (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION TRG_KEYWORD_ON_INSERT()
RETURNS TRIGGER AS $BODY$
DECLARE
    SPLITTED_ARRAY  TEXT[];
    CURRROW         TEXT;
BEGIN
    SPLITTED_ARRAY = regexp_split_to_array(NEW.name, ',');

    FOR CURRROW IN (SELECT UNNEST(SPLITTED_ARRAY)) LOOP
        GET_INSERT_KEYWORD(CURRROW);
    END LOOP;

    RETURN NEW;
END;
$BODY$
LANGUAGE PLPGSQL;


CREATE FUNCTION GET_INSERT_CATEGORY(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    CATEGORY%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM category;

    IF TORETURN IS NULL THEN
        INSERT INTO CATEGORY (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;


CREATE FUNCTION GET_INSERT_LICENSE(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    LICENSE%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM license;

    IF TORETURN IS NULL THEN
        INSERT INTO LICENSE (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;


CREATE FUNCTION GET_INSERT_RESPONSABLE(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    RESPONSABLE%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM responsable;

    IF TORETURN IS NULL THEN
        INSERT INTO RESPONSABLE (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION TRG_UPDATE_FREQUENCY_ON_INSERT()
RETURNS TRIGGER AS $BODY$
DECLARE
    SPLITTED_ARRAY  TEXT[];
    CURRROW         TEXT;
BEGIN
    SPLITTED_ARRAY = regexp_split_to_array(NEW.name, ',');

    FOR CURRROW IN (SELECT UNNEST(SPLITTED_ARRAY)) LOOP
        GET_INSERT_UPDATE_FREQ(CURRROW);
    END LOOP;

    RETURN NEW;
END;
$BODY$
LANGUAGE PLPGSQL;


CREATE FUNCTION GET_INSERT_UPDATE_FREQUENCY(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    UPDATE_FREQUENCY%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM update_frequency;

    IF TORETURN IS NULL THEN
        INSERT INTO UPDATE_FREQUENCY (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;


CREATE FUNCTION GET_INSERT_NORME(m_name TEXT)
RETURNS RECORD
AS $BODY$
DECLARE
    TORETURN    NORME%ROWTYPE;
BEGIN
    SELECT id, name INTO TORETURN FROM norme;

    IF TORETURN IS NULL THEN
        INSERT INTO NORME (name) VALUES (m_name) RETURNING * INTO TORETURN;
    END IF;

    RETURN TORETURN;
END;
$BODY$ LANGUAGE PLPGSQL;
