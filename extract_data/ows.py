#!/bin/env python3

from owslib.csw import CatalogueServiceWeb

# https://download.data.grandlyon.com/catalogue/srv/fre/csw?SERVICE=CSW&request=GetCapabilities&service=CSW&version=2.0.2

__URL__ = 'https://download.data.grandlyon.com/catalogue/srv/fre/csw?SERVICE=CSW&request=GetCapabilities&service=CSW'
__VERSION__ = '2.0.2'

if __name__ == '__main__':
    csw = CatalogueServiceWeb(__URL__, version=__VERSION__)

    #print(list([dir(op) for op in csw.operations]))
    print(list([op.name for op in csw.operations]))
    print(dir(csw))
