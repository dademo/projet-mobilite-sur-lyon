#!/bin/env python3

import csv
import json
import requests

class MyException(Exception):
    pass
class AuthenticationException(MyException):
    pass
class NotFoundException(MyException):
    pass
class RequestException(MyException):
    pass


def request(url, auth = None):
    r = requests.get(url, auth)
    if r.status_code != 200:
        if r.status_code == 401:
            raise AuthenticationException('Ressource `%s` require authentication' % url)
        elif r.status_code == 404:
            raise NotFoundException('Ressource `%s` not found' % url)
        else:
            raise RequestException('Error while fetching ressource %s` (%d)' % (url, r.status_code))
    else:
        return r.text

#__URL__ = 'https://download.data.grandlyon.com/ws/grandlyon/all.json'
__URL__ = 'https://download.data.grandlyon.com/ws/rdata/all.json'

if __name__ == '__main__':
    #outfile = open('table_list.csv', 'w')
    outfile = open('table_list_rdata.csv', 'w')
    #outfile_columns = open('table_column_list.csv', 'w')
    outfile_columns = open('table_column_list_rdata.csv', 'w')
    
    spamwriter = csv.writer(outfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter_columns = csv.writer(outfile_columns, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    
    # Header
    spamwriter.writerow([
        'table_schema',
        'table_name',
        'nb_records',
        'nb_cols',
        'global_precision',
        'href'
    ])
    
    spamwriter_columns.writerow([
        'table_schema',
        'table_name',
        'column_name',
        'column_id',
        'column_type',
        'precision',
        'is_pk',
        'is_nullable',
        'comment',
        'href'
    ])
    

    table_list = json.loads(request(__URL__))
    i = 1
        
    for _row in table_list['results']:
        try:
            print('Record %d of %d' % (i, len(table_list['results'])))
            table_desc = json.loads(request(_row['href']))
            nb_records = table_desc['nb_records']
            nb_cols = table_desc['nb_results']
            global_precision = sum([elem['precision'] or 0 for elem in table_desc['results']])
            
            spamwriter.writerow([
                _row['table_schema'],
                _row['table_name'],
                nb_records,
                nb_cols,
                global_precision,
                _row['href']
            ])
        
            for column_desc_id in range(len(table_desc['results'])):
                curr_table_desc = table_desc['results'][column_desc_id]
                spamwriter_columns.writerow([
                    _row['table_schema'],
                    _row['table_name'],
                    curr_table_desc['column_name'],
                    column_desc_id,
                    curr_table_desc['column_type'],
                    curr_table_desc['precision'],
                    curr_table_desc['is_pk'],
                    curr_table_desc['is_nullable'],
                    curr_table_desc['comment'],
                    curr_table_desc['href']
                ])
                
            i += 1
        except Exception as ex:
            print('Got exception <%s>' % repr(ex))
            spamwriter_columns.writerow([
                _row['table_schema'],
                _row['table_name'],
                '-',
                '-',
                '-',
                '-',
                '-',
                '-',
                'AUTH ERROR',
                '-'
            ])
                
            i += 1
            next
