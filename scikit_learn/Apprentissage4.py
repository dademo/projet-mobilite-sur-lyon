from sklearn import datasets
from sklearn import neighbors
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

iris = datasets.load_iris()
data = iris.data[:, :2]
target = iris.target
data[:5]

# On réapprend
from ipywidgets import interact
@interact(n=(0,20))
def n_change(n=5):	
clf = neighbors.KNeighborsClassifier(n_neighbors = n)
clf.fit(data, target)
 Z = clf.predict(data_samples)
#Z = Z.reshape(xx.shape)
plt.figure(1)
#plt.pcolormesh(xx, yy, Z) # Affiche les déductions en couleurs pour les couples x,y
plt.pcolormesh(xx, yy, Z.reshape(xx.shape)) # Affiche les déductions en couleurs pour les couples x,y
# Plot also the training points
colors = ['violet', 'yellow', 'red']
C = [colors[x] for x in target]
plt.scatter(data[:, 0], data[:, 1], c=C)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xlabel('Longueur du sepal (cm)')
plt.ylabel('Largueur du sepal (cm)');
plt.show()