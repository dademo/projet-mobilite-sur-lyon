from sklearn.naive_bayes import GaussianNB
from sklearn import datasets
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

iris = datasets.load_iris()
data = iris.data[:, :2]
target = iris.target
data[:5]

# On réapprend
clf = GaussianNB()
clf.fit(data, target)
h = .15
# Nous recherchons les valeurs min/max de longueurs/largeurs des sépales
x_min, x_max = data[:, 0].min() - 1, data[:, 0].max() + 1
y_min, y_max = data[:, 1].min() - 1, data[:, 1].max() + 1

x = np.arange(x_min, x_max, h)
y = np.arange(y_min, y_max, h)

print(x)
print(x_min)
print(x_max)
print(y_min)
print(y_max)

xx, yy = np.meshgrid(x,y )
data_samples = list(zip(xx.ravel(), yy.ravel()) )

a = [[10, 20], [1, 2]]
np.array(a).ravel()

Z = clf.predict(data_samples)
#Z = Z.reshape(xx.shape)
plt.figure(1)
#plt.pcolormesh(xx, yy, Z) # Affiche les déductions en couleurs pour les couples x,y
plt.pcolormesh(xx, yy, Z.reshape(xx.shape)) # Affiche les déductions en couleurs pour les couples x,y
# Plot also the training points
colors = ['violet', 'yellow', 'red']
C = [colors[x] for x in target]
plt.scatter(data[:, 0], data[:, 1], c=C)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xlabel('Longueur du sepal (cm)')
plt.ylabel('Largueur du sepal (cm)');
plt.show()