from sklearn.tree import DecisionTreeClassifier

import pydot
import StringIO

# Fomration et objectif du classificateur
train = [[1,2,3],[2,5,1],[2,1,7]]
target = [10,20,30]

# Initialiser classificateur. 
# Random values are initialized with always the same random seed of value 0 
# (allows reproducible results)
dectree = DecisionTreeClassifier(random_state=0)
dectree.fit(train, target)

# Test avec un autre classificateur
test = [2,2,3]
predicted = dectree.predict(test)

print(predicted)


dotfile = StringIO.StringIO()
tree.export_graphviz(dectree, out_file=dotfile)
(graph,)=pydot.graph_from_dot_data(dotfile.getvalue())
graph.write_png("dtree.png")
graph.write_pdf("dtree.pdf")