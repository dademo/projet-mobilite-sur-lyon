from sklearn.naive_bayes import GaussianNB
from sklearn import datasets
import matplotlib.pyplot as plt
import seaborn as sns

iris = datasets.load_iris()
data = iris.data
target = iris.target

# from sklearn.cross_validation import train_test_split # Version 0.17.1
from sklearn.model_selection import train_test_split # version 0.18.1
# split the data with 50% in each set
data_test = train_test_split(data, target
                                 , random_state=0
                                 , train_size=0.5)
data_train, data_test, target_train, target_test = data_test

data_test[:5]

clf = GaussianNB()
clf.fit(data_train, target_train)


#clf.get_params()

result = clf.predict(data_test)
print(result)
print(result - target_test)

#errors = sum(result != target) # 6 erreurs sur 150 mesures
#print("Nb erreurs:", errors)
#print( "Pourcentage de prédiction juste:", (150-errors)*100/150)   # 96 % de réussite


from sklearn.metrics import confusion_matrix
conf = confusion_matrix(target_test, result)


sns.heatmap(conf, square=True, annot=True, cbar=False
           , xticklabels=list(iris.target_names)
            , yticklabels=list(iris.target_names))
#plt.matshow(conf, cmap='rainbow');
plt.xlabel('valeurs prédites')
plt.ylabel('valeurs réelles');
plt.show()