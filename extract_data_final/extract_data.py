import logging
import os
import re
import csv
import json
import datetime
import requests

'''
    Collecte des données publiques du Grand Lyon à partir de services WFS (collecte de geojson).
    Les fichers seront dans le dossier `out_data/extract_<date>` et auront pour nom `dataset_<dataset_name>.json` où date sera au format `%Y%m%d-%H%M%S`
'''

class RequestsFetchEror(RuntimeError):
    pass

M_DIRFORMAT = 'extract_%(date)s'
M_FILEFORMAT = 'dataset_%(dataset_name)s.json'
M_TIMEFORMAT = '%Y%m%d-%H%M%S'
M_DATA_DIR = 'out_data'

def re_get_dataset_name(url):
    res = re.search(r'typename=([^&]+)', url)
    if res:
        return res.group(1)
    else:
        raise ValueError("Impossible d'obtenir le nom du dataset")

def fetch_url(url):
    r = requests.get(url, allow_redirects=True)
    if r.status_code / 100 != 2:
        raise RequestsFetchEror("Code de retour innatendu reçu (%d)" % r.status_code)
    return r.json()

if __name__ == '__main__':
    #logging.basicConfig(format='[%(levelname)s] %(asctime)s - %(filename)s:%(lineno)s - %(message)s', datefmt='%Y%d%m-%H%M%S', level=logging.INFO)
    logging.basicConfig(format='[%(levelname)s] %(asctime)s - %(message)s', datefmt='%Y/%d/%m-%H:%M:%S', level=logging.INFO)
    # Variables du programme
    m_self_path = os.path.dirname(__file__) or '.'
    m_dateextract = datetime.datetime.now().strftime(M_TIMEFORMAT)
    M_EXTRACT_DIR = M_DIRFORMAT % {'date': m_dateextract}
    config = []
    
    # Initialisation
    if not os.path.exists(os.path.join(m_self_path, M_DATA_DIR)):
        logging.info("Le répertoire de sortie (%s) n'existe pas, création" % M_DATA_DIR)
        os.mkdir(os.path.join(m_self_path, M_DATA_DIR))
    if not os.path.exists(os.path.join(m_self_path, M_DATA_DIR, M_EXTRACT_DIR)):
        logging.info("Le répertoire de sortie (%s) n'existe pas, création" % M_EXTRACT_DIR)
        os.mkdir(os.path.join(m_self_path, M_DATA_DIR, M_EXTRACT_DIR))

    # Lecture de la config
    if not os.path.exists(os.path.join(m_self_path, 'config.csv')) or not os.access(os.path.join(m_self_path, 'config.csv'), os.R_OK):
        raise RuntimeError("Impossible de trouver la configuration (config.csv). Cette erreur est fatale !")

    with open(os.path.join(m_self_path, 'config.csv'), 'r') as m_file:
        reader = csv.DictReader(m_file, delimiter=';', quotechar='"')
        config.extend(dict(row) for row in reader)

    for config_row in config:
        m_dataset_name = re_get_dataset_name(config_row['URL'])
        try:
            # Collecte
            logging.info('Collecte du dataset %s' % m_dataset_name)
            data = fetch_url(config_row['URL'])
            # Écriture
            logging.info('Écriture des données')
            with open(os.path.join(m_self_path, M_DATA_DIR, M_EXTRACT_DIR, M_FILEFORMAT % {'dataset_name': m_dataset_name}), 'w') as outfile:
                json.dump(data, outfile)
            logging.info('OK')
        except RequestsFetchEror:
            logging.exception("Impossible de récupérer les données du dataset %s" % m_dataset_name)