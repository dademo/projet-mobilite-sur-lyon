#!/bin/env python3

import os

import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy
from shapely.ops import nearest_points

cached = {}

def m_prepare_cols(arg, **kwargs):
    kwargs.update(arg)
    m_cols = set()
    translated = {}

    for elem_name, elem_value in kwargs.items():
        for column_name in elem_value.columns:
            if column_name in m_cols:
                m_column_name = '%s_%s' % (elem_name.replace('.', '_'), column_name)
                if not elem_name in translated:
                    translated[elem_name] = {}
                translated[elem_name][column_name] = m_column_name
                print('%s -> %s' % (column_name, m_column_name))
                print(column_name in elem_value.columns)
                elem_value.rename(columns={column_name: m_column_name}, inplace=True)
                if column_name == 'geometry' and isinstance(elem_value, gpd.GeoDataFrame):
                    print('-=[]=-')
                    elem_value.set_geometry(m_column_name, inplace=True)
                print('Got: %s' % (','.join(elem_value.columns)))
                column_name = m_column_name
            m_cols.add(column_name)

    return translated

def m_has_translated(translated, element, item):
    if element in translated.keys():
        if item in translated[element].keys():
            return translated[element][item]
    return item

def m_get_union(df):
    global cached
    if id(df) not in cached:
        cached[id(df)] = df.unary_union
    return cached[id(df)]


# src: https://automating-gis-processes.github.io/2017/lessons/L3/nearest-neighbour.html
def nearest(row, df1, df2, geom1_col='geometry', geom2_col='geometry', src_column=None):
    """Find the nearest point and return the corresponding value from specified column."""

    if row.name % 10 == 0:
        print('At index %04d of %04d' % (row.name, len(df1)))

    #geom_union = df2.unary_union
    geom_union = m_get_union(df2)
    # Find the geometry that is closest
    #nearest = df2[geom2_col] == nearest_points(row[geom1_col], geom_union)[1]
    # Get the corresponding value from df2 (matching is based on the geometry)
    #value = df2[nearest][src_column].get_values()[0]

    # Find the nearest points
    nearest_geoms = nearest_points(row[geom1_col], geom_union)

    #_idx = geom_union.index(nearest_geoms[1])

    # Get corresponding values from the other df
    #nearest_data = df2.loc[df2[geom2_col] == nearest_geoms[1]]
    nearest_data = df2.loc[df2[geom2_col].intersects(nearest_geoms[1])]
    #nearest_data = df2.loc[df2[geom2_col].contains(nearest_geoms[1])]

    if len(nearest_data) <= 0:
        toReturn = -1
    elif len(nearest_data) > 1:
        toReturn = nearest_data.index[0]
    else:
        if isinstance(nearest_data.index, numpy.ndarray):
            toReturn = nearest_data.index[0]
        else:
            toReturn = nearest_data.index
    

    if not isinstance(toReturn, int):
        if isinstance(toReturn, pd.core.indexes.numeric.Int64Index):
            if len(toReturn) >= 1:
                toReturn = toReturn.values[0]
        elif isinstance(toReturn, numpy.int64):
            toReturn = toReturn.item()
        elif len(toReturn) > 1:
            toReturn = toReturn[0]
        elif len(toReturn <= 0):
            toReturn = -1
    
    return toReturn
    #return nearest_data.index

def m_merge(desc, dfs, base=None):
    if not base:
        base = dfs.keys()[0]

    final_gdf = dfs[base]
    del dfs[base]
        
    # https://automating-gis-processes.github.io/2017/lessons/L3/nearest-neighbour.html

    for _df_name, _df in dfs.items():
        #final_gdf['centroid'] = final_gdf.centroid
        #final_gdf['nearest_id'] = final_gdf.apply(nearest, df1=final_gdf, df2=_df, geom1_col='centroid', src_column='id', axis=1)
        # Getting row ids
        print('First: %s' % (','.join(final_gdf.columns)))
        print('With: %s' % (','.join(_df.columns)))
        print('%s <> %s' % (m_has_translated(desc, base, 'geometry'), m_has_translated(desc, _df_name, 'geometry')))
        final_gdf['nearest_id'] = final_gdf.apply(nearest, df1=final_gdf, df2=_df, geom1_col=m_has_translated(desc, base, 'geometry'), geom2_col=m_has_translated(desc, _df_name, 'geometry'), src_column='id', axis=1)
        # Merging
        _df['_id'] = _df.index
        #final_gdf = final_gdf.merge(right=_df, left_on='nearest_id', right_index=True)
        final_gdf = final_gdf.merge(right=_df, left_on='nearest_id', right_index=True)
        final_gdf = gpd.GeoDataFrame(final_gdf, geometry='geometry')
        print('OK')
    
    
    # Merging dataframes : https://thispointer.com/pandas-how-to-merge-dataframes-by-index-using-dataframe-merge-part-3/

    return final_gdf

fp = os.path.join('data', 'pvo_patrimoine_voirie.pvotrafic.shp')
fp2 = os.path.join('data', 'lyv_lyvia.lyvchantier.shp')

data_pvotrafic = gpd.read_file(fp)
data_lyvchantier = gpd.read_file(fp2)

# Create a figure with one subplot
#fig, ax = plt.subplots()

# Plot polygons
#data_pvotrafic.plot(ax=ax, facecolor='gray')


#data_pvotrafic.rename(columns={'geometry': 'pvo_patrimoine_voirie.pvotrafic'})

# Make a spatial join
# note: seems to require python3-rtree
#join = gpd.sjoin(data_pvotrafic, data_lyvchantier, how='inner')
#join = gpd.GeoDataFrame(pd.concat([data_lyvchantier, data_pvotrafic], sort=False, ignore_index = True))
#print(join.columns)
#print(join)
# Save to disk
#join.to_file(outfp)

#for _elem in join.iterrows():
#    print(_elem)
#    print(_elem.__class__)

print(data_pvotrafic.geometry)
print(data_lyvchantier.geometry)
desc = m_prepare_cols({
    'pvo_patrimoine_voirie.pvotrafic': data_pvotrafic,
    'lyv_lyvia.lyvchantier': data_lyvchantier
})
print(data_pvotrafic.geometry)
print(data_lyvchantier.geometry)
final_gdf = m_merge(desc, {
    'pvo_patrimoine_voirie.pvotrafic': data_pvotrafic,
    'lyv_lyvia.lyvchantier': data_lyvchantier
}, 'pvo_patrimoine_voirie.pvotrafic')
print(final_gdf)
for column in final_gdf.columns:
    print("%s: %s" % (column, final_gdf[column][0].__class__))

# Output path
outfp = "out.json"
final_gdf.to_file(outfp, driver='GeoJSON')
