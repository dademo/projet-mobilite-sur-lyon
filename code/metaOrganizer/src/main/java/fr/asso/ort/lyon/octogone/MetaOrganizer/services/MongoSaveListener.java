package fr.asso.ort.lyon.octogone.MetaOrganizer.services;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;

import com.mongodb.BasicDBObject;

public class MongoSaveListener extends AbstractMongoEventListener<Object> {
	
	private class AgregateMaxIdResult {
		private BigInteger maxId;
	}

    Logger logger = LoggerFactory.getLogger(MongoSaveListener.class);
    

	private final static Map<String, BigInteger> idMap = new HashMap<>();
	
	@Autowired
    private MongoOperations mongoOperations;
	
	private BigInteger getCollectionMaxId(String collectionName)
	{
		Document doc = mongoOperations
				.getCollection(collectionName)
				.find()
				.sort(new BasicDBObject("id", -1))
				.first();
		try {
			return (doc != null)? (BigInteger) doc.get("id") : BigInteger.valueOf(0);
		} catch (ClassCastException ex) {
			return BigInteger.valueOf(0);
		}
		/*
		String t = null;
		AggregationResults<AgregateMaxIdResult> aggResult = mongoOperations.aggregate(
			Aggregation.newAggregation(Aggregation.group(t).max("id").as("maxId")),
			collectionName,
			AgregateMaxIdResult.class
		);
		return aggResult.getMappedResults().get(0).maxId;
		*/
		
		/*toReturn = mongoOperations.aggregate(Arrays.asList(
				Aggregates.group(null, Accumulators.max("max_id", "$id"))
		)).first().get;*/
		/*
		Aggregation agg = Aggregation.newAggregation(
			Aggregation.group(t).max("id").as("max_id")
		);
		AggregationResults<AgregateMaxIdResult> aggResult = mongoOperations.aggregate(agg, collectionName, AgregateMaxIdResult.class);
		*/
		/*
		DBCursor cursor = mongoOperations.execute(collectionName, new CollectionCallback<DBCursor>() {
			@Override
			public DBCursor doInCollection(MongoCollection<Document> collection)
					throws MongoException, DataAccessException {
				return collection.aggregate(Arrays.asList(
							Aggregates.group(null, Accumulators.max("max_id", "$id"))
						)).cursor();
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		*/
	}
	
    @Override
    public void onBeforeConvert(BeforeConvertEvent<Object> event) {
    	String collectionName = event.getCollectionName();
    	BigInteger maxId;
    	if(idMap.containsKey(collectionName))
    	{
    		maxId = idMap.get(collectionName);
    	}
    	else
    	{
    		maxId = getCollectionMaxId(collectionName);
    	}
    	if(maxId == null)
    	{
    		maxId = BigInteger.valueOf(0);
    	}
    	logger.info("Hello World !");
		logger.info(String.format("MaxId: %s", maxId));
		
		Object o = event.getSource();
		try
		{
			Method mId = o.getClass().getDeclaredMethod("setId", BigInteger.class);
			Method m_Id = o.getClass().getDeclaredMethod("set_id", BigInteger.class);
			//Field f = o.getClass().getDeclaredField("id");
			//f.set(o, maxId + 1);
			BigInteger newId = maxId.add(BigInteger.valueOf(1));
			logger.info(String.format("test: %s", newId));
			//mId.invoke(o, newId);
			//m_Id.invoke(o, newId);
			logger.info(String.format("Updated, now has value %s", o.getClass().getDeclaredMethod("getId").invoke(o)));
			logger.info(String.format("Updated, now has value %s", o.getClass().getDeclaredMethod("get_id").invoke(o)));
		}
		catch (IllegalAccessException ex)
		{
			logger.error(ex.toString());
		}
		catch (InvocationTargetException ex)
		{
			logger.error(ex.toString());
		}
		catch (NoSuchMethodException ex)
		{
			logger.error(ex.toString());
		}
    }
	
	@Override
	public void onBeforeSave(BeforeSaveEvent<Object> event) {
    	logger.info("Hello World 2 !");
	}
	
	@Override
	public void onApplicationEvent(MongoMappingEvent<?> event) {
		Object o = event.getSource();
		try {
			logger.info(String.format(":%s:", o.getClass().getMethod("get_id").invoke(o)));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			// TODO Auto-generated catch block
			logger.error(String.format(e.toString()));
		}
		super.onApplicationEvent(event);
		try {
			logger.info(String.format(":%s:", o.getClass().getMethod("get_id").invoke(o)));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			// TODO Auto-generated catch block
			logger.error(String.format(e.toString()));
		}
	}
}
