package fr.asso.ort.lyon.octogone.MetaOrganizer.entities;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/* Angular Time class: https://angular.io/api/common/Time */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Time {
    @NotNull
	private int hours;
    @NotNull
	private int minutes;
}
