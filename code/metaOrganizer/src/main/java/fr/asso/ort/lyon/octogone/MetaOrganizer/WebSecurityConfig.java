package fr.asso.ort.lyon.octogone.MetaOrganizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.asso.ort.lyon.octogone.MetaOrganizer.components.CustomizeAuthenticationSuccessHandler;
import fr.asso.ort.lyon.octogone.MetaOrganizer.services.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	CustomizeAuthenticationSuccessHandler customizeAuthenticationSuccessHandler;
	
	@Bean
	public UserDetailsService mongoUserDetails() {
	    return new CustomUserDetailsService();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    UserDetailsService userDetailsService = mongoUserDetails();
	    auth
	        .userDetailsService(userDetailsService)
	        .passwordEncoder(bCryptPasswordEncoder);
	}
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.csrf().disable();
        /*http
            .csrf().disable();*/
    	http
        .authorizeRequests()
            .anyRequest().authenticated()
        .and()
        	.antMatcher("/api/**")
            .httpBasic()
        .and()
        	.antMatcher("/**")
        	.formLogin()
            //.loginPage("/login")
            .permitAll()
        .and()
        	.logout();
    }
}
