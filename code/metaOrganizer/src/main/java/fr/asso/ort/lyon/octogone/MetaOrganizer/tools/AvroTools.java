package fr.asso.ort.lyon.octogone.MetaOrganizer.tools;

import javax.validation.constraints.NotNull;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonObject;

import org.apache.avro.Schema;



public abstract class AvroTools {
	
    private static Logger logger = LoggerFactory.getLogger(AvroTools.class);
    
    
	// https://avro.apache.org/docs/1.8.1/api/java/org/apache/avro/Schema.Parser.html
	//public static boolean checkSchema(@NotNull JsonNode schema) {
    //public static boolean checkSchema(@NotNull JsonObject schema) {
    public static boolean checkSchema(@NotNull Object schema) {
		Schema.Parser parser = new Schema.Parser();
		Schema parsedSchema = parser.parse(schema.toString());
		AvroTools.logger.info(parsedSchema.toString());
		return false;
	}
}
