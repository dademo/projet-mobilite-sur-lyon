package fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceType {
	JSON("json"),
	GEOJSON("geojson");
	
	private String value;
}
