package fr.asso.ort.lyon.octogone.MetaOrganizer.constraints;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import fr.asso.ort.lyon.octogone.MetaOrganizer.validators.AvroValidator;

/* http://dolszewski.com/spring/custom-validation-annotation-in-spring/ */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AvroValidator.class)
public @interface AvroSchema {
    String message() default "{fr.asso.ort.lyon.octogone.MetaOrganizer.constraints.AvroSchema.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
