package fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonObject;

import fr.asso.ort.lyon.octogone.MetaOrganizer.constraints.AvroSchema;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.Time;

import javax.validation.constraints.NotNull;

import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta")
public class MetaDescriptor extends MetaDescriptorShort {

    private String fullname;
    @NotNull
    private ServiceType service_type;
	@Indexed(unique = true)
    @NotNull
    private String url;
	private String crs;
    @NotNull
    private Time collect_interval;
    @NotNull
    private String module;
    @NotNull
    private String data_source;
    @NotNull
    private String data_source_ref;
    @NotNull
    private String privacy;
    @NotNull
    private String license;
    /* https://www.baeldung.com/jackson-mapping-dynamic-object */
    @NotNull
    @AvroSchema /* Avro schema checker */
    //private JsonNode schema;
    //private JsonObject schema;
    private Object schema;
    
}
