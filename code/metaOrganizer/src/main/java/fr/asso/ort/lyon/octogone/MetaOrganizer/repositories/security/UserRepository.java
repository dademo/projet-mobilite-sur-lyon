package fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.security;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.security.User;

public interface UserRepository extends MongoRepository<User, String> {
	public User findByEmail(String email);
	public User findByUsername(String username);
}
