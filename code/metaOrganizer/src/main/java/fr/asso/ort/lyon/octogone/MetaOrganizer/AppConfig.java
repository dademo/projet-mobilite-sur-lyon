package fr.asso.ort.lyon.octogone.MetaOrganizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class AppConfig {
	
	@Autowired
	MongoMappingContext mongoMappingContext;
	
	// From: https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#mongo.mongo-java-config
	// https://www.djamware.com/post/5b2f000880aca77b083240b2/spring-boot-security-and-data-mongodb-authentication-example
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
	    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	    return bCryptPasswordEncoder;
	}

	/*
	@Bean
	public MongoSaveListener customMongoSaveListener() {
	    return new MongoSaveListener();
	}
	*/
}
