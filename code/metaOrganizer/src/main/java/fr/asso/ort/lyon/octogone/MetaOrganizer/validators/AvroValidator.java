package fr.asso.ort.lyon.octogone.MetaOrganizer.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import fr.asso.ort.lyon.octogone.MetaOrganizer.constraints.AvroSchema;

/* http://dolszewski.com/spring/custom-validation-annotation-in-spring/ */
//public class AvroValidator implements ConstraintValidator<AvroSchema, JsonNode> {
//public class AvroValidator implements ConstraintValidator<AvroSchema, JsonObject> {
public class AvroValidator implements ConstraintValidator<AvroSchema, Object> {

    private static Logger logger = LoggerFactory.getLogger(AvroValidator.class);
    
	@Override
		public void initialize(AvroSchema constraintAnnotation) {
			ConstraintValidator.super.initialize(constraintAnnotation);
		}
	
	@Override
	//public boolean isValid(JsonNode schema, ConstraintValidatorContext context) {
	//public boolean isValid(JsonObject schema, ConstraintValidatorContext context) {
	public boolean isValid(Object schema, ConstraintValidatorContext context) {
		/* https://avro.apache.org/docs/1.8.1/api/java/org/apache/avro/Schema.Parser.html */
		try {
			Schema.Parser parser = new Schema.Parser();
			Schema parsedSchema = parser.parse(new Gson().toJson(schema));
			//AvroTools.logger.info(parsedSchema.toString());
			return true;
		} catch (org.apache.avro.SchemaParseException ex) {
			/* https://avro.apache.org/docs/1.6.3/api/java/org/apache/avro/SchemaParseException.html */
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(ex.getLocalizedMessage()).addConstraintViolation();
			return false;
		}
		//return false;
	}
}
