package fr.asso.ort.lyon.octogone.MetaOrganizer.controllers;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import fr.asso.ort.lyon.octogone.MetaOrganizer.constraints.AvroSchema;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.Time;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.MetaDescriptor;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.MetaDescriptorShort;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.ServiceType;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.converter.ServiceTypeConverter;
import fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.meta.MetaDescriptorRepository;
import fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.meta.MetaDescriptorShortRepository;
import fr.asso.ort.lyon.octogone.MetaOrganizer.tools.AvroTools;

@RestController
@RequestMapping("/api/metadescriptor")
public class MetaDescriptorController {
	
    Logger logger = LoggerFactory.getLogger(MetaDescriptorController.class);
	
	@Autowired
	private MetaDescriptorRepository metaDescriptorRepository;
	
	@Autowired
	private MetaDescriptorShortRepository metaDescriptorShortRepository;
	
	@InitBinder
	public void initBinder(final WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(ServiceType.class, new ServiceTypeConverter());
	}
	
	@GetMapping(path={"", "/"}, produces={ MediaType.APPLICATION_JSON_VALUE /* OK */})
	public ResponseEntity<List<MetaDescriptorShort>> list() {
		//MetaDescriptorShort toReturn = this.metaDescriptorShortRepository
		return new ResponseEntity<List<MetaDescriptorShort>>(this.metaDescriptorShortRepository.findAllQuery(), HttpStatus.OK);
	}

	@GetMapping(path="/{id}", produces={ MediaType.APPLICATION_JSON_VALUE /* OK */, MediaType.TEXT_PLAIN_VALUE /* Error */ })
	public ResponseEntity<?> get(@PathVariable String id) {
		MetaDescriptor toReturn = this.metaDescriptorRepository.findBy_id(id);
		if(toReturn == null) {
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<String>("Erreur : Aucune valeur trouvée", headers, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<MetaDescriptor>(toReturn, HttpStatus.OK);
		}
	}
	
	@PostMapping(path={"", "/"}, produces={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MetaDescriptor> addMetaDescriptor(@Valid @RequestBody MetaDescriptor metaDescriptor) {
		//MetaDescriptorShort toReturn = this.metaDescriptorShortRepository
		//return new ResponseEntity<List<MetaDescriptorShort>>(this.metaDescriptorShortRepository.findAll(), HttpStatus.OK);
		/* Saving */
		
		return new ResponseEntity<MetaDescriptor>(this.metaDescriptorRepository.save(metaDescriptor), HttpStatus.OK);
	}

	
	@PutMapping(path="/{id}", produces={ MediaType.APPLICATION_JSON_VALUE /* OK */, MediaType.TEXT_PLAIN_VALUE /* Error */ })
	public ResponseEntity<?> addMetaDescriptor(@Valid @RequestBody MetaDescriptor metaDescriptor, @PathVariable String id) {
		//MetaDescriptorShort toReturn = this.metaDescriptorShortRepository
		//return new ResponseEntity<List<MetaDescriptorShort>>(this.metaDescriptorShortRepository.findAll(), HttpStatus.OK);
		/* Saving */
		
		MetaDescriptor newDescriptor = this.metaDescriptorRepository.findBy_id(id);
		if(newDescriptor == null) {
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<String>("Erreur : L'objet n'existe pas", headers, HttpStatus.NOT_FOUND);
		}

		newDescriptor.setName(metaDescriptor.getName());
		newDescriptor.setDescription(metaDescriptor.getDescription());
		newDescriptor.setFullname(metaDescriptor.getFullname());
		newDescriptor.setUrl(metaDescriptor.getUrl());
		newDescriptor.setCollect_interval(metaDescriptor.getCollect_interval());
		newDescriptor.setModule(metaDescriptor.getModule());
		newDescriptor.setData_source(metaDescriptor.getData_source());
		newDescriptor.setData_source_ref(metaDescriptor.getData_source_ref());
		newDescriptor.setPrivacy(metaDescriptor.getPrivacy());
		newDescriptor.setLicense(metaDescriptor.getLicense());
		newDescriptor.setSchema(metaDescriptor.getSchema());
		
		return new ResponseEntity<MetaDescriptor>(this.metaDescriptorRepository.save(newDescriptor), HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public ResponseEntity<?> deleteddMetaDescriptor(@PathVariable String id) {
		MetaDescriptor newDescriptor = this.metaDescriptorRepository.findBy_id(id);
		if(newDescriptor == null) {
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<String>("Erreur : L'objet n'existe pas", headers, HttpStatus.NOT_FOUND);
		}
		this.metaDescriptorRepository.delete(newDescriptor);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
}
