package fr.asso.ort.lyon.octogone.MetaOrganizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetaOrganizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetaOrganizerApplication.class, args);
	}

}
