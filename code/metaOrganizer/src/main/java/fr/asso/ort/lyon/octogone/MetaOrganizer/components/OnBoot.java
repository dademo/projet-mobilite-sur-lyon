package fr.asso.ort.lyon.octogone.MetaOrganizer.components;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.security.Role;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.security.User;
import fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.security.RoleRepository;
import fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.security.UserRepository;
import fr.asso.ort.lyon.octogone.MetaOrganizer.services.CustomUserDetailsService;

@Component
public class OnBoot {

    Logger logger = LoggerFactory.getLogger(OnBoot.class);
    
    private static final String ADMIN_USERNAME = "admin";
    private static final String ADMIN_MAIL = null;
    private static final String ADMIN_PASSWORD = "admin";
    private static final boolean ADMIN_ENABLED = true;
    private static final String[] ADMIN_ROLE_NAMES = { "admin" };
    
    @Autowired
    private CustomUserDetailsService userService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;

    
	@PostConstruct
	void CheckAdmin()
	{
		User admin = this.userRepository.findByUsername(ADMIN_USERNAME);
		
		if (admin == null) {
			Set<Role> ADMIN_ROLES = new HashSet<>();
			// Adding roles
			for(String role_str : ADMIN_ROLE_NAMES) {
				Role role = this.roleRepository.findByRole(role_str);
				if(role == null) {
					role = new Role(null, role_str);
					this.roleRepository.save(role);
					role = this.roleRepository.findByRole(role_str);
				}
				ADMIN_ROLES.add(role);
			}
			
			// Adding a default admin
			admin = new User(
				null,
				ADMIN_USERNAME,
				ADMIN_MAIL,
				ADMIN_PASSWORD,
				ADMIN_ENABLED,
				ADMIN_ROLES
			);
			logger.info(String.format("Adding a default admin (`%s`, `%s`) with roles [%s]",
				admin.getUsername(),
				admin.getPassword(),
				String.join(",", ADMIN_ROLE_NAMES)
			));
			userService.saveUser(admin);
			//userRepository.save(admin);
		}
	}
}
