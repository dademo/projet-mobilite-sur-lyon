package fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta;

import java.math.BigInteger;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.Time;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta")
public class MetaDescriptorShort {
	
	@Id
	private String _id;
	@Indexed(unique = true)
	@NotNull
	private String name;
    private String description;
}
