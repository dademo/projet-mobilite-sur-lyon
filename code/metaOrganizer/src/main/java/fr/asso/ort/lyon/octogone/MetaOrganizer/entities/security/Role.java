package fr.asso.ort.lyon.octogone.MetaOrganizer.entities.security;

import java.math.BigInteger;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.Time;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// https://www.djamware.com/post/5b2f000880aca77b083240b2/spring-boot-security-and-data-mongodb-authentication-example

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta_roles")
public class Role {
	
	// https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#mongo-template.id-handling
	@Id
	private BigInteger _id;
	@Indexed(unique = true)
	@NotNull
	private String role;
}
