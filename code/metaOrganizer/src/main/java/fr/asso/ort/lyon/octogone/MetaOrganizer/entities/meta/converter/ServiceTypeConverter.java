package fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.converter;

import java.beans.PropertyEditorSupport;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.ServiceType;

public class ServiceTypeConverter extends PropertyEditorSupport {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(ServiceType.valueOf(text));
	}
}
