package fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.meta;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.MetaDescriptor;
import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.MetaDescriptorShort;

public interface MetaDescriptorRepository extends MongoRepository<MetaDescriptor, String> {
	public MetaDescriptor findBy_id(String id);
}
