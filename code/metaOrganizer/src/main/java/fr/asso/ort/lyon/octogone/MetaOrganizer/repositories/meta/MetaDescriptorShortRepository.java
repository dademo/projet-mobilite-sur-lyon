package fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.meta;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.meta.MetaDescriptorShort;

public interface MetaDescriptorShortRepository extends MongoRepository<MetaDescriptorShort, String> {
	
	@Query(value="{}", fields="{ '_id': 1, 'name': 1, 'description': 1 }")
	List<MetaDescriptorShort> findAllQuery();
}
