package fr.asso.ort.lyon.octogone.MetaOrganizer.repositories.security;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.asso.ort.lyon.octogone.MetaOrganizer.entities.security.Role;

public interface RoleRepository extends MongoRepository<Role, BigInteger> {
	public Role findByRole(String role);
}
