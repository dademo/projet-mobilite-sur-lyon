package main.java.fr.asso.ort.lyon.octogone.common;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MetaRepository {
	private static final String mongoConnectionString = "mongodb://root:root@localhost:27017/meta?authSource=admin";
	// https://www.mongodb.com/blog/post/getting-started-with-mongodb-and-java-part-i
	private static MongoClient mongoClient = null;
	
	public static MongoClient getConnection() {
		if(MetaRepository.mongoClient == null) {
			MetaRepository.mongoClient = new MongoClient(new MongoClientURI(MetaRepository.mongoConnectionString));
		}
		return MetaRepository.mongoClient;
	}
	
}
