package main.java.fr.asso.ort.lyon.octogone.common.entities;

import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {

	private String _id;
	@NotNull
	@Indexed(unique = true)
	private String jobName;
}
