#!/bin/bash

# Freezing: pip freeze > requirements.txt

M_PATH="$(dirname $0)"
if [ -z "${M_PATH}" ]; then M_PATH="."; fi

set -o xtrace

virtualenv -p python3 --no-site-package "${M_PATH}/.env" && \
    chmod +x "${M_PATH}/.env/bin/activate" && \
    "${M_PATH}/.env/bin/activate" && \
    bash -c "source '${M_PATH}/.env/bin/activate' && pip install -r '${M_PATH}/requirements.txt'"
