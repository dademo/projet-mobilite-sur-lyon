#!/bin/env python3

import sys
import os
import re
import datetime
import configparser

from pprint import pprint

from lib.modules.dataadaptators.HadoopAdaptor import HadoopAdaptor
from lib.modules.dataadaptators.FilesystemAdaptor import FilesystemAdaptor

_CONF_PATH = os.path.join(os.path.dirname(sys.argv[0]) or '.', 'config.ini')

def get_config():
    config = configparser.ConfigParser()
    if not config.read(_CONF_PATH):
        print("Unable to read conf")
        exit(1)
    return config

def init_hadoop_driver():
    config = get_config()
    return HadoopAdaptor(config.get("hadoop", "hdfs"), config.get("hadoop", "hdfs_user"), config.get("hadoop", "hdfs_basedir"))

def init_local_driver():
    config = get_config()
    return FilesystemAdaptor(config.get("localfs", "localfs_basedir"))

#__META__ = 'adr_voie_lieu.adrecole'
__META__ = 'pvo_patrimoine_voirie.pvochantierperturbant'
#__DRIVER__ = init_hadoop_driver()
__DRIVER__ = init_local_driver()

def get_latest(metaName: str):
    # finalFileName = '%s-%s.avro' % (self.getJobDescription()['name'], now.strftime('%Y%m%d_%H%M%S'))
    reg = re.compile(r'.+-(?P<date>[0-9_]+)\.avro')
    _file = None
    old_date = None
    base_dir = 'data/collectors/%s' % metaName
    year = max(map(lambda x: int(x), __DRIVER__.listDir(base_dir)))
    month = max(map(lambda x: int(x), __DRIVER__.listDir('%s/%04d' % (base_dir, year))))
    day = max(map(lambda x: int(x), __DRIVER__.listDir('%s/%04d/%02d' % (base_dir, year, month))))
    for filename in __DRIVER__.listDir('%s/%04d/%02d/%02d' % (base_dir, year, month, day)):
        res = reg.match(filename)
        if res:
            date = datetime.datetime.strptime(res.groupdict()['date'], '%Y%m%d_%H%M%S')
            if not old_date or date > old_date:
                _file = '%s/%04d/%02d/%02d/%s' % (base_dir, year, month, day, filename)
                old_date = date
        else:
            print('Pattern not match: %s' % filename)

    return _file

if __name__ == '__main__':
    filePath = get_latest(__META__)
    data, schema = __DRIVER__.avroRead(filePath)
    pprint(data)
    print("%d datas" % len(data))
