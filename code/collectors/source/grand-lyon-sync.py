#!/bin/env python

import configparser
import os
import json
import requests
from pprint import pprint

__USERNAME__ = 'dev.damien.demonaz@gmail.com'
__PASSWORD__ = '!rr95UQ8A'
__CURSOR_LEN__ = 50
__URL__ = 'https://data.grandlyon.com/api/elasticsearch/_search?request_cache=true'
__URL_POST_BODY__ = {
    "from": 0,
    "size": __CURSOR_LEN__,
    #"size": 1000,
    "query": {
        "bool": {
            "must": [
                {
                    "query_string": {
                        "query": "*",
                        "fields": [
                            "data_and_metadata",
                            "metadata-fr.title^5",
                            "metadata-fr.abstract^3",
                            "content-fr.title^5",
                            "content-fr.excerpt^3",
                            "content-fr.plaintext"
                        ],
                        "analyzer": "my_search_analyzer",
                        "fuzziness": "AUTO",
                        "minimum_should_match": "90%"
                        }
                },
                {
                    "bool": {
                        "should": [
                            {
                                "term": { "metadata-fr.category.keyword": "localisation" }
                            },
                            {
                                "term": { "metadata-fr.category.keyword": "services" }
                            },
                            {
                                "term": { "metadata-fr.category.keyword": "transport" }
                            }
                        ]
                    }
                },
                {
                    "bool": {
                        "should": [
                            {
                                "term": { "metadata-fr.link.service.keyword": "WS" }
                            }
                        ]
                    }
                }
            ]
        }
    },
    "collapse": {
        "field": "uuid.keyword",
        "inner_hits": {
            "name": "data",
            "size": 3,
            "highlight": {
                "pre_tags": [
                    "<span class=\\\"highlighted\\\">"
                ],
                "post_tags": [
                    "</span>"
                ],
                "require_field_match": False,
                "type": "unified",
                "fragmenter": "span",
                "order": "score",
                "highlight_query": {
                    "query_string": {
                        "query": "*",
                        "fields": [
                            "data_and_metadata",
                            "metadata-fr.title",
                            "metadata-fr.abstract",
                            "content-fr.title",
                            "content-fr.plaintext"
                        ],
                        "analyzer": "my_search_analyzer",
                        "fuzziness": "AUTO",
                        "minimum_should_match": "90%"
                    }
                },
                "fields": {
                    "*data-fr.*": {
                        "fragment_size": 100,
                        "number_of_fragments": 1
                    },
                    "metadata-fr.title": {
                        "number_of_fragments": 0
                    },
                    "content-fr.title": {
                        "number_of_fragments": 0
                    },
                    "content-fr.plaintext": {
                        "fragment_size": 50,
                        "number_of_fragments": 1
                    }
                }
            }
        }
    },
    "aggregations": {
        "metadata-fr.category": {
            "terms": {
                "field": "metadata-fr.category.keyword",
                "order": { "_key": "asc" },
                "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": {
                        "field": "uuid.keyword"
                    }
                }
            }
        },
        "metadata-fr.responsibleParty.organisationName": {
            "nested": {
                "path": "metadata-fr.responsibleParty"
            },
            "aggs": {
                "metadata-fr.responsibleParty.organisationName": {
                    "terms": {
                        "field": "metadata-fr.responsibleParty.organisationName.keyword",
                        "order": { "_key": "asc" },
                        "size": 500
                    },
                    "aggs": {
                        "count_per_metadata": {
                            "reverse_nested": {},
                            "aggregations": {
                                "uuid": {
                                    "cardinality": { "field": "uuid.keyword" }
                                }
                            }
                        },
                        "metadata-fr.responsibleParty.individualName": {
                            "terms": {
                                "field": "metadata-fr.responsibleParty.individualName.keyword",
                                "order": { "_key": "asc" },
                                "size": 500
                            },
                            "aggs": {
                                "count_per_metadata": {
                                    "reverse_nested": {},
                                    "aggregations": {
                                        "uuid": {
                                            "cardinality": { "field": "uuid.keyword" }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "metadata-fr.type": {
            "terms": {
                "field": "metadata-fr.type.keyword",
                "order": { "_key": "asc" },
                "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "metadata-fr.license": {
            "terms": {
                "field": "metadata-fr.license.keyword",
                "order": { "_key": "asc" },
                "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "metadata-fr.link.service": {
            "terms": {
                "field": "metadata-fr.link.service.keyword",
                "order": { "_key": "asc" },
                "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "metadata-fr.link.formats": {
            "terms": { 
                "field": "metadata-fr.link.formats.keyword",
                "order": { "_key": "asc" },
                "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "metadata-fr.publicationDate": {
            "date_histogram": {
                "field": "metadata-fr.publicationDate",
                "interval": "year",
                "min_doc_count": 1,
                "order": { "_count": "desc" }
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "metadata-fr.updateFrequency": {
            "terms": {
            "field": "metadata-fr.updateFrequency.keyword",
            "order": { "_key": "asc" },
            "size": 500
            },
            "aggs": {
                "count_per_metadata": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        },
        "count_by_scope": {
                "terms": {
                    "field": "type.keyword",
                    "order": { "_key": "asc" },
                    "size": 500
                },
            "aggs": {
                "count": {
                    "cardinality": { "field": "uuid.keyword" }
                }
            }
        }
    },
    "sort": [{ "_score": "desc" }]
}

def get_avro_types(field_types: dict):
    toReturn = []
    # datetime
    for key, value in field_types.items():
        key = key.replace('.', '_').replace(':', '_').replace('-', '_')
        if value == 'datetime' or value == 'timestamp' or value == 'date':
            toReturn.append({"name": key, "type": [ "null", "string" ], "default": None})
        elif value == 'str' or value == 'varchar':
            toReturn.append({"name": key, "type": [ "null", "string" ], "default": None})
        elif value.startswith('int'):
            toReturn.append({"name": key, "type": [ "null", "int", "long" ], "default": None})
        elif value.startswith('bool'):
            toReturn.append({"name": key, "type": [ "null", "boolean" ], "default": None})
        else:
            toReturn.append({"name": key, "type": [ "null", value ], "default": None})

    return toReturn


#print(json.dumps(__URL_POST_BODY__, indent=None))
#print(json.dumps(json.loads('{"from":0,"size":10,"query":{"bool":{"must":[{"query_string":{"query":"*","fields":["data_and_metadata","metadata-fr.title^5","metadata-fr.abstract^3","content-fr.title^5","content-fr.excerpt^3","content-fr.plaintext"],"analyzer":"my_search_analyzer","fuzziness":"AUTO","minimum_should_match":"90%"}},{"bool":{"should":[{"term":{"metadata-fr.category.keyword":"localisation"}},{"term":{"metadata-fr.category.keyword":"services"}},{"term":{"metadata-fr.category.keyword":"transport"}}]}},{"bool":{"should":[{"term":{"metadata-fr.link.service.keyword":"WS"}}]}}]}},"collapse":{"field":"uuid.keyword","inner_hits":{"name":"data","size":3,"highlight":{"pre_tags":["<span class=\\\"highlighted\\\">"],"post_tags":["</span>"],"require_field_match":false,"type":"unified","fragmenter":"span","order":"score","highlight_query":{"query_string":{"query":"*","fields":["data_and_metadata","metadata-fr.title","metadata-fr.abstract","content-fr.title","content-fr.plaintext"],"analyzer":"my_search_analyzer","fuzziness":"AUTO","minimum_should_match":"90%"}},"fields":{"*data-fr.*":{"fragment_size":100,"number_of_fragments":1},"metadata-fr.title":{"number_of_fragments":0},"content-fr.title":{"number_of_fragments":0},"content-fr.plaintext":{"fragment_size":50,"number_of_fragments":1}}}}},"aggregations":{"metadata-fr.category":{"terms":{"field":"metadata-fr.category.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.responsibleParty.organisationName":{"nested":{"path":"metadata-fr.responsibleParty"},"aggs":{"metadata-fr.responsibleParty.organisationName":{"terms":{"field":"metadata-fr.responsibleParty.organisationName.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"reverse_nested":{},"aggregations":{"uuid":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.responsibleParty.individualName":{"terms":{"field":"metadata-fr.responsibleParty.individualName.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"reverse_nested":{},"aggregations":{"uuid":{"cardinality":{"field":"uuid.keyword"}}}}}}}}}},"metadata-fr.type":{"terms":{"field":"metadata-fr.type.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.license":{"terms":{"field":"metadata-fr.license.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.link.service":{"terms":{"field":"metadata-fr.link.service.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.link.formats":{"terms":{"field":"metadata-fr.link.formats.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.publicationDate":{"date_histogram":{"field":"metadata-fr.publicationDate","interval":"year","min_doc_count":1,"order":{"_count":"desc"}},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"metadata-fr.updateFrequency":{"terms":{"field":"metadata-fr.updateFrequency.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count_per_metadata":{"cardinality":{"field":"uuid.keyword"}}}},"count_by_scope":{"terms":{"field":"type.keyword","order":{"_key":"asc"},"size":500},"aggs":{"count":{"cardinality":{"field":"uuid.keyword"}}}}},"sort":[{"_score":"desc"}]}'), indent=4))

if __name__ == '__main__':
    baseRequest = __URL_POST_BODY__
    reqLen = -1
    cursor = 0
    it = 0

    hits = [None]

    config = configparser.ConfigParser()
    if not config.read(os.path.join(os.path.dirname(__file__), 'config.ini')):
        raise RuntimeError("Unable to read conf")

    httpSession = requests.session()

    while len(hits) != 0:
        baseRequest['from'] = cursor
        result = requests.post(__URL__, json=baseRequest)
        '''
        print(json.dumps(baseRequest))
        pprint(result)
        pprint(result.json())
        '''

        hits = result.json()['hits']['hits']

        for hit in hits:
            print('--[%d]--' % it)
            _source = hit['_source']
            _meta_fr = _source['metadata-fr']
            privacy = ' | '.join(_meta_fr['legalConstraints'])

            if 'responsibleParty' in _meta_fr:
                responsible = ','.join(_responsible['organisationName'] for _responsible in _meta_fr['responsibleParty']) or '?'
            else:
                responsible = 'INCONNU'

            # Link and url
            url = ""
            rest_schema_url = ""
            name = None
            for link in _meta_fr['link']:
                if not 'WFS' in url and 'WFS' in link['content-type']:
                    if 'GeoJSON' in link['formats']:
                        name = link['name']
                        url = "%(url)s?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=%(typename)s&SRSNAME=EPSG:4171&outputFormat=application/json; subtype=geojson" % {
                            "url": link['url'],
                            "typename": link['name']
                        }
                        break
                    else:
                        print("No GeoJSON for WFS service [%s]" % link['name'])
                elif not url and 'JSON' in link['formats']:
                    name = link['name']
                    url = "%(url)s/%(name)s/all.json" % {
                        "url": link['url'],
                        "name": link['name']
                    }
                    rest_schema_url = "%(url)s/%(name)s.json" % {
                        "url": link['url'],
                        "name": link['name']
                    }

            if not url:
                print('Aucune URL trouvée pour la donnée [%s]' % _meta_fr['title'])
            else:
                # https://avro.apache.org/docs/1.8.2/spec.html#schema_primitive
                if 'ws' in url:
                    #print('JSON: %s' % name)
                    print('Getting schema [%s]' % rest_schema_url)
                    try:
                        r = requests.get(
                            rest_schema_url,
                            auth=(
                                config.get('datasource:GrandLyon', 'username'),
                                config.get('datasource:GrandLyon', 'password')
                            )
                        )
                        if r.status_code == 200:
                            schema = {
                                "type": "record",
                                "name": name,
                                "fields": get_avro_types(dict([(result['column_name'], result['column_type']) for result in r.json()['results']]))
                            }

                            toAdd = {
                                'name':             name,
                                'description':      _meta_fr['abstract'],
                                'fullname':         _meta_fr['title'],
                                'service_type':     'JSON',
                                'url':              url,
                                'crs':              None,
                                'collect_interval': { 'hours': 1, 'minutes': 0 },
                                'module':           'GrandLyon',
                                'data_source':      responsible,
                                'data_source_ref':  'https://data.grandlyon.com/jeux-de-donnees/%s/donnees' % _source['slug'],
                                'privacy':          privacy,
                                'license':          _meta_fr['license'],
                                'schema':           schema,
                                'uuid':             _source['uuid']
                            }
                        else:
                            print('Error, got status [%d] (%s)' % (r.status_code, r.reason))
                            continue
                    except Exception as ex:
                        print(str(ex))
                        continue
                else:
                    schema = {
                        "type": "record",
                        "name": name,
                        "fields": [
                            {"name": "type", "type": "string"},
                            {"name": "properties", "type": {
                                "name": "properties", "type": "record", "fields": get_avro_types(_source['fields']['types'])
                            }},
                            {"name": "geometry", "type": {
                                "name": "geometry",
                                "type": "record", "fields": [
                                    {"name": "type", "type": "string"},
                                    #{"name": "coordinates", "type": {"type": "array", "items": "float"}}
                                    {"name": "coordinates", "type": {"type": "array", "items": [
                                        "float", # Point
                                        {"type": "array", "items": [
                                            "float",    # LineString
                                            {"type": "array", "items": [
                                                "float",    # Polygon
                                                { "type": "array", "items": "float"},   # MutiPolygon
                                            ]},
                                        ]},
                                        #{"type": "array", "items": {"name": "lineStrings", "type": "record", "fields": [{"name": "lineString", "type": {"type": "array", "items": "float"} }]}}    # Polygon
                                    ]}}
                                ]
                            }}
                        ]
                    }
                    toAdd = {
                        'name':             name,
                        'description':      _meta_fr['abstract'],
                        'fullname':         _meta_fr['title'],
                        'service_type':     'GEOJSON',
                        'url':              url,
                        'crs':              'EPSG:4171',
                        'collect_interval': { 'hours': 1, 'minutes': 0 },
                        'module':           'GrandLyon',
                        'data_source':      responsible,
                        'data_source_ref':  'https://data.grandlyon.com/jeux-de-donnees/%s/donnees' % _source['slug'],
                        'privacy':          privacy,
                        'license':          _meta_fr['license'],
                        'schema':           schema,
                        'uuid':             _source['uuid']
                    }

                # Pushing datas
                if toAdd:
                    r = httpSession.post(
                        url='http://localhost:8080/api/metadescriptor/',
                        json=toAdd,
                        headers={
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                        },
                        auth=('admin', 'admin')
                    )
                    if r.status_code == 200:
                        print('Pushed')
                    else:
                        print('[%d] %s (%s)' % (r.status_code, r.text, r.reason))
                else:
                    print('No toAdd')
            
            it += 1

        # {
        #     'name': None,
        #     'description': None,
        #     'fullname': None,
        #     'url': None,
        #     'collect_interval': None,
        #     'module': None,
        #     'data_source': None,
        #     'data_source_ref': None,
        #     'privacy': None,
        #     'license': None,
        #     'schema': None,
        #     'uuid': None
        # }
        cursor += __CURSOR_LEN__

    httpSession.close()