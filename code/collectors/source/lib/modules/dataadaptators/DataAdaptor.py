import sys
import abc
import logging
import json
import io

import fastavro


class DataAdaptor(abc.ABC):

    # http://hadoop.apache.org/docs/r1.0.4/webhdfs.html#FileStatus
    @abc.abstractmethod
    def listDir(self, path) -> list:
        pass

    @abc.abstractmethod
    def read(self, path) -> bytearray:
        pass

    @abc.abstractmethod
    def write(self, data, path) -> int:
        pass

    @abc.abstractmethod
    def writefp(self, fp, path) -> int:
        pass

    @abc.abstractmethod
    def avroWrite(self, records, schema, path) -> int:
        pass

    @abc.abstractmethod
    def avroRead(self, path) -> (dict, list):
        # return: (records, schema)
        pass
        

    @abc.abstractmethod
    def avroReadAsync(self, path, callback) -> None:
        # return: (records, schema)
        pass

    @abc.abstractmethod
    def mkdir(self, path) -> None:
        pass

    @abc.abstractmethod
    def init(self) -> None:
        pass

    @abc.abstractmethod
    def free(self) -> None:
        pass

    def __enter__(self):
        self.init()

    def __exit__(self, type, value, traceback):
        self.free()