from .DataAdaptor import DataAdaptor

import hdfs.client as hdfsClient
import hdfs.ext.avro as hdfsAvro

class HadoopAdaptorException(RuntimeError):
    pass

class HadoopAdaptor(DataAdaptor):

    def __init__(self, url: str, user: str, basedir: str):
        self.client = hdfsClient.InsecureClient(url=url, root="/", user=user)
        self.basedir = basedir
    
    def listDir(self, path):
        return self.client.list(hdfs_path="%s/%s" % (self.basedir, path))

    def read(self, path):
        #toReturn = b''
        # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.client.Client.read
        #with self.client.read(hdfs_path=path) as reader:
        #    toReturn = reader.read()
        #return toReturn
        return self.client.read(hdfs_path="%s/%s" % (self.basedir, path))

    def write(self, data, path):
        # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.client.Client.write
        return self.client.write(hdfs_path="%s/%s" % (self.basedir, path), data=data, overwrite=False, append=False)

    def writefp(self, fp, path):
        return self.write(data=fp, path=path)

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.ext.avro.AvroWriter
    def avroWrite(self, records, schema, path) -> int:
        with hdfsAvro.AvroWriter(self.client, "%s/%s" % (self.basedir, path)) as writer:
            for record in records:
                writer.write(record)

        return -1

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.ext.avro.AvroReader
    def avroRead(self, path) -> (dict, list):
        # return: (records, schema)
        toReturn = []
        schema = None

        def onRead(record, reader):
            toReturn.append(record)
            schema = reader.schema

        self.avroReadAsync(path, onRead)

        return (toReturn, schema)

    def avroReadAsync(self, path, callback) -> None:
        # return: (records, schema)
        with hdfsAvro.AvroReader(self.client, "%s/%s" % (self.basedir, path)) as reader:
            for record in reader:
                callback(record, reader)

    def mkdir(self, path):
        return self.client.makedirs(hdfs_path="%s/%s" % (self.basedir, path))

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.client.Client
    def init(self):
        pass

    def free(self):
        #self.connection.close()
        pass