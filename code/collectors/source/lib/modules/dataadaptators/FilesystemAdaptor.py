from .DataAdaptor import DataAdaptor

import os
import fastavro
import logging

class FilesystemAdaptorException(RuntimeError):
    pass


def _clean_path(path: str):
    '''
    Removes all the '/' at the beginning of a string
    '''
    while path.startswith('/'):
        path = path[1:]
    return path

class FilesystemAdaptor(DataAdaptor):

    def __init__(self, basedir: str):
        self.basedir = basedir
    
    def listDir(self, path):
        return os.listdir(os.path.join(self.basedir, _clean_path(path)))

    def read(self, path):
        with open(os.path.join(self.basedir, _clean_path(path)), 'rb') as _file:
            return _file.read(-1)

    def write(self, data, path):
        with open(os.path.join(self.basedir, _clean_path(path)), 'wb') as _file:
            return _file.write(data)

    def writefp(self, fp, path):
        return self.write(fp.read(-1), path)

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.ext.avro.AvroWriter
    def avroWrite(self, records, schema, path) -> int:
        with open(os.path.join(self.basedir, _clean_path(path)), 'wb') as _file:
            return fastavro.writer(_file, schema, records)

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.ext.avro.AvroReader
    def avroRead(self, path) -> (dict, list):
        # return: (records, schema)
        toReturn = []
        schema = None

        def onRead(content, reader):
            toReturn.append(content)
            schema = reader.schema

        self.avroReadAsync(path, onRead)

        return (toReturn, schema)

    def avroReadAsync(self, path, callback) -> None:
        # return: (records, schema)
        with open(os.path.join(self.basedir, _clean_path(path)), 'rb') as _file:
            reader = fastavro.reader(_file)
            for record in reader:
                callback(record, reader)

    def mkdir(self, path):
        logging.info(self.basedir)
        return os.mkdir(os.path.join(self.basedir, _clean_path(path)))

    # https://hdfscli.readthedocs.io/en/latest/api.html#hdfs.client.Client
    def init(self):
        pass

    def free(self):
        #self.connection.close()
        pass