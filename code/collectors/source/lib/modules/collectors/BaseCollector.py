import sys
import abc
import logging
import json
import io
import os
import configparser
import datetime
import threading
import traceback

from lib import iterable

from lib.modules.dataadaptators.DataAdaptor import DataAdaptor

import fastavro

class BaseCollectorIterator(abc.ABC):

    # Only for debugging purposes
    @abc.abstractmethod
    def getJsonWrapper(self) -> dict:
        pass

    def __iter__(self):
        return self

    def __next__(self) -> dict:
        return self.next()

    def __len__(self) -> int:
        return self.size()

    @abc.abstractmethod
    def next(self) -> dict:
        pass

    @abc.abstractmethod
    def size(self) -> int:
        pass


class BaseWSIterator(BaseCollectorIterator):

    @abc.abstractmethod
    def getFields(self) -> list:
        pass

    def JsonWrapperMoreFields(self) -> dict:
        return {}

    def getJsonWrapper(self) -> dict:
        toReturn = {
            "fields": self.getFields(),
            "values": self.__iter__(),
        }
        toReturn.update(self.JsonWrapperMoreFields())
        return toReturn


class BaseWFSIterator(BaseCollectorIterator):

    @abc.abstractmethod
    def getName(self) -> str:
        pass

    def getJsonWrapper(self) -> dict:
        return {
            "type": "FeatureCollection",
            "name": self.getName(),
            "features": list(self.__iter__()),
        }


class BaseCollector(abc.ABC, threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.report = None

    def setJobConfiguration(self, configuration: configparser.ConfigParser):
        self.configuration = configuration

    def getJobConfiguration(self) -> configparser.ConfigParser:
        return self.configuration or None

    def setJobDescription(self, description: dict):
        self.description = description
        self.name = self.getJobDescription()['name']

    def getJobDescription(self) -> dict:
        return self.description

    def setDataAdaptor(self, dataAdaptor: DataAdaptor):
        self.dataAdaptor = dataAdaptor

    def getDataAdaptor(self) -> DataAdaptor:
        return self.dataAdaptor

    def getReport(self):
        return self.report

    @abc.abstractmethod
    def fetchData(self) -> BaseCollectorIterator:
        pass

    def pushData(self, data: iterable) -> (int, int):   # (extracted_data_size, generated_data_size)
        # Path: /collectors/${JOB_NAME}/${YEAR}/${MONTH}/${DAY}
        def check_make_dir(path):
            if not os.path.basename(path) in self.getDataAdaptor().listDir(os.path.dirname(path)):
                logging.info("Creating dir [%s]" % path)
                self.getDataAdaptor().mkdir(path)

        currPath = ""
        now = datetime.datetime.now()
        globalPath = [ 'data', 'collectors', self.getJobDescription()['name'], '%04d' % now.year, '%02d' % now.month, '%02d' % now.day ]
        logging.info("Checking for directories")
        for m_path in globalPath:
            currPath += '/%s' % m_path
            check_make_dir(currPath)

        # Pushing avro data
        finalFileName = '%s-%s.avro' % (self.getJobDescription()['name'], now.strftime('%Y%m%d_%H%M%S'))
        finalPath = '/%s/%s' % ('/'.join(globalPath), finalFileName)
        logging.info("Generating avro file")
        with io.BytesIO() as buffer:
            # TODO: convert data format [str de int -> int, str de float -> float, etc; et depuis le schéma]
            #for _data in data:
            #    print(_data)
            fastavro.writer(buffer, self.getJobDescription()['schema'], data)
            extracted_data_size = len(data)
            generated_data_size = buffer.tell()
            logging.info("Writing data to %s" % (finalPath))
            self.getDataAdaptor().write(buffer.getvalue(), finalPath)
            logging.info("OK")
            return (extracted_data_size, generated_data_size)
        #self.getDataAdaptor().avroWrite(data, self.getJobDescription()['schema'], finalPath)

    def run(self):
        job_start = datetime.datetime.now()
        self.report = {
            #"_id": "_____",
            "jobName": '%s_%s' % (self.getJobDescription()['name'], datetime.datetime.now().strftime('%Y%m%d%H%M%S')),
            "meta": self.getJobDescription()['name'],
            "timestamp": job_start,
            "batchDurationSeconds": None,  #  timedelta.total_seconds()
            "module": self.getJobDescription()['module'],
            "extractedDataSize": None,
            "generatedDataSize": None,
            "status": None,
            "errorType": None,
            "errorDescription": None
        }

        try:
            # Fetching data
            data = self.fetchData()
            if data == None:
                raise RuntimeError("Le module n'a pas retourné de donnée !")

            extracted_data_size, generated_data_size = self.pushData(data)
            self.report['status'] = 'ok'
            self.report['extractedDataSize'] = extracted_data_size
            self.report['generatedDataSize'] = generated_data_size
        except Exception as ex:
            logging.exception("Une erreur est survenue (%s; %s)" % (ex.__class__, str(ex)))
            self.report['status'] = 'ko'
            self.report['error_type'] = 'exception:%s' % ex.__class__
            self.report['error_description'] = traceback.format_exc()
        finally:
            self.report['batchDurationSeconds'] = (datetime.datetime.now() - job_start).total_seconds()

        return