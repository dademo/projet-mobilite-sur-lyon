from lib import BaseCollector, BaseCollectorIterator, BaseWSIterator, BaseWFSIterator
import requests
import logging
import json


# Number of values to query
FETCH_COUNT = 10000


## Main collector type ##
class GrandLyon(BaseCollector):

    def fetchData(self) -> BaseCollectorIterator:
        logging.info("Préparation de la collecte des données pour la meta [%s]" % self.getJobDescription()['name'])
        url = self.getJobDescription()['url']
        if '/wfs/' in url:
            #return self.fetch_WFS(url, withAuth=False)
            return GrandLyonWFSReader(self)
        elif '/ws/' in url:
            #return self.fetch_JSON(url, withAuth=False)
            return GrandLyonWSReader(self)
        else:
            logging.error("No method found for url [%s]" % url)
            return None


# Tool for transforming data into the right type
def transform_ws(data: dict, description: dict):
    #fields = list(filter(lambda x: x['name'] == 'values', description['schema']['fields']))[0]['type']['items']['fields']
    #for field in fields:
    for field in description['schema']['fields']:
        if field['name'] in data:
            _check = None
            _equalCheck = None
            if isinstance(field['type'], list):
                _check = lambda m_check: any(_type in m_check for _type in field['type'])
                _equalCheck = lambda m_check: any(_type == m_check for _type in field['type'])
            else:
                _check = lambda m_check: field['type'] in m_check
                _equalCheck = lambda m_check: field['type'] == m_check
            ##
            if _check(['int', 'long']):
                data[field['name']] = int(data[field['name']])
            elif _check(['float', 'double']):
                data[field['name']] = float(data[field['name']])
            elif _equalCheck('boolean'):
                if isinstance(data, str):
                    if data[field['name']].lower() == "true":
                        data[field['name']] = True
                    else:
                        data[field['name']] = False
                logging.warning("%s: %s" % (field['name'], data[field['name']]))
            elif _equalCheck('string'):
                data[field['name']] = str(data[field['name']])
        else:
            data[field['name']] = None

    return data

# Tool for transforming data into the right type
def transform_wfs(data: dict, description: dict):
    for field in list(filter(lambda x: x['name'] == 'properties', description['schema']['fields']))[0]['type']['fields']:
        if field['name'] in data['properties'] and data['properties'][field['name']] != None and data['properties'][field['name']] != '':
            _check = None
            _equalCheck = None
            if isinstance(field['type'], list):
                _check = lambda m_check: any(_type in m_check for _type in field['type'])
                _equalCheck = lambda m_check: any(_type == m_check for _type in field['type'])
            else:
                _check = lambda m_check: field['type'] in m_check
                _equalCheck = lambda m_check: field['type'] == m_check
            ##
            if _check(['int', 'long']):
                data['properties'][field['name']] = int(data['properties'][field['name']])
            elif _check(['float', 'double']):
                data['properties'][field['name']] = float(data['properties'][field['name']])
            elif _check('boolean'):
                if isinstance(data['properties'][field['name']], str):
                    if data['properties'][field['name']].lower() == "true":
                        data['properties'][field['name']] = True
                    else:
                        data['properties'][field['name']] = False
                elif isinstance(data['properties'][field['name']], int):
                    data['properties'][field['name']] = bool(data['properties'][field['name']])
            elif _check('string'):
                data['properties'][field['name']] = str(data['properties'][field['name']])
        else:
            data['properties'][field['name']] = None

    return data


## Data collectors ##

class GrandLyonWSReader(BaseWSIterator):

    def __init__(self, job: GrandLyon):
        self.job = job
        self.iterator = 0
        self.cache = []
        self.fields = []
        self.withAuth = None
        self.dataLength = 0

    def fetch(self) -> None:
        url = '%(url)s?maxfeatures=%(featureCount)d&start=%(start)d' % {
            'url': self.job.getJobDescription()['url'],
            'featureCount': FETCH_COUNT,
            'start': (self.iterator+1),
        }
        auth = None

        if self.withAuth:
            try:
                section_name = 'datasource:%s' % self.job.getJobDescription()['module']
                auth = (
                    self.job.getJobConfiguration().get(section=section_name, option="username"),
                    self.job.getJobConfiguration().get(section=section_name, option="password")
                )
            except Exception as ex:
                logging.error("Impossible de charger l'utilisateur connecté (%s)" % str(ex))
                return None

        logging.info('Querying ...')
        httpResult = requests.get(url, auth=auth)

        if httpResult.status_code == 401 and not self.withAuth:     # Unauthorized
            logging.warning("Code [%d] obtenu. On essaie avec l'authentification" % httpResult.status_code)
            self.withAuth = True
            return self.fetch()
        elif httpResult.status_code != 200:
            logging.error("Code [%d] obtenu (%s). Abandon" % (httpResult.status_code, httpResult.reason))
            raise RuntimeError("Unable to fetch data (%d: %s)" % (httpResult.status_code, httpResult.reason))
        else:
            # Merging
            res = httpResult.json()
            if not 'values' in res:
                raise RuntimeError("Unknown format for WS service")
            else:
                self.fields = res['fields']
                self.cache = res['values']
                self.dataLength += len(json.dumps(res))


    def next(self) -> dict:
        _index = self.iterator % FETCH_COUNT
        self.iterator += 1
        # If we are at a multiple of `FETCH_COUNT`, refreshing cache
        if _index == 0:
            self.fetch()

        if _index >= len(self.cache):
            # If we reached the last element, we stop the iteration
            raise StopIteration()
        else:
            return transform_ws(data=self.cache[_index], description=self.job.getJobDescription())

    def getFields(self) -> list:
        if self.fields:
            self.fetch()
            return self.fields
        else:
            return []

    def getLayerName(self) -> str:
        return self.job.getJobDescription()['name']

    def JsonWrapperMoreFields(self):
        return {
            "layer_name": self.getLayerName(),
            "table_href": self.job.getJobDescription()['url']
        }

    def size(self) -> int:
        return self.dataLength



class GrandLyonWFSReader(BaseWFSIterator):
    def __init__(self, job: GrandLyon):
        self.job = job
        self.iterator = 0
        self.cache = []
        self.withAuth = None
        self.dataLength = 0

    def fetch(self) -> dict:
        url = '%(url)s&count=%(count)d&startIndex=%(startIndex)d' % {
            'url': self.job.getJobDescription()['url'],
            'count': FETCH_COUNT,
            'startIndex': self.iterator,
        }
        auth = None

        if self.withAuth:
            try:
                section_name = 'datasource:%s' % self.job.getJobDescription()['module']
                auth = (
                    self.job.getJobConfiguration().get(section=section_name, option="username"),
                    self.job.getJobConfiguration().get(section=section_name, option="password")
                )
            except Exception as ex:
                logging.error("Impossible de charger l'utilisateur connecté (%s)" % str(ex))
                return None

        logging.info('Querying ...')
        httpResult = requests.get(url, auth=auth)

        if httpResult.status_code == 401 and not self.withAuth:     # Unauthorized
            logging.warning("Code [%d] obtenu. On essaie avec l'authentification" % httpResult.status_code)
            self.withAuth = True
            return self.fetch()
        elif httpResult.status_code != 200:
            print(url)
            logging.error("Code [%d] obtenu (%s). Abandon" % (httpResult.status_code, httpResult.reason))
            raise RuntimeError("Unable to fetch data (%d: %s)" % (httpResult.status_code, httpResult.reason))
        else:
            # Merging
            res = httpResult.json()
            if not 'features' in res:
                raise RuntimeError("Unknown format for WS service")
            else:
                self.cache = res['features']
                self.dataLength += len(json.dumps(res))

    def next(self) -> dict:
        _index = self.iterator % FETCH_COUNT
        self.iterator += 1
        # If we are at a multiple of `FETCH_COUNT`, refreshing cache
        if _index == 0:
            self.fetch()

        if _index >= len(self.cache):
            # If we reached the last element, we stop the iteration
            raise StopIteration()
        else:
            return transform_wfs(data=self.cache[_index], description=self.job.getJobDescription())

    def getName(self) -> str:
        return self.job.getJobDescription['name']

    def size(self) -> int:
        return self.dataLength
