#!/bin/env python3

# NOTE: Require Python 3.5
# https://docs.python.org/3/library/typing.html

import sys
import os
import time
import logging
import configparser
import argparse
import importlib
import datetime

import re

import pymongo

from lib import BaseCollector
from lib.modules.dataadaptators.HadoopAdaptor import HadoopAdaptor
from lib.modules.dataadaptators.FilesystemAdaptor import FilesystemAdaptor


_CONF_PATH = os.path.join(os.path.dirname(sys.argv[0]) or '.', 'config.ini')

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Exécute un job de collecte")
    parser.add_argument('--job', '-j',
        metavar='JOB_NAME',
        type=str,
        action='store',
        required=False,
        help='Le nom du job à exécuter')
    parser.add_argument('--driver', '-d',
        metavar='JOB_NAME',
        type=str,
        choices=["hdfs", "localfs"],
        action='store',
        required=True,
        default="hdfs",
        help='Le nom du job à exécuter')
    return parser.parse_args(sys.argv[1:])

def get_module(moduleName: str) -> BaseCollector:
    def m_filter(path):
        if path.startswith('./'):
            path = path[2:]
        if os.path.dirname(sys.argv[0]):
            return path.replace('%s%s' % (os.path.dirname(sys.argv[0]).replace('./', ''), os.path.sep), '')
        else:
            return path
    
    # http://www.blog.pythonlibrary.org/2012/07/31/advanced-python-how-to-dynamically-load-modules-or-classes/
    for dirpath, dirnames, filenames in os.walk(os.path.dirname(sys.argv[0]) or '.'):
        if '%s.py' % moduleName in filenames:
            modulePath = m_filter(dirpath).replace(os.path.sep, '.')
            if modulePath:
                modulePath = '%s.' % modulePath
            finalModuleName = '%s%s' % (modulePath, moduleName)
            logging.info("Chargement du module [%s]" % finalModuleName)
            module = importlib.import_module(finalModuleName)
            if not moduleName in dir(module):
                raise RuntimeError("La classe [%s] n'a pas été trouvée dans le module [%s]" % (moduleName, finalModuleName))
            moduleClass = getattr(module, moduleName)
            if not BaseCollector in moduleClass.__bases__:
                raise RuntimeError("La classe [%s] n'est pas un enfant de [%s]" % (module, BaseCollector))
            #return moduleClass()
            return moduleClass

    raise ModuleNotFoundError(moduleName)

def init_db(database: pymongo.database):
    jobCollection = database.get_collection('meta_jobs')
    index_information = jobCollection.index_information()
    if not index_information:
        logging.info("Initializing database")
        jobCollection.create_indexes([
            pymongo.IndexModel(
                keys=[("file", pymongo.ASCENDING)],     # Indexes
                unique=True     # Unique index
            ),
            pymongo.IndexModel(
                keys=[
                    ("collector_name", pymongo.ASCENDING),
                    ("timestamp", pymongo.DESCENDING)
                ],
                unique=True
            )
        ])


if __name__ == '__main__':
    # Variables
    metaList = []
    jobList = []
    moduleDict = {}
    jobThreadList = []
    jobThreadListCurrentIndex = 0
    current_threads = []
    __MAX_CONCURRENT_THREADS = 10


    # Init path loader
    #sys.path.append(os.path.join(os.path.dirname(sys.argv[0]) or '.', 'lib'))

    # Init logging
    logging.basicConfig(format="[%(threadName)s]\t[%(levelname)s]\t%(filename)s:%(lineno)03d\t%(asctime)s\t%(message)s", datefmt="%Y/%m/%d %H:%M:%S", level=logging.INFO)
    logging.info("Démarrage du collecteur générique")

    # Reading conf
    logging.info("Chargement des configurations")
    config = configparser.ConfigParser()
    if not config.read(_CONF_PATH):
        logging.error("Impossible de lire la configuration (fichier [%s])" % _CONF_PATH)
        exit(1)

    # Establishing db connection
    ## MongoDB
    logging.info("Connexion à la base de données")
    try:
        db_connction_string = config.get(section="db", option="meta")
    except configparser.NoOptionError as ex:
        logging.error("Impossible de récupérer la configuration (%s)" % str(ex))
        exit(1)

    # https://api.mongodb.com/python/current/tutorial.html
    try:
        db_client = pymongo.MongoClient(db_connction_string)
        meta_db = db_client.get_database('meta')
        #init_db(meta_db)   # -> Done by Java job
    except Exception as ex:
        logging.exception(ex)
        logging.error("Impossible de se connecter à la base de données (%s)" % str(ex))
        exit(1)


    # Parsing args
    args = parse_args()

    if args.driver == 'hdfs':
        ## HDFS
        logging.info("Connection au système HDFS")
        data_driver = HadoopAdaptor(config.get("hadoop", "hdfs"), config.get("hadoop", "hdfs_user"), config.get("hadoop", "hdfs_basedir"))
    elif args.driver == 'localfs':
        logging.info("Utilisation du système de fichiers local")
        data_driver = FilesystemAdaptor(config.get("localfs", "localfs_basedir"))
    else:
        raise ValueError("Bad value for the driver [%s]" % args.driver)

    # Loading job
    # Loading only a single job or all jobs
    try:
        query = {
            'name': args.job or re.compile('.*')
        }

        logging.info("Exécution de la requête [%s] sur la base MongoDB", str(query))
        cursor = meta_db.get_collection('meta').find(query)
        metaList.extend(cursor)
        cursor.close()
    except Exception as ex:
        logging.error("Une erreur est survenue lors de la récupération des metas (%s)" % str(ex))
        exit(1)

    if not metaList:
        logging.error("Aucune meta trouvée")
        exit(1)

    try:
        pipeline_executed_jobs_query = [{
            '$match': { 'status': re.compile('ok', re.IGNORECASE) }
        }, {
            '$group': {
                '_id': {
                    'meta': '$meta',
                    'module': '$module',
                    'status': '$status',
                },
                'timestamp': {
                    '$max': '$timestamp'
                },
            }
        }]
        logging.info("Exécution de l'aggrégaton [%s] sur la base MongoDB", str(pipeline_executed_jobs_query))
        cursor = meta_db.get_collection('meta_jobs').aggregate(pipeline_executed_jobs_query)
        jobList.extend(cursor)
        cursor.close()
    except Exception as ex:
        logging.error("Une erreur est survenue lors de la récupération des anciens jobs (%s)" % str(ex))
        exit(1)

    logging.info("Préparation du plan d'exécution")
    #print(jobList)
    m_now = datetime.datetime.now()
    def _filter(m_meta):
        for executedJob in jobList:
            if m_meta['name'] == executedJob['_id']['meta']:
                if executedJob['timestamp'] + datetime.timedelta(hours=m_meta['collect_interval']['hours'], minutes=m_meta['collect_interval']['minutes']) < m_now:
                    return True
                else:
                    return False
        # Job not already executed
        return True
    '''
    def m_test_time_ok(executedJob, timeObject):
        return executedJob['timestamp'] + datetime.timedelta(hours=timeObject['hours'], minutes=timeObject['minutes']) < m_now

    finalMetaList = filter(
        lambda m_meta: (
                any(m_meta['module'] == executedJob['_id']['module'] and m_test_time_ok(executedJob, m_meta['collect_interval']) for executedJob in jobList) or \
                not any(m_meta['module'] == executedJob['_id']['module'] for executedJob in jobList)
            )
        , metaList
    )
    '''

    #print(any(_meta['module'] == jobList[0] and m_test_time_ok(_meta, jobList[0]['timestamp']))

    # Loading unique modules
    for moduleName in set([meta['module'] for meta in metaList]):
        try:
            moduleDict[moduleName] = get_module(moduleName)
        except ModuleNotFoundError as ex:
            logging.error("Impossible de charger le module (%s)" % str(ex))
            exit(1)



    jobThreadList = []
    current_threads = []
    jobThreadListCurrentIndex = 0
    __MAX_CONCURRENT_THREADS = 10

    logging.info("Préparation de l'exécution des jobs")
    #for meta in metaList:
    for meta in filter(_filter, metaList):
        module = moduleDict[meta['module']]()
        module.setJobConfiguration(config)
        #meta['url'] = 'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=tcl_sytral.tclalertetrafic&SRSNAME=EPSG:4171&outputFormat=application/json; subtype=geojson&startIndex=0'
        module.setJobDescription(meta)
        module.setDataAdaptor(data_driver)
        jobThreadList.append(module)


    logging.info("Exécution des jobs (%d jobs à exécuter)" % len(jobThreadList))
    while jobThreadListCurrentIndex < len(jobThreadList):
        if len(current_threads) < __MAX_CONCURRENT_THREADS:
            # Running thread
            _thread = jobThreadList[jobThreadListCurrentIndex]
            logging.info("[Thread %00d/%00d]: %s" % (jobThreadListCurrentIndex+1, len(jobThreadList), _thread.name))
            _thread.start()
            current_threads.append(_thread)
            jobThreadListCurrentIndex += 1
        else:
            # Looking for a free thread
            for _thread in current_threads:
                if _thread.ident:   # Thread has started
                    _thread.join(0)
                    if not _thread.is_alive():
                        # Pushing report
                        if _thread.getReport():
                            meta_db.get_collection('meta_jobs').insert_one(_thread.getReport())
                        else:
                            logging.error("No report for thread [%s]" % _thread.name)
                        # Deleting of the current threads
                        current_threads.remove(_thread)
            time.sleep(0.1)

    # Waiting for the last threads
    while len(current_threads) > 0:
        for _thread in current_threads:
            if _thread.ident:   # Thread has started
                _thread.join(0)
                if not _thread.is_alive():
                    # Pushing report
                    if _thread.getReport():
                        meta_db.get_collection('meta_jobs').insert_one(_thread.getReport())
                    else:
                        logging.error("No report for thread [%s]" % _thread.name)
                    # Deleting of the current threads
                    current_threads.remove(_thread)
        time.sleep(0.1)


        #meta_db.get_collection('meta_jobs')







        '''
        m_start = datetime.datetime.now()
        report = {
            #"_id": "_____",
            "jobName": '%s_%s' % (meta['name'], datetime.datetime.now().strftime('%Y%m%d%H%M%S')),
            "meta": meta['name'],
            "timestamp": m_start,
            "batchDurationSeconds": None,  #  timedelta.total_seconds()
            "module": meta['module'],
            "extractedDataSize": None,
            "generatedDataSize": None,
            "status": None,
            "errorType": None,
            "errorDescription": None
        }
        try:
            module.setJobConfiguration(config)
            #meta['url'] = 'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=tcl_sytral.tclalertetrafic&SRSNAME=EPSG:4171&outputFormat=application/json; subtype=geojson&startIndex=0'
            module.setJobDescription(meta)
            module.setDataAdaptor(data_driver)
            extracted_data_size, generated_data_size = module.run()
            report['batchDurationSeconds'] = (datetime.datetime.now() - m_start).total_seconds()
            report['status'] = 'ok'
            report['extractedDataSize'] = extracted_data_size
            report['generatedDataSize'] = generated_data_size
            meta_db.get_collection('meta_jobs').insert_one(report)
            # Making statistics to MongoDB
            #module.setHdfsConnection('')
        except Exception as ex:
            logging.exception("Une erreur est survenue (%s; %s)" % (ex.__class__, str(ex)))
            report['status'] = 'ko'
            report['error_type'] = 'exception:%s' % ex.__class__
            report['error_description'] = str(ex)
            meta_db.get_collection('meta_jobs').insert_one(report)
    '''
