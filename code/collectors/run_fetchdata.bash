#!/bin/bash

M_PATH="$(dirname $0)"
if [ -z "${M_PATH}" ]; then M_PATH="."; fi

if [ ! -d .env ]; then
    bash "${M_PATH}/virtualenv_init.bash"
fi

source "${M_PATH}/.env/bin/activate"

python "${M_PATH}/source/fetch_data.py" "$@"
