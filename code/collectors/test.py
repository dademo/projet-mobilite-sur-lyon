import fastavro
import json
import io
import sys

schema = {
    "type" : "record",
    "name" : "adr_voie_lieu.adradresse",
    "fields" : [
        {
            "name" : "type",
            "type" : "string"
        },
        {
            "name" : "name",
            "type" : "string"
        },
        {
            "name" : "features",
            "type" : {
                "type" : "array",
                "items" : {
                    "name" : "feature",
                    "type" : "record",
                    "fields" : [
                        {
                            "name" : "type",
                            "type" : "string"
                        },
                        {
                            "name" : "properties",
                            "type" : {
                                "name" : "properties",
                                "type" : "record",
                                "fields" : [
                                    {
                                        "name" : "codefuv",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "commune",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "datecreation",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "gid",
                                        "type" : "int"
                                    },
                                    {
                                        "name" : "inseecommune",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "numero",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "voie",
                                        "type" : "string"
                                    }
                                ]
                            }
                        },
                        {
                            "name" : "geometry",
                            "type" : {
                                "name" : "geometry",
                                "type" : "record",
                                "fields" : [
                                    {
                                        "name" : "type",
                                        "type" : "string"
                                    },
                                    {
                                        "name" : "coordinates",
                                        "type" : {
                                            "type" : "array",
                                            "items" : "float"
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        }
    ]
}

data = None
with open('data.json', 'r') as data_buffer:
    data = json.load(data_buffer)

fastavro.validate(data, schema)
schema = fastavro.parse_schema(schema)
with io.BytesIO() as buffer:
    fastavro.writer(buffer, schema, [data])
    extracted_data_size = sys.getsizeof(data)
    generated_data_size = buffer.tell()
    print("%d <> %d" % (extracted_data_size, generated_data_size))