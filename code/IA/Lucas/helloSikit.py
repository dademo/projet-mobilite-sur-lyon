from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(random_state=0)
X = [[ 1,  2,  3],  # 2 samples, 3 features
     [11, 12, 13]]
y = [0, 1]  # classes of each sample
clf.fit(X, y)   

print (clf.predict(X))


from sklearn.preprocessing import StandardScaler
Z = [[0, 15],
     [1, -10]]
print (StandardScaler().fit(Z).transform(Z))