from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.datasets import load_iris
from sklearn.datasets import load_wine
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Create Pipeline 

pipe = make_pipeline(
    StandardScaler(),
    LogisticRegression(random_state=0)
)

#Load the iris dataset and split into train and test sets
print("Loading File...")
X, y = load_wine(return_X_y=True)
print("X = %s" % X)
print("Y = %s" % y)     
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
print("X_test = %s" % X_test )
print("Y_test = %s" % y_test)
print(X_test.__class__)
# Fit the pipeline

pipe.fit(X_train, y_train)



# Use it like any other estimator 

result = accuracy_score(pipe.predict(X_test), y_test)

print("Predict : ", pipe.predict(X_test))
print("Accuracy : ", result)