import json
import pandas as pd
import geopandas as gpd
from sklearn import preprocessing
from shapely.geometry import mapping, shape
from sklearn import decomposition
import matplotlib.pyplot as plt


#data = pd.read_json('.\data\dataset_pvo_patrimoine_voirie.pvostationvelov.json')
data2 = gpd.read_file('.\data\dataset_pvo_patrimoine_voirie.pvostationvelov.json')
#world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
decath = pd.read_csv('.\data\decathlon.txt', sep="\t")


#data_json = data.to_json()
#data_parse = json.loads(data_json)
#data_parse = json.load(data2)

#print(data_parse['features'])
print(data2.head())

print(data2['nom'].head())

print(data2['geometry'].head())

print('Point geo : ', data2['geometry'])

#print(world.head())
#world.plot()

my_data = data2.drop(columns=['nom', 'adresse1', 'adresse2', 'ouverte', 'achevement', 'commune', 'pole', 'code_insee'   ])

data_decath = decath.drop(['Points', 'Rank', 'Competition'], axis=1)

print(data_decath)

#print(my_data)

#X = my_data.values

X = data_decath.values

#std_scale = preprocessing.StandardScaler().fit(data2)
#X_scaled = std_scale.transform(data2)

std_scale = preprocessing.StandardScaler().fit(X)
X_scaled = std_scale.transform(X)


pca = decomposition.PCA(n_components=2)
pca.fit(X_scaled)

print(pca.explained_variance_ratio_)
print(pca.explained_variance_ratio_.sum())


X_projected = pca.transform(X_scaled)

plt.scatter(X_projected[:, 0], X_projected[:, 1],
        c=decath.get('Rank'))

plt.xlim([-5.5, 5.5])
plt.ylim([-4, 4])
plt.colorbar()

#plt.show()

pcs = pca.components_

for i, (x,y) in enumerate(zip(pcs[0, :], pcs[1, :])):
    plt.plot([0, x], [0, y], color = 'k')
    plt.text(x, y, decath.columns[i], fontsize='14')

plt.plot([-0.7, 0.7], [0, 0], color='grey', ls='--')

plt.plot([0, 0], [-0.7, 0.7], color='grey', ls='--')

plt.xlim([-0.7, 0.7])
plt.ylim([-0.7, 0.7])

plt.show()
