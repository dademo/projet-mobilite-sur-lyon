import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


house_data = pd.read_csv('house_data.csv')

#xtrain, xtest, ytrain, ytest = train_test_split(house_data['price'], house_data['surface'], train_size = 0.8)

plt.plot(house_data['price'], house_data['surface'], house_data['arrondissement'], 'ro', markersize=4)

X = np.matrix([np.ones(house_data.shape[0]), house_data['surface'].values]).T
y = np.matrix(house_data['price']).T

theta = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)

print(theta)