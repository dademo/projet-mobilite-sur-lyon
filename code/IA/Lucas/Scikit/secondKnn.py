from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from sklearn import neighbors
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

mnist = fetch_openml('mnist_784', version=1)
sample = np.random.randint(70000, size=5000)
data = mnist.data[sample]
target = mnist.target[sample]

xtrain, xtest, ytrain, ytest = train_test_split(data, target, train_size=0.8)

knn = neighbors.KNeighborsClassifier(n_neighbors=3)
knn.fit(xtrain, ytrain)

errors = []
for k in range(2,15):
    knn = neighbors.KNeighborsClassifier(k)
    errors.append(100*(1 - knn.fit(xtrain, ytrain).score(xtest, ytest)))
plt.plot(range(2,15), errors, 'o-')
plt.show()