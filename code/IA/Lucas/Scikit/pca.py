import json
import pandas as pd
import geopandas as gpd
from sklearn import preprocessing
from shapely.geometry import mapping, shape
from sklearn import decomposition
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn import datasets
import seaborn as sns
from sklearn.mixture import GaussianMixture



data2 = gpd.read_file('.\data\dataset_pvo_patrimoine_voirie.pvostationvelov.json')

iris = datasets.load_iris()

data = iris.data
target = iris.target

df = pd.DataFrame(data, columns=iris['feature_names'] )
model = PCA(n_components=2)

model.fit(iris.data)

reduc = model.transform(iris.data)

print('Valeur originel : ', iris.data[:5])
print('')
print('Valeur reduite :', reduc[:5])

print(df.head())

df['PCA1'] = reduc[:, 0]
df['PCA2'] = reduc[:, 1]
df['target'] = target
df['label'] = df.apply(lambda x: iris['target_names'][int(x.target)], axis=1)

print(df.head())

colors = ['violet', 'yellow', 'blue']

plt.scatter(df['PCA1'], df['PCA2'], c=[ colors[c] for c in df['target'] ]);
plt.xlabel('PCA1')
plt.ylabel('PCA2');

#plt.show()

model2 = GaussianMixture (n_components=3, covariance_type='full')

model2.fit(df[['PCA1', 'PCA2']])

groups = model2.predict(df[['PCA1', 'PCA2']])

df['group'] = groups
sns.lmplot("PCA1", "PCA2", data=df, hue='label', col='group', fit_reg=False);

plt.show()