import json
import pandas as pd
import geopandas as gpd
from sklearn import preprocessing
from shapely.geometry import mapping, shape
from sklearn import decomposition
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn import datasets
import seaborn as sns
from sklearn.mixture import GaussianMixture



velov = gpd.read_file('.\data\dataset_pvo_patrimoine_voirie.pvostationvelov.json')

model = PCA(n_components=2)

model.fit(velov)