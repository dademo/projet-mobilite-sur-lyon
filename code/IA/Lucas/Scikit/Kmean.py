#Import des librairies
import pandas as pd
import numpy as np
import sklearn.metrics as sm
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import datasets
from sklearn import preprocessing


iris = datasets.load_iris()

#velov = pd.read_json('.\data\dataset_pvo_patrimoine_voirie.pvostationvelov.json')

#print(velov)

#Stocker les données en tant que DataFrame
x = pd.DataFrame(iris.data)
#X = velov.values

std_scale = preprocessing.StandardScaler().fit(x)
X_scaled = std_scale.transform(x)

#Définir nom de colonnes
x.columns=['Sepal_Length', 'Sepal_width', 'Petal_Length', 'Petal_width']
y= pd.DataFrame(iris.target)

#print("Y : ", y.)

#Cluster K-Means

model=KMeans(n_clusters=3)

#Adapter le modèle 
model.fit(x)

print(model.labels_)

plt.scatter(x.Petal_Length, x.Petal_width)

colormap=np.array(['Red','green','blue'])
plt.scatter(x.Petal_Length, x.Petal_width,c=colormap[y.Target],s=40)
plt.scatter(x.Petal_Length, x.Petal_width,c=colormap[model.labels_],s=40)

plt.show()

