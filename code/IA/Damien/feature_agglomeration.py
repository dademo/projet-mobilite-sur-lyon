#!/bin/env python3

## from: https://scikit-learn.org/stable/auto_examples/cluster/plot_digits_agglomeration.html#sphx-glr-auto-examples-cluster-plot-digits-agglomeration-py

# Retgroupes les données suivant des correspondances,
# trace des liens entre les données
# et produit une donnée finale


# Code source: Gaël Varoquaux
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets, cluster
from sklearn.feature_extraction.image import grid_to_graph

## Chargement des images
digits = datasets.load_digits()
images = digits.images
## Passage à un tableau unidimensionnel
X = np.reshape(images, (len(images), -1))
print("%d images" % len(images))
# https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.image.grid_to_graph.html
# Recherche de liens entre des données d'image
# stockées dans un tableau et retourne les liens entre elles
connectivity = grid_to_graph(*images[0].shape)

# Pour relier des données, il faut créer une connectivité entre elles
# de manière à pouvoir les regrouper

# https://scikit-learn.org/stable/modules/generated/sklearn.cluster.FeatureAgglomeration.html#sklearn.cluster.FeatureAgglomeration
# https://scikit-learn.org/stable/modules/generated/sklearn.cluster.AgglomerativeClustering.html
# Groupe les données entre elles (suivant une distnace minimale)
#  * linkage => ward :: variance minimale entre les éléments
#  * 32 clusters => 32 groupes à former
#  NOTE: Avec plusieurs nombres de clusters -> moins il y a de clusters,
#        plus la données est "grossière" et moins précise
agglo = cluster.FeatureAgglomeration(connectivity=connectivity,
                                     n_clusters=32)

# Entraînement des données
agglo.fit(X)
# Groupempent des données ?
X_reduced = agglo.transform(X)

# Dégroupement des données, on obtenient une version "générique"
X_restored = agglo.inverse_transform(X_reduced)
images_restored = np.reshape(X_restored, images.shape)
plt.figure(1, figsize=(4, 3.5))
plt.clf()
plt.subplots_adjust(left=.01, right=.99, bottom=.01, top=.91)
for i in range(4):
    plt.subplot(3, 4, i + 1)
    plt.imshow(images[i], cmap=plt.cm.gray, vmax=16, interpolation='nearest')
    plt.xticks(())
    plt.yticks(())
    if i == 1:
        plt.title('Original data')
    plt.subplot(3, 4, 4 + i + 1)
    plt.imshow(images_restored[i], cmap=plt.cm.gray, vmax=16,
               interpolation='nearest')
    if i == 1:
        plt.title('Agglomerated data')
    plt.xticks(())
    plt.yticks(())

plt.subplot(3, 4, 10)
plt.imshow(np.reshape(agglo.labels_, images[0].shape),
           interpolation='nearest', cmap=plt.cm.nipy_spectral)
plt.xticks(())
plt.yticks(())
plt.title('Labels')
plt.show()