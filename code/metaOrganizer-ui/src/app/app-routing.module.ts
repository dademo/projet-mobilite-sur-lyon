import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MetaListComponent } from './components/meta-list/meta-list.component';
import { MetaShowComponent } from './components/meta-show/meta-show.component';
import { MetaEditComponent } from './components/meta-edit/meta-edit.component';
import { MetaAddComponent } from './components/meta-add/meta-add.component'

const routes: Routes = [
  { path: 'meta', component: MetaListComponent },
  { path: 'meta/add', component: MetaAddComponent },
  { path: 'meta/edit/:id', component: MetaEditComponent },
  { path: 'meta/view/:id', component: MetaShowComponent },
  { path: '',
    redirectTo: '/meta',
    pathMatch: 'full'
  },
  { path: '**',
    redirectTo: '/meta',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
