export interface MetaDescriptorShort {
    
    _id: string;
    name: string;
    description: string;
}