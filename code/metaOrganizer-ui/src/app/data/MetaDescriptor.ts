import { Time } from '@angular/common';
import { MetaDescriptorShort } from './MetaDescriptorShort'

export interface ServiceType {
    value: string;
    viewValue: string;
}

export const AllServiceTypes: ServiceType[] = [
    { value: "JSON", viewValue: "JSON" },
    { value: "GEOJSON", viewValue: "GeoJSON" },
]

export interface MetaDescriptor extends MetaDescriptorShort {

    fullname: string;
    service_type: string;
    url: string;
    crs: string;
    collect_interval: Time;
    module: string;
    data_source: string;
    data_source_ref: string;
    privacy: string;
    license: string;
    schema: any;
}

    /*
cat << EOF_FIELD | while read line; do cat << EOF_CODE
fullname
url
collect_interval
module
privacy
license
data_source
data_source_ref
schema
EOF_FIELD
public get${line}(): string {
        return this.${line};
}

public set${line}(${line}: string): void {
        this.${line} = ${line};
}

EOF_CODE
done
    */
