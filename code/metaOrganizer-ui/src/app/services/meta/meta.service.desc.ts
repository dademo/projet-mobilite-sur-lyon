import { Observable, Observer } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ObserversModule } from '@angular/cdk/observers';
import { MetaDescriptor, MetaDescriptorShort } from 'src/app/data/data';

export interface MetaServiceInterface {
    /* CRUD MetaDescriptorShort */
    getMetaDescriptorShort(): Observable<HttpResponse<MetaDescriptorShort[] | string | object>> | null;
    /* CRUD MetaDescriptor */
    getMetaDescriptor(_id: string): Observable<HttpResponse<MetaDescriptor | string | object>> | null;
    addMetaDescriptor(item: MetaDescriptor): Observable<HttpResponse<MetaDescriptor | string | object>> | null;
    updateMetaDescriptor(item: MetaDescriptorShort): Observable<HttpResponse<MetaDescriptor | string | object>> | null;
    deleteMetaDescriptor(_id: string): Observable<HttpResponse<any>> | null;
    /* -=[]=- */
    setFilter(filter: string): void;
    getFilter(): string;
    doFilter(): MetaDescriptorShort[];
    setCache(cache: MetaDescriptorShort[] | string | object): void;
    getCache(): MetaDescriptorShort[] | string | object;
    refresh(): void;
}