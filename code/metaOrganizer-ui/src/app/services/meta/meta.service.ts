import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Time } from '@angular/common';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { MetaServiceInterface } from './meta.service.desc';
import { MetaDescriptor, MetaDescriptorShort } from 'src/app/data/data';
import { delay } from 'src/app/tools/delay';

const fakeBody: MetaDescriptor = {
  _id:              "123",
  name:             "Hello Descriptor 1",
  description:      "DESCRIPTION 1",
  fullname:         "",
  service_type:     "geojson",
  url:              "https://perdu.com",
  crs:              "EPSG:4171",
  collect_interval: <Time>{ hours: 1, minutes: 30 },
  module:           "fr.asso.ort.lyon.Octogone.GrandLyon",
  data_source:      "Grand Lyon",
  data_source_ref:  "https://perdu.com",
  privacy:          "Publique",
  license:          "License Ouverte",
  schema:           {"value": "Hello World", "list": [1, 2, 3]}
};

const fakeBodyShort: MetaDescriptorShort[] = [
  <MetaDescriptorShort>{_id: "azerty1", name: "Hello Descriptor 1", description: "DESCRIPTION 1"},
  <MetaDescriptorShort>{_id: "azerty2", name: "Hello Descriptor 2", description: "DESCRIPTION 2"},
  <MetaDescriptorShort>{_id: "azerty3", name: "Hello Descriptor 3", description: "DESCRIPTION 3"}
];

@Injectable({
  providedIn: 'root'
})
export class MetaService implements MetaServiceInterface {

  private filter: RegExp = new RegExp("");
  private cache: HttpResponse<MetaDescriptorShort[] | string | object>;

  private metaDescriptorShort: BehaviorSubject<HttpResponse<MetaDescriptorShort[] | string | object>> = new BehaviorSubject(new HttpResponse({ body: fakeBodyShort, status: 200 }));

  constructor(private httpClient: HttpClient) { }

  public getMetaDescriptorShort(): Observable<HttpResponse<MetaDescriptorShort[] | string | object>> {
    /*
    let toReturn: Observable<HttpResponse<MetaDescriptor | string | object>> = new Observable(observable => {
      this.metaDescriptorShort.subscribe(response => {
        observable.next(response);
      });
    });
    */
    of(new HttpResponse<MetaDescriptorShort[]>({body: fakeBodyShort, status: 200})).subscribe(response => {
      this.setCache(response);
      this.metaDescriptorShort.next(response);
    });
    /*
    return new Observable<MetaDescriptorShort[]>(function subscribe (subscriber) {
      subscriber.next([
        new MetaDescriptorShort("azerty", "Hello Descriptor", "DESCRIPTION")
      ]);
      subscriber.complete();
    });
    */
   return this.metaDescriptorShort;
  }

  public getMetaDescriptor(_id: string): Observable<HttpResponse<MetaDescriptor | string | object>> {
    return of(
      new HttpResponse({ body: fakeBody, status: 200})
    );
  }
  
  public addMetaDescriptor(item: MetaDescriptor): Observable<HttpResponse<MetaDescriptor | string | object>> {
    return of(
      new HttpResponse({body: fakeBody, status: 200})
    );
  }

  public updateMetaDescriptor(item: MetaDescriptorShort): Observable<HttpResponse<MetaDescriptor | string | object>> {
    return of(
      new HttpResponse({body: fakeBody, status: 200})
    );
  }

  public deleteMetaDescriptor(_id: string): Observable<HttpResponse<any>> {
    return of(
      new HttpResponse({status: 204})
    );
  }

  
  public setFilter(filter: string): void {
    this.filter = new RegExp(filter, "i");
    this.refresh();
  }

  public getFilter(): string {
    return this.filter.source;
  }

  public setCache(cache: HttpResponse<MetaDescriptorShort[] | string | object>): void {
    this.cache = cache;
  }

  public getCache(): HttpResponse<MetaDescriptorShort[] | string | object> {
    return this.cache;
  }

  public doFilter(): MetaDescriptorShort[] {
    if(this.getCache()) {
      if(this.getCache().status == 200) {
        return (this.getCache().body as MetaDescriptorShort[]).filter(
          (element, index, array) => { return this.filter.test(element.name); }
        );
      }
    }
    return [];
  }

  public refresh(): void {
    if(this.metaDescriptorShort != null) {
      this.metaDescriptorShort.next(
        new HttpResponse({ body: this.doFilter(), status: 200 })
      );
    }
  }
}
