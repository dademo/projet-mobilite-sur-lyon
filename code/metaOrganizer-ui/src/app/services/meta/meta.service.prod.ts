import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { MetaServiceInterface } from './meta.service.desc';
import { MetaDescriptor, MetaDescriptorShort } from 'src/app/data/data';

@Injectable({
  providedIn: 'root'
})
export class MetaService implements MetaServiceInterface {

  private httpHeaders: HttpHeaders = new HttpHeaders();

  private filter: RegExp = new RegExp("");
  private cache: HttpResponse<MetaDescriptorShort[] | string | object>;

  private metaDescriptorShort: BehaviorSubject<HttpResponse<MetaDescriptorShort[] | string | object>> = new BehaviorSubject(new HttpResponse({ body: [], status: 200 }));

  constructor(private httpClient: HttpClient) {
    /* Constructing HttpHeaders */
    this.httpHeaders.append('Content-Type', 'application/json');  /* We push JSON */
    this.httpHeaders.append('Accept', 'application/json');        /* Want JSON */
  }

  public getMetaDescriptorShort(): Observable<HttpResponse<MetaDescriptor | string | object>> | null {
    /* API Call */
    this.httpClient.get<MetaDescriptorShort[] | string | object>(
      '/api/metadescriptor/',
      { headers: this.httpHeaders, observe: "response" }
    ).subscribe(response => {
      this.setCache(response);
      this.metaDescriptorShort.next(response);
    });

    return this.metaDescriptorShort;
  }

  public getMetaDescriptor(_id: string): Observable<HttpResponse<MetaDescriptor | string | object>> | null {
    return this.httpClient.get<MetaDescriptor[] | string | object>(
      '/api/metadescriptor/' + _id,
      { headers: this.httpHeaders, observe: "response" }
    );
  }
  
  public addMetaDescriptor(item: MetaDescriptor): Observable<HttpResponse<MetaDescriptor | string | object>> | null {
    return this.httpClient.post<MetaDescriptor[] | string | object>(
      '/api/metadescriptor/',
      item,
      { headers: this.httpHeaders, observe: "response" }
    );
  }

  public updateMetaDescriptor(item: MetaDescriptorShort): Observable<HttpResponse<MetaDescriptor | string | object>> | null {
    return this.httpClient.put<MetaDescriptor[] | string | object>(
      '/api/metadescriptor/' + item._id,
      item,
      { headers: this.httpHeaders, observe: "response" }
    );
  }

  public deleteMetaDescriptor(_id: string): Observable<HttpResponse<any>> | null {
    return this.httpClient.delete<MetaDescriptor[] | string | object>(
      '/api/metadescriptor/' + _id,
      { headers: this.httpHeaders, observe: "response" }
    );
  }
  
  public setFilter(filter: string): void {
    this.filter = new RegExp(filter, "i");
    this.refresh();
  }

  public getFilter(): string {
    return this.filter.source;
  }

  public doFilter(): MetaDescriptorShort[] {
    if(this.getCache()) {
      if(this.getCache().status == 200) {
        return (this.getCache().body as MetaDescriptorShort[]).filter(
          (element, index, array) => { return this.filter.test(element.name); }
        );
      }
    }
    return [];
  }

  public setCache(cache: HttpResponse<MetaDescriptorShort[] | string | object>): void {
    this.cache = cache;
  }

  public getCache(): HttpResponse<MetaDescriptorShort[] | string | object> {
    return this.cache;
  }

  public refresh(): void {

  }
}
