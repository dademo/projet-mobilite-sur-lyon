import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Title } from '@angular/platform-browser';

/* https://stackoverflow.com/questions/51286357/angular-display-title-of-selected-component */

export interface LoaderStatus {
  mode: 'determinate' | 'indeterminate' | 'buffer' | 'query';
  value?: number | null;
  bufferValue?: number | null;
  value$?: Observable<number> | null;
  bufferValue$?: Observable<number> | null;
}

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private appTitle = new BehaviorSubject<string>('MetaOrganizer');
  private appTitle$ = this.appTitle.asObservable();

  private hasBack = new BehaviorSubject<boolean>(false);
  private hasBack$ = this.hasBack.asObservable();

  private backLocation: string = "";

  private loaderStatus = new BehaviorSubject<LoaderStatus>(null);
  private loaderStatus$ = this.loaderStatus.asObservable();

  

  constructor(
    protected appTitleService: Title
  ) { }

  public setAppTitle(appTitle: string) {
    this.appTitle.next(appTitle);
    this.appTitleService.setTitle(appTitle);
  }

  public getAppTitle(): Observable<string> {
    return this.appTitle$;
  }

  public setBackLocation(location: string|null) {
    this.backLocation = location;
    this.setHasBack(location !== null);
  }

  public getBackLocation(): string|null {
    return this.backLocation;
  }

  private setHasBack(hasBack: boolean) {
    this.hasBack.next(hasBack);
  }

  public getHasBack(): Observable<boolean> {
    return this.hasBack$;
  }

  public setLoaderStatus(loaderStatus: LoaderStatus) {
    this.loaderStatus.next(loaderStatus);
  }

  public getLoaderStatus() {
    return this.loaderStatus$;
  }

}
