import { Component, OnInit, Input } from '@angular/core';
import { ValidatorFn, Validators, AbstractControl, FormControl, FormGroup, ValidationErrors } from '@angular/forms';
import { element } from 'protractor';
import { Observable, BehaviorSubject } from 'rxjs';

export type FormInput = {
  name: string,
  // https://material.angular.io/components/input/overview
  inputType: "color" | "date" | "datetime-local" | "email" | "month" | "number" | "password" |
              "search" | "tel" | "text" | "textarea" | "time" | "url" | "week" | null,
  required: boolean,
  /* https://stackoverflow.com/questions/15877362/declare-and-initialize-a-dictionary-in-typescript */
  validators?: { [Key: string]: string },
  placeholder?: string,
  formFieldName?: string
}


/* https://angular.io/api/forms/FormControlName */

@Component({
  selector: 'app-auto-form',
  templateUrl: './auto-form.component.html',
  styleUrls: ['./auto-form.component.css']
})
export class AutoFormComponent implements OnInit {

  @Input()
  public formDesc: FormInput[];

  @Input()
  public data: object;

  @Input()
  public readonly: boolean = false;

  public formgroup: FormGroup = null;

  /*
  private form_data: FormInput[] = [
    { key: "_id", required: false, inputType: null },
    { key: "name", required: true, inputType: "text" },
    { key: "description", required: false, inputType: "textarea" },
  ];
  */


  constructor() { console.log("__construct__"); }

  ngOnInit() {
    if(this.formDesc === null) {
      throw new Error("formDesc ne doît pas être [null]");
    }

    let formgroup = {};

    this.formDesc.forEach(element => {
      let objectFieldValue = (this.data && element.name in this.data)? this.data[element.name] : null;
      //let validators = element.validators || [];
      let validators = [];
      if(element.validators) {
        Object.entries(element.validators).forEach(value => {
          validators.push(value[1]);
        })
      }
      if(element.required) {
        validators.push(Validators.required);
      }
      let formControl = new FormControl(objectFieldValue, validators)
      formgroup[element.formFieldName || element.name] = formControl;
    })
    this.formgroup = new FormGroup(formgroup);
  }

  public formGroupFromDesc(desc: FormInput): FormGroup {
    let formGroup: any = {};

    Object.entries(this.formDesc).forEach(element => {
      let form_data;
      let validators;
      this.formDesc.forEach((element) => {
        if(element.name === element[0]) {
          form_data = element;
        }
      });
      validators = form_data.validators || [];
      if(form_data.required) {
        validators.append(Validators.required);
      }
      formGroup[element[0]] = new FormControl(element[1], validators);
    });

    return new FormGroup(formGroup);
  }

  public getEnabledFormDesc(): FormInput[] {
    return this.formDesc.filter(element => element.inputType !== null);
  }

  public test(): void {
    console.log(this.formgroup);
  }

  /* https://gist.github.com/JohannesHoppe/e8d07d63fc345a5fdfdf4fc4989ef2e4 */
  public inputErrors(controlKey: string): string[] | null {
    /* FormControl */
    if(this.formgroup) {
      let input = this.formgroup.get(controlKey);
      if(input.status === 'INVALID') {
        console.log(input.errors);
        if(input && input.errors) {
          let toReturn = [];
          Object.entries(input.errors).forEach(element => {
            //toReturn.append()
            console.log(element);
            //toReturn.push(element[0]);
          });
          return toReturn;
        }
      }
    }
    return null;
  }

  public onSubmit(formValue: any): void {
    
  }
}
