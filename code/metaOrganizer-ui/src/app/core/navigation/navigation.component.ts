import { Component, Input, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { Router, NavigationStart, RouterOutlet } from '@angular/router';
import { trigger } from '@angular/animations';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/base-component/base-component.component';
import { AppService, LoaderStatus } from 'src/app/services/app-service/app-service.service';
import { fadeIn, fadeOut, progressIn, progressOut } from '../transitions';

/* ng generate @angular/material:material-nav --name=navigation */

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  animations: [
    trigger('routeAnimations', [
      fadeIn,
      fadeOut,
    ]),
    trigger('onProgress', [
      progressIn,
      progressOut
    ])
  ]
})
export class NavigationComponent extends BaseComponent implements OnInit {

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  public hasBack$: Observable<boolean> = this.appService.getHasBack();
  public loaderStatus$: Observable<LoaderStatus> = this.appService.getLoaderStatus();
  public lastUrl: string | null = null;

  constructor(
    private breakpointObserver: BreakpointObserver,
    protected appService:AppService,
    //private location: Location,
    private router: Router
  ) {
    super(appService);
    this.router.events.subscribe(event => {
      if(event instanceof NavigationStart) {
        if(this.lastUrl && event.url != this.lastUrl) {
          /* Resetting interface */
          this.appService.setAppTitle("Chargement ...");
          this.appService.setBackLocation(null);
          this.appService.setLoaderStatus(null);
        }
        this.lastUrl = event.url;
      }
    })
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public onBack() {
    this.router.navigateByUrl(this.appService.getBackLocation());
  }

  public onLogout() {
    window.location.href="/logout";
  }

  /* https://angular.io/guide/route-animations */
  public prepareRoute(outlet: RouterOutlet): boolean {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

}
