import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorPopupDialog } from './error-popup.dialog';

describe('ErrorPopupDialog', () => {
  let component: ErrorPopupDialog;
  let fixture: ComponentFixture<ErrorPopupDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorPopupDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorPopupDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
