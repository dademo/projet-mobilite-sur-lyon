import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.dialog.html',
  styleUrls: ['./error-popup.dialog.css']
})
export class ErrorPopupDialog implements OnInit {

  public message: string = "";
  public errorList: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<ErrorPopupDialog>,
    @Inject(MAT_DIALOG_DATA) public data: HttpErrorResponse) {
      /* Generating error string */
      if(typeof this.data.error == "string") {
        this.message = this.data.error;
      } else {
        this.message = this.data.error.message;
        if(this.data.error.errors){
          this.data.error.errors.forEach(error => {
            this.errorList.push("[" + (error.field || "") + "] " + (error.defaultMessage || ""));
          });
        }
      }
      //
    }

  ngOnInit() {
  }

  onConfirmClick(): void {
    this.dialogRef.close();
  }

}
