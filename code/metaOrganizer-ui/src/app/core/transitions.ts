import { transition, style, animate } from "@angular/animations";

/* https://stackblitz.com/edit/angular-animation-fade-in */
const fadeIn = transition(':enter', [style({ opacity: 0 }), animate('250ms', style({ opacity: 1 }))]);
const fadeOut = transition(':leave', [style({ opacity: 1 }), animate('250ms', style({ opacity: 0 }))]);

const progressIn = transition(':enter', [style({ height: '0px' }), animate('250ms', style({ height: '4px' }))]);
const progressOut = transition(':leave', [style({ height: '4px' }), animate('250ms', style({ height: '0px' }))]);

const blurIn = transition(':enter', [style({ filter: 'blur(4px)' }), animate('250ms', style({ filter: 'blur(0px)' }))]);
const blurOut = transition(':leave', [style({ filter: 'blur(0px)' }), animate('250ms', style({ filter: 'blur(4px)' }))]);

export {
    fadeIn,
    fadeOut,
    progressIn,
    progressOut,
    blurIn,
    blurOut
}