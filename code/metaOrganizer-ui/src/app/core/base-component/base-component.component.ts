import { Component, OnInit } from '@angular/core';
import { AppService, LoaderStatus } from 'src/app/services/app-service/app-service.service';

/* https://stackoverflow.com/questions/51286357/angular-display-title-of-selected-component */

@Component({
  selector: 'app-base-component',
  templateUrl: './base-component.component.html',
  styleUrls: ['./base-component.component.css']
})
export class BaseComponent implements OnInit {

  public appTitle: String;
  public hasBack: boolean = false;
  public loaderStatus: LoaderStatus = null;

  constructor(
    protected appService: AppService
  ) {
    this.appService.setLoaderStatus(null);
    this.appService.setBackLocation(null);
    this.appService.getAppTitle().subscribe(appTitle => this.appTitle = appTitle);
    this.appService.getHasBack().subscribe(hasBack => this.hasBack = hasBack);
    this.appService.getLoaderStatus().subscribe(loaderStatus => this.loaderStatus = loaderStatus);
  }

  ngOnInit() {
  }

}
