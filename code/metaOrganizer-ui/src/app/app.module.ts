import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DeviceDetectorModule } from 'ngx-device-detector';

/* Design */
import { MatToolbarModule } from '@angular/material/toolbar';
import { NavigationComponent } from './core/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BaseComponent } from './core/base-component/base-component.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field'; 
import { MatInputModule, MatSelectModule, MatSnackBarModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip'; 
import { MatDialogModule } from '@angular/material/dialog'; 
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'; 
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { DatePipe } from '@angular/common';

/* Components */
import { MetaListComponent } from './components/meta-list/meta-list.component';
import { MetaShowComponent } from './components/meta-show/meta-show.component';
import { MetaEditComponent } from './components/meta-edit/meta-edit.component';

/* Dialogs */
import { MetaListComponentOnDeleteDialog } from 'src/app/components/meta-list/meta-list.component';
import { MetaAddComponent } from './components/meta-add/meta-add.component';
import { AutoFormComponent } from './core/auto-form/auto-form.component';

import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { ErrorPopupDialog } from './core/error-popup/error-popup.dialog';

/**
 * Import specific languages to avoid importing everything
 * The following will lazy load highlight.js core script (~9.6KB) + the selected languages bundle (each lang. ~1kb)
 */
export function getHighlightLanguages() {
  return {
    json: () => import('highlight.js/lib/languages/json')
  };
}


@NgModule({
  declarations: [
    AppComponent,
    MetaListComponent,
    MetaShowComponent,
    NavigationComponent,
    BaseComponent,
    MetaListComponentOnDeleteDialog,
    MetaShowComponent,
    MetaEditComponent,
    MetaAddComponent,
    AutoFormComponent,
    ErrorPopupDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    DeviceDetectorModule.forRoot(),
    //
    HighlightModule
  ],
  entryComponents: [
    MetaListComponentOnDeleteDialog,
    ErrorPopupDialog
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        languages: getHighlightLanguages()
      }
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
