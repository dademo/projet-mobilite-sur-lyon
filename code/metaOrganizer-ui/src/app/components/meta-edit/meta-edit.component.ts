import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { trigger } from '@angular/animations';
import { Observable } from 'rxjs';
import { BaseComponent } from 'src/app/core/base-component/base-component.component';
import { MetaDescriptor, ServiceType, AllServiceTypes } from 'src/app/data/data';
import { AppService } from 'src/app/services/app-service/app-service.service';
import { MetaService } from 'src/app/services/meta/meta.service';
import { fadeIn, fadeOut } from 'src/app/core/transitions';
import { delay } from 'src/app/tools/delay';
import { HttpResponse } from '@angular/common/http';
import { ErrorPopupDialog } from 'src/app/core/error-popup/error-popup.dialog';

@Component({
  selector: 'app-meta-edit',
  templateUrl: './meta-edit.component.html',
  styleUrls: ['./meta-edit.component.css'],
  /* http://animationsftw.in */
  /* https://angular.io/guide/transition-and-triggers#boolean-values-in-transitions */
  animations: [
    trigger('onLoad', [
      fadeIn,
      fadeOut
    ])
  ]
})
export class MetaEditComponent extends BaseComponent implements OnInit {

  public metaDescriptor$: Observable<HttpResponse<MetaDescriptor | string | object>> = null;
  public metaDescriptor: MetaDescriptor = null;

  public formgroup: FormGroup = null;

  public AllServiceTypes: ServiceType[] = AllServiceTypes;

  
  constructor(
    protected appService: AppService,
    private metaService: MetaService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private datePipe: DatePipe) {
      super(appService);
      this.appService.setLoaderStatus({ mode: "query" });

      this.formgroup = new FormGroup({
        _id: new FormControl(null, [ Validators.required ]),
        name: new FormControl(null, [ Validators.required ]),
        description: new FormControl(null),
        fullname: new FormControl(null),
        service_type: new FormControl(null, [ Validators.required ]),
        url: new FormControl(null, [ Validators.required ]),
        crs: new FormControl(null),
        collect_interval: new FormControl(null, [ Validators.required ]),
        module: new FormControl(null, [ Validators.required ]),
        data_source: new FormControl(null, [ Validators.required ]),
        data_source_ref: new FormControl(null, [ Validators.required ]),
        privacy: new FormControl(null, [ Validators.required ]),
        license: new FormControl(null, [ Validators.required ]),
        schema: new FormControl(null, [ Validators.required, (control: AbstractControl): ValidationErrors => {
          try {
            JSON.parse(control.value);
            return null;
          } catch (exception) { /* SyntaxError */
            return {'formatError': {value: control.value}}
          }
        } ])
      });

      this.metaDescriptor$ = metaService.getMetaDescriptor(this.route.snapshot.params.id);
      if(this.metaDescriptor$) {
        this.metaDescriptor$.subscribe(response => {
          if(response)
          {
            if(response.status === 200) {
              this.metaDescriptor = response.body as MetaDescriptor;
              this.appService.setAppTitle("[Edition] " + this.metaDescriptor.name);
              this.appService.setLoaderStatus(null);
  
              /* Setting values */
              Object.keys(this.metaDescriptor).forEach(objKey => {
                let input = this.formgroup.get(objKey);
                if(input) {
                  /* https://angular.io/api/common/Time */
                  if(this.metaDescriptor[objKey].hours !== undefined && this.metaDescriptor[objKey].minutes !== undefined) {
                    let d = new Date();
                    d.setHours(this.metaDescriptor[objKey].hours);
                    d.setMinutes(this.metaDescriptor[objKey].minutes);
                    input.setValue(this.datePipe.transform(d, 'hh:mm'));
                  } else if (this.metaDescriptor[objKey] instanceof Object) {
                    input.setValue(JSON.stringify(this.metaDescriptor[objKey], null, 4));
                  } else {
                    input.setValue(this.metaDescriptor[objKey] || '<vide>');
                  }
                }
              });
            } else {
              /* Handling error */
              let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger la meta", "Compris", {
                duration: 3000
              });
            }
          } else {
            /* Handling error */
            let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger la meta", "Compris", {
              duration: 3000
            });
          }
        }, error => {
          let dialogRef = this.dialog.open(ErrorPopupDialog, {
            minWidth: '30%',
            maxWidth: '60%',
            data: error
          });
        });
      } else {
        // this.metaDescriptor$
        let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger la meta", "Compris", {
          duration: 3000
        });
        this.router.navigateByUrl('/meta');
        /*
        snackBar.onAction().subscribe(() => {
          console.log("snackbar -> onAction");
        });
        */
      }
    }

  ngOnInit() {
    super.ngOnInit();
    this.appService.setBackLocation("/meta/view/" + this.route.snapshot.params.id);
  }

  public onSubmit(formValue: any): void {
    if(!(this.formgroup.invalid || this.formgroup.pending)) {
      try {
        /* Update JSON schema */
        let schema = JSON.parse(formValue.schema);
        formValue.schema = schema;

        /* Update collect_interval */
        let collect_interval = new RegExp('([0-9]{1,2}):([0-9]{1,2})').exec(formValue.collect_interval);
        if(collect_interval) {
          formValue.collect_interval = {
            hours: parseInt(collect_interval[1]),
            minutes:parseInt(collect_interval[2])
          }
        } else {
          return;
        }

        let updater = this.metaService.updateMetaDescriptor(formValue);
        this.appService.setLoaderStatus({mode: "query"});
        if(updater) {
          updater.subscribe(response => {
            if(response) {
              if(response.status == 200) {
                this.snackBar.open("La meta a bien été mise à jour", "Compris", {
                  duration: 3000
                });
                this.router.navigateByUrl('/meta/view/' + this.route.snapshot.params.id);
              } else {
                /* Handling error */
                this.snackBar.open("Une erreur est survenue, impossible de mettre à jour la meta", "Compris", {
                  duration: 3000
                });
              }
            } else {
              /* Handling error */
              this.snackBar.open("Une erreur est survenue, impossible de mettre à jour la meta", "Compris", {
                duration: 3000
              });
            }
          }, error => {
            let dialogRef = this.dialog.open(ErrorPopupDialog, {
              minWidth: '30%',
              maxWidth: '60%',
              data: error
            });
          });
        } else {
          this.snackBar.open("Une erreur inconnue est survenue, impossible de mettre à jour la meta", "Compris", {
            duration: 3000
          });
        }

      } catch (exception) { /* SyntaxError */
        /* Nothing */
      }
    }
  }

}
