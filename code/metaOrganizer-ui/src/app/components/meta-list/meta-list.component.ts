import { Component, OnInit, Inject } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { Router } from '@angular/router';
import { trigger } from '@angular/animations';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AppService } from 'src/app/services/app-service/app-service.service';
import { MetaService } from 'src/app/services/meta/meta.service';
import { BaseComponent } from 'src/app/core/base-component/base-component.component';
import { MetaDescriptorShort } from 'src/app/data/data';

import { delay } from 'src/app/tools/delay';
import { fadeIn, fadeOut } from 'src/app/core/transitions';
import { MatSnackBar } from '@angular/material';
import { HttpResponse } from '@angular/common/http';
import { ErrorPopupDialog } from 'src/app/core/error-popup/error-popup.dialog';

/* @angular/material:dashboard */

@Component({
  selector: 'app-meta-list',
  templateUrl: './meta-list.component.html',
  styleUrls: ['./meta-list.component.css'],
  animations: [
    trigger('onLoad', [
      fadeIn,
      fadeOut
    ]),
    trigger('onLoadSearch', [
      fadeIn,
      fadeOut
    ])
  ]
})
export class MetaListComponent extends BaseComponent implements OnInit {

  public m_metas$: Observable<HttpResponse<MetaDescriptorShort[] | string | object>> = this.metaService.getMetaDescriptorShort();
  public metas$: BehaviorSubject<MetaDescriptorShort[]> = new BehaviorSubject(null);

  public searchError: string = null;
  public searchStateMatcher: SearchErrorStateMatcher = new SearchErrorStateMatcher(this);

 public nCardsWidth: number = 2;


  //constructor(private breakpointObserver: BreakpointObserver) {}
  constructor(
    protected appService: AppService,
    private metaService: MetaService,
    private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
    private deviceService: DeviceDetectorService,
    private snackBar: MatSnackBar,
    private router: Router) {
      
    super(appService);

    this.appService.setLoaderStatus({mode: "query"});

    this.m_metas$ = this.metaService.getMetaDescriptorShort();
    if(this.m_metas$) {
      this.m_metas$.subscribe(response => {
        if(response.status == 200) {
          this.appService.setLoaderStatus(null);
          this.appService.setAppTitle("Liste des metas");
          this.metas$.next(response.body as MetaDescriptorShort[]);  // Will be the final value
        } else {
          /* Handling error */
          let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger les metas", "Compris", {
            duration: 3000
          });
        }
      }, error => {
        let dialogRef = this.dialog.open(ErrorPopupDialog, {
          minWidth: '30%',
          maxWidth: '60%',
          data: error
        });
      });
    } else {
      let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger les metas", "Compris", {
        duration: 3000
      });
    }

    let defs;
    if(this.deviceService.isDesktop()) {
      defs = [
        { breakpoint: Breakpoints.XSmall, width: 1 },
        { breakpoint: Breakpoints.Small, width: 1 },
        { breakpoint: Breakpoints.Medium, width: 3 },
        { breakpoint: Breakpoints.Large, width: 4 },
        { breakpoint: Breakpoints.XLarge, width: 4 }
      ]
    }
    else {
      defs = [
        { breakpoint: Breakpoints.Handset, width: 1 },
        { breakpoint: Breakpoints.Tablet, width: 2 },
        { breakpoint: Breakpoints.Web, width: 3 }
      ];
    }
    let m_breakpoints = [];
    defs.forEach((def) => m_breakpoints.push(def.breakpoint));
    this.breakpointObserver.observe(m_breakpoints).subscribe(result => {
      for(let breakpoint of Object.entries(result.breakpoints).filter((value) => value[1])) {
        for(let def of defs) {
          if(def.breakpoint.includes(breakpoint[0])) {
            this.nCardsWidth = def.width;
            //return;
          }
        }
      }
    });
  }

  ngOnInit() {
    super.ngOnInit();
    /*
    let dialogRef = this.dialog.open(ErrorPopupDialog, {
      minWidth: '30%',
      maxWidth: '60%',
      data: {
        error: {
          errors: [
            { field: "Field 1", defaultMessage: "Oh My Message 1" },
            { field: "Field 2", defaultMessage: "Oh My Message 2" },
            { field: "Field 3", defaultMessage: "Oh My Message 3" }
          ]
        },
        message: "MY ERROR"
      }
    });
    */
  }

  public onUserSearchInput($event): void {
    let value = $event.originalTarget.value;
    let m_this = this;
    delay(function() {
      /* console.log("Typed " + value); */
      m_this.setFilter(value);
    }, 500, "MetaListComponent:onUserSearchInput");
  }

  public card_onClick(_id: string): void {
    this.card_onViewItem(_id);
  }

  public card_onViewItem(_id: string): void {
    this.router.navigateByUrl("/meta/view/" + _id);
  }

  public card_onEditItem(_id: string): void {
    this.router.navigateByUrl("/meta/edit/" + _id);
  }

  public card_onDeleteItem(_id: string): void {
    const dialogRef = this.dialog.open(MetaListComponentOnDeleteDialog, {
      minWidth: '30%',
      maxWidth: '60%',
      data: { _id: _id, name: 'Hello' }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.metaService.refresh();
    });
  }

  public addMeta_onClick(): void {
    this.router.navigateByUrl("/meta/add");
  }

  public setFilter(filter: string): void {
    try {
      this.appService.setLoaderStatus({ mode: "indeterminate", });
      this.metaService.setFilter(filter);
      this.searchError = null;
    } catch (ex) {
      if(ex instanceof SyntaxError) {
        this.searchError = ex.message;
      } else {
        throw ex;
      }
    }
  }

  public getException(): string {
    return this.searchError;
  }

}

export class SearchErrorStateMatcher implements ErrorStateMatcher {

  constructor(private metaListComponent: MetaListComponent) {}

  public isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return this.metaListComponent.getException() != null;
  }
}

export interface MetaListComponentOnDeleteDialogData {
  _id: string;
  name: string;
}

@Component({
  selector: 'on-delete.dialog',
  templateUrl: './dialog/on-delete.dialog.html',
})
export class MetaListComponentOnDeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<MetaListComponentOnDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: MetaListComponentOnDeleteDialogData,
    private appService: AppService,
    private metaService: MetaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) {}

  public onNoClick(): void {
    /* TODO */
    this.dialogRef.close();
  }

  public onOkClick(): void {
    /* TODO */
    this.metaService.deleteMetaDescriptor(this.data._id).subscribe(response => {
      if(response) {
        if(response.status == 204) {
          let snackBar = this.snackBar.open("Meta supprimée", "Compris", {
            duration: 3000
          });
        } else {
          let snackBar = this.snackBar.open("Une erreur est survenue, impossible de supprimer la meta", "Compris", {
            duration: 3000
          });
        }
      } else {
        let snackBar = this.snackBar.open("Une erreur est survenue, impossible de supprimer la meta", "Compris", {
          duration: 3000
        });
      }
      this.appService.setLoaderStatus({mode: "query"});
      this.metaService.refresh();
      this.dialogRef.close();
    }, error => {
      let dialogRef = this.dialog.open(ErrorPopupDialog, {
        minWidth: '30%',
        maxWidth: '60%',
        data: error
      });
    });
      
  }
}