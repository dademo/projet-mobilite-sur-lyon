import { Component, OnInit } from '@angular/core';
import { Time, DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from "@angular/router";
import { trigger } from '@angular/animations';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from 'src/app/services/app-service/app-service.service';
import { MetaService } from 'src/app/services/meta/meta.service';
import { BaseComponent } from 'src/app/core/base-component/base-component.component';
import { MetaDescriptorShort, MetaDescriptor, ServiceType, AllServiceTypes } from 'src/app/data/data';
import { delay } from 'src/app/tools/delay';
import { FormInput } from 'src/app/core/auto-form/auto-form.component';
import { fadeIn, fadeOut } from 'src/app/core/transitions';
import { HttpResponse } from '@angular/common/http';
import { ErrorPopupDialog } from 'src/app/core/error-popup/error-popup.dialog';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-meta-show',
  templateUrl: './meta-show.component.html',
  styleUrls: ['./meta-show.component.css'],
  animations: [
    trigger('onLoad', [
      fadeIn,
      fadeOut
    ])
  ]
})
export class MetaShowComponent extends BaseComponent implements OnInit {

  public metaDescriptor$: Observable<HttpResponse<MetaDescriptor | string | object>> = null;
  public metaDescriptor: MetaDescriptor = null;

  public formgroup: FormGroup = null;

  public AllServiceTypes: ServiceType[] = AllServiceTypes;
  

  constructor(
    protected appService: AppService,
    private metaService: MetaService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private datePipe: DatePipe) {
      super(appService);
      this.appService.setLoaderStatus({ mode: "query" });

      this.formgroup = new FormGroup({
        _id: new FormControl(null, [ Validators.required ]),
        name: new FormControl(null, [ Validators.required ]),
        description: new FormControl(null),
        fullname: new FormControl(null),
        service_type: new FormControl(null, [ Validators.required ]),
        url: new FormControl(null, [ Validators.required ]),
        crs: new FormControl(null),
        collect_interval: new FormControl(null, [ Validators.required, Validators.min(1) ]),
        module: new FormControl(null, [ Validators.required ]),
        data_source: new FormControl(null, [ Validators.required ]),
        data_source_ref: new FormControl(null, [ Validators.required ]),
        privacy: new FormControl(null, [ Validators.required ]),
        license: new FormControl(null, [ Validators.required ]),
        schema: new FormControl(null, [ Validators.required ])
      });

      this.metaDescriptor$ = metaService.getMetaDescriptor(this.route.snapshot.params.id);
      if(this.metaDescriptor$) {
        this.metaDescriptor$.subscribe(response => {
          if(response)
          {
            if(response.status == 200) {
              this.metaDescriptor = response.body as MetaDescriptor;
              this.appService.setAppTitle("[Visualisation] " + this.metaDescriptor.name);
              this.appService.setLoaderStatus(null);
  
              /* Setting values */
              Object.keys(this.metaDescriptor).forEach(objKey => {
                let input = this.formgroup.get(objKey);
                if(input) {
                  /* https://angular.io/api/common/Time */
                  if(this.metaDescriptor[objKey].hours !== undefined && this.metaDescriptor[objKey].minutes !== undefined) {
                    let d = new Date();
                    d.setHours(this.metaDescriptor[objKey].hours);
                    d.setMinutes(this.metaDescriptor[objKey].minutes);
                    input.setValue(this.datePipe.transform(d, 'hh:mm'));
                  } else {
                    input.setValue(this.metaDescriptor[objKey] || '<vide>');
                  }
                }
              });
            }
          } else {
            /* Handling error */
            let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger la meta", "Compris", {
              duration: 3000
            });
            this.router.navigateByUrl('/meta');
          }
        }, error => {
          let dialogRef = this.dialog.open(ErrorPopupDialog, {
            minWidth: '30%',
            maxWidth: '60%',
            data: error
          });
        });
      } else {
        /* Handling error */
        let snackBar = this.snackBar.open("Une erreur est survenue, impossible de charger la meta", "Compris", {
          duration: 2000
        });
        this.router.navigateByUrl('/meta');
        /*
        snackBar.onAction().subscribe(() => {
          console.log("snackbar -> onAction");
        });
        */
      }
    }

  ngOnInit() {
    super.ngOnInit();
    //this.appService.setAppTitle("Meta <strong>" + this.metaDescriptor.getId() + "</strong>");
    this.appService.setBackLocation("/meta");

    //this.router.navigateByUrl("");
  }

  public onSubmit(formValue: any): void {
    /* Nothing to do */
  }

  public onEditClick(): void {
    this.router.navigateByUrl("/meta/edit/" + this.route.snapshot.params.id);
  }

}
