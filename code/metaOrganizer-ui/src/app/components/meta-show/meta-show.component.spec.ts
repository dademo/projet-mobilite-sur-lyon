import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaShowComponent } from './meta-show.component';

describe('MetaShowComponent', () => {
  let component: MetaShowComponent;
  let fixture: ComponentFixture<MetaShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetaShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetaShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
