import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MetaDescriptor, ServiceType, AllServiceTypes } from 'src/app/data/data';
import { MetaService } from 'src/app/services/meta/meta.service';
import { AppService } from 'src/app/services/app-service/app-service.service';
import { BaseComponent } from 'src/app/core/base-component/base-component.component';
import { ErrorPopupDialog } from 'src/app/core/error-popup/error-popup.dialog';

@Component({
  selector: 'app-meta-add',
  templateUrl: './meta-add.component.html',
  styleUrls: ['./meta-add.component.css']
})
export class MetaAddComponent extends BaseComponent implements OnInit {

  public formgroup: FormGroup = null;

  public AllServiceTypes: ServiceType[] = AllServiceTypes;


  constructor(
    protected appService: AppService,
    private metaService: MetaService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) {

      super(appService);
      //this.appService.setLoaderStatus({ mode: "query" });


      this.formgroup = new FormGroup({
        name: new FormControl(null, [ Validators.required ]),
        description: new FormControl(null),
        fullname: new FormControl(null),
        service_type: new FormControl(null, [ Validators.required ]),
        url: new FormControl(null, [ Validators.required ]),
        crs: new FormControl(null),
        collect_interval: new FormControl(null, [ Validators.required ]),
        module: new FormControl(null, [ Validators.required ]),
        data_source: new FormControl(null, [ Validators.required ]),
        data_source_ref: new FormControl(null, [ Validators.required ]),
        privacy: new FormControl(null, [ Validators.required ]),
        license: new FormControl(null, [ Validators.required ]),
        schema: new FormControl(null, [ Validators.required, (control: AbstractControl): ValidationErrors => {
          try {
            JSON.parse(control.value);
            return null;
          } catch (exception) { /* SyntaxError */
            return {'formatError': {value: control.value}}
          }
        } ])
      });
    }

  ngOnInit() {
    super.ngOnInit();
    this.appService.setBackLocation("/meta");
    this.appService.setAppTitle("Création d'une meta");
  }

  public onSubmit(formValue: any): void {

    if(!(this.formgroup.invalid || this.formgroup.pending)) {
      try {
        /* Update JSON schema */
        let schema = JSON.parse(formValue.schema);
        formValue.schema = schema;

        /* Update collect_interval */
        let collect_interval = new RegExp('([0-9]{1,2}):([0-9]{1,2})').exec(formValue.collect_interval);
        if(collect_interval) {
          formValue.collect_interval = {
            hours: parseInt(collect_interval[1]),
            minutes:parseInt(collect_interval[2])
          }
        } else {
          return;
        }

        let updater = this.metaService.addMetaDescriptor(formValue);
        if(updater) {
          updater.subscribe(response => {
            if(response) {
              if(response.status == 200) {
                this.snackBar.open("La meta a bien été créée", "Compris", {
                  duration: 3000
                });
                this.router.navigateByUrl('/meta');
              } else {
                /* Handling error */
                this.snackBar.open("Une erreur est survenue, impossible de créer la meta", "Compris", {
                  duration: 3000
                });
              }
            } else {
              /* Handling error */
              this.snackBar.open("Une erreur est survenue, impossible de créer la meta", "Compris", {
                duration: 3000
              });
            }
          }, error => {
            let dialogRef = this.dialog.open(ErrorPopupDialog, {
              minWidth: '30%',
              maxWidth: '60%',
              data: error
            });
          });
        } else {
          this.snackBar.open("Une erreur inconnue est survenue, impossible de créer la meta", "Compris", {
            duration: 3000
          });
        }

      } catch (exception) { /* SyntaxError */
        /* Nothing */
      }
    }
  }

}
