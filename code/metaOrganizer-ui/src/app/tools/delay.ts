import { v4 as uuid } from 'uuid';

type delayValue = {
    uuid: string,
    callback: (...args: any[]) => any
};

var delayList = {};

export function delay(callback: (...args: any[]) => void, ms: number, key: string | null = null, ...args: any[]): void {
    if(!key) {
        setTimeout(callback, ms);
    } else {
        let m_uuid = uuid();
        delayList[key] = { uuid: m_uuid, callback: callback };
        setTimeout(function() {
            if(key in delayList && delayList[key].uuid === m_uuid) {
                delayList[key].callback(args);
            }
        }, ms);
    }
}