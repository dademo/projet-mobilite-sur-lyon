package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers;

import org.springframework.beans.factory.annotation.Autowired;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.JobRepository;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MetaRepository;

/**
 * 
 * Tools to query meta on the HDFS
 * 
 * @author dademo
 *
 */
public final class MetaTools {

	@Autowired
	private MetaRepository metaRepository;
	
	@Autowired
	private JobRepository jobRepository;
		
}
