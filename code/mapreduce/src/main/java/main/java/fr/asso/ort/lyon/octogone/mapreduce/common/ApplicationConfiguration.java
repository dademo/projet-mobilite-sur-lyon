package main.java.fr.asso.ort.lyon.octogone.mapreduce.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;

@Configuration
@PropertySource("classpath:application.properties")
@Getter
public class ApplicationConfiguration {

	@Value("${hdfs.name-node}")
	private String namenodeUrl;
	
	@Value("${hdfs.auth.username}")
	private String hdfsUsername;
	
	@Value("${hdfs.auth.password}")
	private String hdfsUserPassword;
	
	@Value("${hdfs.collectors.path}")
	private String hdfsCollectorsPath;
}
