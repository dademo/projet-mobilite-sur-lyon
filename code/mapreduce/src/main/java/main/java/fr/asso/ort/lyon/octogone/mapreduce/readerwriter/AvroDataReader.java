package main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter;

import java.io.IOException;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.FileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericData.Record;
import org.apache.hadoop.fs.AvroFSInput;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import lombok.AccessLevel;
import lombok.Setter;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsDataFinder;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsDataFinder.DataCaster;

public class AvroDataReader extends RecordReader<Long, Object> {
	
	/** STATIC **/
	@Setter(value = AccessLevel.PUBLIC)
	private static FileSystem hdfs;
	
	@Setter(value = AccessLevel.PUBLIC)
	private static Class<? extends Object> castTo = null;
	
	@Setter(value = AccessLevel.PUBLIC)
	private static DataCaster caster = null;
	
	/** INSTANCE **/

	private Path avroFilePath = null;
	private FSDataInputStream inputStream = null;
	private FileReader<Record> fileReader = null;
	
	private long iterator = 0;
	private Object currentValue = null;
	private long fileLen = -1;
	
	public AvroDataReader(Path avroFilePath) {
		this.avroFilePath = avroFilePath;
	}
	

	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		// Getting file length
		FileStatus filestatus = AvroDataReader.hdfs.getFileStatus(avroFilePath);
		this.fileLen = filestatus.getLen();
		// Opening stream
		this.inputStream = AvroDataReader.hdfs.open(avroFilePath);
		// Making an avro input
		AvroFSInput input = new AvroFSInput(this.inputStream, this.fileLen);
		// Opening an avro reader
		this.fileReader = DataFileReader.openReader(input, new GenericDatumReader<>());
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if(this.fileReader.hasNext()) {
			if(this.castTo != null && this.caster != null) {
				this.currentValue = HdfsDataFinder.readCast(this.fileReader.next(), this.castTo, this.caster);
			} else {
				this.currentValue = this.fileReader.next();
			}
			this.iterator += 1;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Long getCurrentKey() throws IOException, InterruptedException {
		return this.iterator;
	}

	@Override
	public Object getCurrentValue() throws IOException, InterruptedException {
		return this.currentValue;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return ((float)this.fileLen) / this.fileReader.tell();
	}

	@Override
	public void close() throws IOException {
		this.fileReader.close();
		this.inputStream.close();
	}

}
