package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.avro.generic.GenericData;
import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.protobuf.UnknownFieldSet.Field;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public abstract class MapperHelper {

	private static Logger logger = LoggerFactory.getLogger(MapperHelper.class);
	
	public static void mergeIntoDBObject(DBObject finalObj, Object obj, String fieldName) {
		BasicDBObject inDBObject = new BasicDBObject();
		Gson gson = JobHelper.getGson();
		
		Arrays.stream(obj.getClass().getMethods())
		.filter(func -> func.getName().startsWith("get") && !"getClass".equals(func.getName()))
		.forEach(func -> {
			char[] valueName = func.getName().substring(3).toCharArray();
			valueName[0] = Character.toLowerCase(valueName[0]);
			try {
				Object val = func.invoke(obj);
				if(val instanceof GenericData.Record) {
					recordToDBObject((GenericData.Record) val, inDBObject);
				} else if (val instanceof Geometry) {
					inDBObject.put(
						new String(valueName),
						gson.fromJson(GeoJsonTools.GeoSerialize((Geometry) val), BasicDBObject.class)
					);
				} else {
					inDBObject.put(new String(valueName), val);
				}
			} catch (Exception ex) {
				// Nothing
				logger.info("Unable to put key [%s]", ex);
			}
		});
		finalObj.put(fieldName, inDBObject);
	}

	public static void mergeToHashMap(HashMap<String, Object> finalObj, Object obj, String prefix) {
		Arrays.stream(obj.getClass().getMethods())
		.filter(func -> func.getName().startsWith("get") && !"getClass".equals(func.getName()))
		.forEach(func -> {
			char[] valueName = func.getName().substring(3).toCharArray();
			valueName[0] = Character.toLowerCase(valueName[0]);
			String finalValueName = String.format("%s_%s", prefix, new String(valueName));
			try {
				Object val = func.invoke(obj);
				if(val instanceof GenericData.Record) {
					recordToMap((GenericData.Record) val, finalObj);
				} else {
					finalObj.put(finalValueName, func.invoke(obj));
				}
			} catch (Exception ex) {
				// Nothing
				logger.info(String.format("Unable to put key [%s]", valueName), ex);
			}
		});
	}
	
	public static void recordToDBObject(GenericData.Record record, DBObject dbObj) {
		record.getSchema().getFields().forEach(
			field -> {
				String fieldName = field.name();
				Object value = record.get(fieldName);
				dbObj.put(fieldName, (value instanceof org.apache.avro.util.Utf8)? value.toString() : value);
			}
		);
	}
	
	public static void recordToMap(GenericData.Record record, HashMap<String, Object> map) {
		record.getSchema().getFields().forEach(
			field -> map.put(field.name(), record.get(field.name()))
		);
	}
}
