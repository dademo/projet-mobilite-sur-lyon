package main.java.fr.asso.ort.lyon.octogone.mapreduce.jobs.testJob;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import lombok.SneakyThrows;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.BaseJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.MapReduceJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Meta;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoDataSingle;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeature;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.GeoJsonTools;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.JobHelper;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.JobRepository;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MetaRepository;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.MongoDataWriter.DBObjectCaster;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsDataFinder;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsMetaDataFinder;

@MapReduceJob
public class TestJob implements BaseJob {

    private static Logger logger = LoggerFactory.getLogger(TestJob.class);
    
    private static final String SOURCE_META_1 = "adr_voie_lieu.adrecole";
    
    private static final String SOURCE_META_2 = "pvo_patrimoine_voirie.pvochantierperturbant";
    
    private Meta metaPvochantierperturbant;
    
    @Autowired
    private JobRepository jobRepository;
    
    @Autowired
    private MetaRepository metaRepository;
    
    @Autowired
    private FileSystem hdfs;
    
    @Autowired
    private HdfsDataFinder hdfsDataFinder;
    
    @Autowired
    private HdfsMetaDataFinder hdfsMetaDataFinder;
    
    @Autowired
    private JobHelper jobHelper;
    
    @Override
    public String getJobName() {
    	return "TestJob";
    }

	@Override
	@SuppressWarnings("rawtypes")
	public Class<? extends Mapper> getMapperClass() {
		return TestJobMapper.class;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Class<? extends Reducer> getReducerClass() {
		return TestJobReducer.class;
	}

	@Override
	public Class<? extends Object> getMongoOutputType() {
		return BasicDBObject.class;
	}

	@Override
	public DBObjectCaster getMongoCaster() {
		return new JobHelper.HashMapCaster();
	}

	@Override
	public List<Path> getAvroFilePath() {
		return Arrays.asList(hdfsMetaDataFinder.findLatestByMeta(this.metaPvochantierperturbant));
	}

	@Override
	public String getOutpuCollectionName() {
		return "testjob_output";
	}
    
    /**
     * Usage: File1, File2
     */
	@Override
	@SneakyThrows
	public void main(String[] args) {
		logger.info("Hello TestJob");
		if(args.length != 2) {
			throw new IllegalArgumentException(String.format("Usage: [job] File1 File2"));
		}

		String file1 = args[0];
		String file2 = args[1];
		
		Meta metaAdrEcole = metaRepository.findByName(SOURCE_META_1);
		this.metaPvochantierperturbant = metaRepository.findByName(SOURCE_META_2);
		if (metaAdrEcole != null) {
			List<Path> files = hdfsDataFinder.getDataSince(LocalDateTime.now().minusDays(10), metaAdrEcole);
			Path maxFile = hdfsDataFinder.getLatestFile(metaAdrEcole);
			
			GeoDataSingle[] adrEcole = files.stream()
				.flatMap(file -> Arrays.stream(hdfsDataFinder.readGeoJson(file)))
				//.peek(elem -> logger.info(elem.toString()))
				.map(elem -> GeoJsonTools.fromSingleRecord((GeoJSONFeature)elem))
				.toArray(GeoDataSingle[]::new);
			
			TestJobMapper.setAdrEcole(Arrays.<GeoDataSingle<Object>>asList(adrEcole));
			
			TestJobMapper.setMetas(Arrays.asList(metaAdrEcole, this.metaPvochantierperturbant));
			
			/*
			for(int i = 0; i < fileGeoData.length-2; i++) {
				logger.info(String.format("Distance: %f", fileGeoData[i].getGeometry().distance(fileGeoData[i+1].getGeometry())));
			}

			List<Path> files2 = hdfsMetaDataFinder.findByJobAndMetaSinceLastJob(this, sourceData1);
			Path maxFile2 = hdfsMetaDataFinder.findLatestByMeta(sourceData1);
			*/
			/*
			GeoDataSingle[] fileGeoData2 = files.stream()
					.flatMap(file -> Arrays.stream(hdfsDataFinder.readGeoJson(file)))
					//.peek(elem -> logger.info(elem.toString()))
					.map(elem -> GeoJsonTools.fromSingleRecord((GeoJSONFeature)elem))
					.toArray(GeoDataSingle[]::new);
			*/
			
			//throw new RuntimeException("Hello Error World !");
		} else {
			logger.error("No meta found");
			System.exit(1);
		}
		
		jobHelper.jobInitWithGeoJson(this).waitForCompletion(true);
	}
}
