package main.java.fr.asso.ort.lyon.octogone.mapreduce.services;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import lombok.SneakyThrows;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.ApplicationConfiguration;

@Component
public class HdfsConnection {

	@Autowired
	ApplicationConfiguration applicationConfiguration;
	
	private static Logger logger = LoggerFactory.getLogger(HdfsConnection.class);
	
	private Configuration configuration;
	
	private FileSystem filesystem;
	
	@Bean
	public FileSystem getHdfsConnection() {
		// https://creativedata.atlassian.net/wiki/spaces/SAP/pages/52199514/Java+-+Read+Write+files+with+HDFS
		return this.getFileSystem();
	}
	
	@Bean
	public Configuration getHdfsConfiguration() {
		return this.getConfiguration();
	}
	
	@SneakyThrows
	private FileSystem getFileSystem() {
		logger.info(String.format("Connecting to HDFS with uri [%s]", applicationConfiguration.getNamenodeUrl()));
		if(this.filesystem == null) {
			Configuration config = this.getConfiguration();
			this.filesystem = FileSystem.get(URI.create(applicationConfiguration.getNamenodeUrl()), config);
			logger.info(String.format("Connected to HDFS with [%s] filesystem", this.filesystem.getClass().getCanonicalName()));
		}
		return this.filesystem;
	}
	
	private Configuration getConfiguration() {
		if (this.configuration == null) {
			this.configuration = new Configuration();
			// Set FileSystem URI
			//configuration.set("fs.defaultFS", applicationConfiguration.getNamenodeUrl());
			// Because of Maven
			//configuration.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
			//configuration.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
			// Set HADOOP user
			System.setProperty("HADOOP_USER_NAME", applicationConfiguration.getHdfsUsername());
			System.setProperty("hadoop.home.dir", "/");
		}
		return this.configuration;
	}
	
}
