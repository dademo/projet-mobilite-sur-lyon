package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;

import org.apache.avro.generic.GenericData;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsDataFinder;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.services.HdfsDataFinder.DataCaster;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GeoJSONFeature<T> {

	private String type;
	private GeoJSONFeatureGeometry geometry;
	private T properties;
	
	public static Object geometryCaster(Object toCast, @NotNull String fieldName, @NotNull Object castedObject) {
		if("geometry".equals(fieldName)) {
			return HdfsDataFinder.readCast((GenericData.Record) toCast, GeoJSONFeatureGeometry.class, GeoJSONFeatureGeometry::geometryCaster);
		} else if (toCast instanceof org.apache.avro.util.Utf8) {
			return toCast.toString();
		} else {
			// Default
			return toCast;
		}
	}
}
