package main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter;

import java.io.IOException;

import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import lombok.SneakyThrows;

public class MongoOutputCommitter extends OutputCommitter {

	@Override
	public void setupJob(JobContext jobContext) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setupTask(TaskAttemptContext taskContext) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean needsTaskCommit(TaskAttemptContext taskContext) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@SneakyThrows
	public void commitTask(TaskAttemptContext taskContext) throws IOException {
		// TODO Auto-generated method stub
		MongoDataWriter.setDoCommit(true);
	}

	@Override
	public void abortTask(TaskAttemptContext taskContext) throws IOException {
		// TODO Auto-generated method stub
		MongoDataWriter.setDoCommit(false);
	}

}
