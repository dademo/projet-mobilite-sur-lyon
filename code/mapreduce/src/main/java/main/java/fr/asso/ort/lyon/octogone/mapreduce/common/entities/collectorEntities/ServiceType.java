package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceType {
	JSON("json"),
	GEOJSON("geojson");
	
	private String value;
}
