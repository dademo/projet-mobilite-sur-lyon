package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta_mapreduce_jobs")
public class MapReduceJob {

	private String _id;
	private String jobName;
	private LocalDateTime timestamp;
	private Long batchDurationSeconds;
	private String status;
	private String errorType;
	private String errorDescription;
}
