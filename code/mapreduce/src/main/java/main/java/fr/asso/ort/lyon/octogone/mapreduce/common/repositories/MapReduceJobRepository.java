package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.MapReduceJob;

import java.time.LocalDateTime;
import java.util.List;


public interface MapReduceJobRepository extends MongoRepository<MapReduceJob, String> {
	public MapReduceJob findBy_id(String id);
	public List<MapReduceJob> findByJobName(String name);
	@Aggregation({ "{ $match: { 'jobName': ?0 } }", "{ $sort: { timestamp: -1 } }", "{ $limit: 1 }" })
	public MapReduceJob findFirstByJobNameByOrderByTimestampDesc(String name);
	public List<MapReduceJob> findByJobNameAndTimestampAfter(String name, LocalDateTime date);
}
