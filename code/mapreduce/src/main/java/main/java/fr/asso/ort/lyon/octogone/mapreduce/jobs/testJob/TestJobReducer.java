package main.java.fr.asso.ort.lyon.octogone.mapreduce.jobs.testJob;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.JobHelper;


public class TestJobReducer extends Reducer<Text, Text, Text, Text> {

	private static Logger logger = LoggerFactory.getLogger(TestJobReducer.class);
	
	private Gson gson = JobHelper.getGson();
	
	@Override
	protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {
		Iterator<Text> it = values.iterator();
		if(it.hasNext()) {
			context.write(key, it.next());
		}
	}
}
