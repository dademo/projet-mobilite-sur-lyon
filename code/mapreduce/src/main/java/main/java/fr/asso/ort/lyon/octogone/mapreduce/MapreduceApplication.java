package main.java.fr.asso.ort.lyon.octogone.mapreduce;

import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import lombok.Value;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.BaseJob;

@SpringBootApplication
/*https://stackoverflow.com/questions/45006266/could-not-found-bean-for-mongorepository-spring-boot/45189670 */
@EnableMongoRepositories(basePackages = "main.java.fr.asso.ort.lyon.octogone")
public class MapreduceApplication {
	
	@Value
	private static class CommandLineOptions {
		private String jobName;
	}
	
	private static class BeanNotFoundException extends RuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public BeanNotFoundException() {
			super();
		}
		public BeanNotFoundException(String message) {
			super(message);
		}
		public BeanNotFoundException(String message, Throwable cause) {
			super(message, cause);
		}
		public BeanNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}
		public BeanNotFoundException(Throwable cause) {
			super(cause);
		}
	}

    private static Logger logger = LoggerFactory.getLogger(MapreduceApplication.class);
    
    /* https://thierry-leriche-dessirier.developpez.com/tutoriels/java/common-cli-5-minutes */
    private static Options getJobOptions() {
    	Options jobOpts = new Options();
		jobOpts.addOption(Option
				.builder("j")
				.longOpt("job")
	            .argName("job") 
				.desc("Job à exécuter")
				.hasArg(true)
				.required(true)
				.build()
				);
		jobOpts.addOption(Option
				.builder("h")
				.longOpt("help")
	            .argName("help") 
				.desc("Affiche cette aide")
				.hasArg(false)
				.required(false)
				.build()
				);
		return jobOpts;
    }
    
    private static Options getHelpOption() {
    	Options helpOpt = new Options();
    	helpOpt.addOption(Option
				.builder("h")
				.longOpt("help")
	            .argName("help") 
				.desc("Affiche cette aide")
				.hasArg(false)
				.required(false)
				.build()
				);
    	return helpOpt;
    }
    
    private static CommandLineOptions getCommandlineOptions(String[] args) {
    	String jobName = "";
		try {
			CommandLine command = new DefaultParser().parse(getHelpOption(), args, true);
			if(command.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
			    formatter.printHelp("MapReduce runner", MapreduceApplication.getJobOptions(), true);
			    System.exit(0);
			}
			command = new DefaultParser().parse(MapreduceApplication.getJobOptions(), args);
			jobName = command.getOptionValue("job");
			
			return new CommandLineOptions(jobName);
		} catch (ParseException e) {
			System.err.println(String.format("Unable to parse options (%s)", e.getMessage()));
			HelpFormatter formatter = new HelpFormatter();
		    formatter.printHelp("MapReduce runner", MapreduceApplication.getJobOptions(), true);
		    System.exit(1);
		    return null;
		}
    }
    
    private static BaseJob getJob(String jobName) {
    	BaseJob toReturn = null;
    	
    	logger.info("Recherche du job MapReduce");
		Reflections reflections = new Reflections(MapreduceApplication.class);
		Set<Class<? extends BaseJob>> jobs = reflections.getSubTypesOf(BaseJob.class);

		for(Class<? extends BaseJob> job : jobs) {
			if(job.getSimpleName().equals(jobName)) {
				try {
					toReturn = job.newInstance();
					break;
				} catch (InstantiationException | IllegalAccessException e) {
					logger.error(String.format("Unable to instanciate [%s]. You must provide a public default constructor", job.getName()));
				}
				System.exit(0);
			}
		}
		
		return toReturn;
    }
    
    private static String getRealBeanName(ConfigurableApplicationContext context, String wantedName) {
		for(String beanName : context.getBeanDefinitionNames()) {
			if(beanName.equalsIgnoreCase(wantedName)) {
				return beanName;
			}
		}
		throw new BeanNotFoundException(String.format("Can't find bean [%s]", wantedName));
    }

	public static void main(String[] args) {
		
		CommandLineOptions options = getCommandlineOptions(args);
		
		ConfigurableApplicationContext context = SpringApplication.run(MapreduceApplication.class, args);
		/*
		for(String name : context.getBeanDefinitionNames()) {
			logger.info(String.format("Bean [%s] of class [%s]", name, context.getBean(name)));
		}
		*/
		
		BaseJob job = null;
		MapReduceRunner runner = null;
		try {
			logger.info("Recherche du job MapReduce");
			//BaseJob job = getJob(options.getJobName());
			job = (BaseJob) context.getBean(getRealBeanName(context, options.getJobName()));
			runner = (MapReduceRunner) context.getBean(getRealBeanName(context, "MapReduceRunner"));
		} catch (org.springframework.beans.factory.NoSuchBeanDefinitionException | BeanNotFoundException ex) {
			logger.error(String.format("Can't load bean [%s], the MapReduce can't run", options.getJobName()));
		}
		
		if(job != null ) {
			logger.info(String.format("Running job [%s]", options.getJobName()));
			// Making args //
			ArrayList<String> m_args = new ArrayList<String>();
			for(int i = 0; i < args.length; i++) {
				if("-j".equals(args[i])) {
					i += 1;
				} else {
					m_args.add(args[i]);
				}
			}
			
			System.exit(
					runner.main(
							job,
							m_args.toArray(new String[m_args.size()])
			));
		} else {
			logger.error("No job loaded, aborting");
		}
		
		/*
		Reflections reflections = new Reflections(MapreduceApplication.class);
		Set<Class<? extends BaseJob>> jobs = reflections.getSubTypesOf(BaseJob.class);
		logger.info(jobs.toString());
		for(Class<? extends BaseJob> job : jobs) {
			if(job.getSimpleName().equals(jobName)) {
				logger.info(String.format("Running job [%s]", job.getName()));
				System.exit(0);
			}
		}
		*/
		System.exit(1);
	}

}
