package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers;

import java.util.Arrays;
import java.util.Collection;

import javax.management.RuntimeErrorException;

import org.apache.avro.generic.GenericData;
import org.apache.commons.lang3.NotImplementedException;
import org.geotools.geojson.geom.GeometryJSON;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.istack.NotNull;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoData;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoDataSingle;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeature;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeatureGeometry;

public abstract class GeoJsonTools {
	
	private static Logger logger = LoggerFactory.getLogger(GeoJsonTools.class);
	
	@FunctionalInterface
	public interface ArrayCaster {
		public Object cast(Object obj);
	}
	
	private static class ArrayCasterImpl implements ArrayCaster {

		@Override
		public Object cast(Object obj) {
			if (obj instanceof GenericData.Array) {
				return fromGenericArray((GenericData.Array) obj, this);
			} else {
				try {
					return obj;
				} catch (ClassCastException ex) {
					logger.error("Unable to cast", ex);
					return null;
				}
			}
		}
	}
	

	public static <T> GeoDataSingle<T> fromSingleRecord(GeoJSONFeature<T> value) {
		
		GeometryFactory geometryFactory = new GeometryFactory();
		Geometry geom = null;
		ArrayCaster arrayCaster = new ArrayCasterImpl();
		Object coordinates;
		
		if (value.getGeometry().getCoordinates() instanceof GenericData.Array) {
			coordinates = fromGenericArray((GenericData.Array) value.getGeometry().getCoordinates(), arrayCaster);
		} else {
			coordinates = value.getGeometry().getCoordinates();
		}
		
		switch(value.getGeometry().getType()) {
			case Point:
				geom = geometryFactory.createPoint(
					new Coordinate(
						((float[]) coordinates)[0],
						((float[]) coordinates)[1]
				));
				break;
			case LineString:
				// org.apache.avro.generic.GenericData.Array
				// GenericData.Array
				geom = geometryFactory.createLineString(
					Arrays.stream((Object[]) coordinates)
					.map(element -> 
						new Coordinate(
							(Float)((Object[])element)[0],
							(Float)((Object[])element)[1]
					))
					.toArray(Coordinate[]::new)
				);
				break;
			case Polygon:
				Polygon[] c = Arrays.stream((Object[]) coordinates)
					.map(element1 -> 
						Arrays.stream((Object[])element1)
						.map(element2 -> new Coordinate(
								(Float)((Object[])element2)[0],
								(Float)((Object[])element2)[1]
						))
						.toArray(Coordinate[]::new)
					)
					.map(e -> geometryFactory.createPolygon(e))
					.toArray(Polygon[]::new);
				if(c.length > 1) {
					// Multi
					geom = geometryFactory.createMultiPolygon(c);
				} else {
					geom = c[0];
				}
				break;
			case MultiPoint:
				geom = geometryFactory.createMultiPointFromCoords(
						Arrays.stream((Object[]) coordinates)
						.map(element -> 
							new Coordinate(
								(Float)((Object[])element)[0],
								(Float)((Object[])element)[1]
						))
						.toArray(Coordinate[]::new)
					);
				break;
			case MultiLineString:
				geom = geometryFactory.createMultiLineString(
						Arrays.stream((Object[]) coordinates)
					.map(element1 -> 
						Arrays.stream((Object[])element1)
						.map(element2 -> new Coordinate(
								(Float)((Object[])element2)[0],
								(Float)((Object[])element2)[1]
						))
						.toArray(Coordinate[]::new)
					)
					.map(e -> geometryFactory.createLineString(e))
					.toArray(LineString[]::new)
				);
				break;
			case MultiPolygon:
				geometryFactory.createMultiPolygon(
					Arrays.stream((Object[]) coordinates)
					.map(element1 -> 
						Arrays.stream((Object[])element1)
						.map(element2 -> new Coordinate(
								(Float)((Object[])element2)[0],
								(Float)((Object[])element2)[1]
						))
						.toArray(Coordinate[]::new)
					)
					.map(e -> geometryFactory.createPolygon(e))
					.toArray(Polygon[]::new)
				);
			default:
				throw new NotImplementedException(String.format("Can't handle argument [%s]", value.getGeometry().getType()));
		}
		return new GeoDataSingle<T>(value.getProperties(), value.getGeometry().getType(), geom);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> GeoData<T>[] fromRecords(Collection<GeoJSONFeature<T>> values) {
		return values.stream()
			.map(value -> fromSingleRecord(value))
			.toArray(GeoData[]::new);
	}
	
	public static <T> GeoData<T>[] fromRecords(GeoJSONFeature<T>[] values) {
		return fromRecords(Arrays.asList(values));
	}
	
	private static Object[] fromGenericArray(@NotNull GenericData.Array<?> array, ArrayCaster caster) {
		return array
			.parallelStream()
			.map(element -> caster.cast(element))
			.toArray(Object[]::new);
	}
	
	public static String GeoSerialize(Geometry geometry) {
		return new GeometryJSON().toString(geometry);
	}
}
