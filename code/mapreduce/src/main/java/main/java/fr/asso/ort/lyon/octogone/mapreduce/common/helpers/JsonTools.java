package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * Tools to get values by path with JSON objects (rendered in Java with LinkedHashMap).
 * 
 * @author dademo
 *
 */
public abstract class JsonTools {

	public static Object getDictValueByPath(Map<String, Object> obj, String path) {
		Object current = obj;
		String currentPath = ".";
		
		for(String pathElement : path.split("\\.")) {
			if(!pathElement.isEmpty()) {
				currentPath += pathElement;
				if(current instanceof Map) {
					if(((Map)current).containsKey(pathElement)) {
						current = ((Map)current).get(pathElement);
					} else {
						throw new IllegalArgumentException(String.format("Value not found for path `%s`", currentPath));
					}
				} else {
					throw new IllegalArgumentException(String.format("Value not found for path `%s`", currentPath));
				}
			}
		};
		
		return current;
	}
	
	// org.apache.avro.generic.GenericData.Record
	// https://github.com/filosganga/geogson
	
	
}
