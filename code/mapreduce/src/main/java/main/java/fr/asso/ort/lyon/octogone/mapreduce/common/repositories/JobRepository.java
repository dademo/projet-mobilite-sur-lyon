package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Job;

import java.time.LocalDateTime;
import java.util.List;


public interface JobRepository extends MongoRepository<Job, String> {
	public Job findBy_id(String id);
	public List<Job> findByJobName(String name);
	public List<Job> findByMeta(String name);
	public List<Job> findByTimestampAfter(LocalDateTime date);
	public Job findFirstByOrderByTimestampDesc();
	@Aggregation({ "{ $match: { 'jobName': ?0 } }", "{ $sort: { timestamp: -1 } }", "{ $limit: 1 }" })
	public Job findFirstByJobNameAndByOrderByTimestampDesc(String name);
}
