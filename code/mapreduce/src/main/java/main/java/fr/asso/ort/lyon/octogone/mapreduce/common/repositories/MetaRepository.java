package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories;
import org.springframework.data.mongodb.repository.MongoRepository;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Meta;

public interface MetaRepository extends MongoRepository<Meta, String> {
	public Meta findBy_id(String id);
	public Meta findByName(String name);
}
