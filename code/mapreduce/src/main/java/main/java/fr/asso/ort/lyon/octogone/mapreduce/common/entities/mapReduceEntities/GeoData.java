package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class GeoData<T> {

	@NotNull
	protected T properties;
	
	@NotNull
	protected SourceGeometryType sourceGeometryType;
	
}
