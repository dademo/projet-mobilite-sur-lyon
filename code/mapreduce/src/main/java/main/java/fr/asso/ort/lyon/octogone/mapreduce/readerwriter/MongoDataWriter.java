package main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.eclipse.jetty.util.ajax.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.sun.istack.NotNull;
import com.sun.xml.txw2.Document;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoData;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.JobHelper;

public class MongoDataWriter extends RecordWriter<Text, Text> {
	
	@FunctionalInterface
	public static interface DBObjectCaster {
		public DBObject castTo(@NotNull Object o);
	}

	/** STATIC **/
	private static Logger logger = LoggerFactory.getLogger(MongoDataWriter.class);

	@Setter(value = AccessLevel.PUBLIC)
	private static MongoCollection<DBObject> outputCollection = null;
	
	@Getter(value = AccessLevel.PUBLIC)
	@Setter(value = AccessLevel.PUBLIC)
	private static boolean doCommit = true;
	
	@Getter(value = AccessLevel.PUBLIC)
	private static MongoDataWriter mongoDataWriter = new MongoDataWriter();
	
	@Setter(value = AccessLevel.PUBLIC)
	private static Class<? extends Object> castTo = null;
	
	@Setter(value = AccessLevel.PUBLIC)
	private static DBObjectCaster caster = null;
	
	

	/** INSTANCE **/
	private HashMap<String, MongoData<DBObject>> buffer = new HashMap<>();
	
	private void checkRequirements() {
		if(castTo == null) {
			throw new RuntimeException("No cast defined for mongoDataWriter");
		}
		if(caster == null) {
			throw new RuntimeException("No caster set for output datas");
		}
	}
	
	@Override
	public void write(Text key, Text value) throws IOException, InterruptedException {
		checkRequirements();
		if(!buffer.containsKey(key.toString())) {
			/*
			this.buffer.put(key.toString(), new MongoData<DBObject>(
					caster.castTo(JobHelper.getGson().fromJson(value.toString(), castTo))
			));*/
			this.buffer.put(key.toString(), new MongoData<DBObject>(
					JobHelper.getGson().fromJson(value.toString(), BasicDBObject.class)
			));
		}
	}

	@Override
	public void close(TaskAttemptContext context) throws IOException, InterruptedException {
		// Writing data
		if(MongoDataWriter.isDoCommit()) {
			logger.info("Writing values");
			buffer.values()
			.stream()
			.forEach(value -> MongoDataWriter.outputCollection.insertOne(value.toDBObject()));
			logger.info(String.format("%d written", buffer.size()));
		} else {
			logger.info("Commit is discarded, will not write any value");
		}
	}

}
