package main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class AvroInputFormat extends FileInputFormat<Long, Object> {

	@Override
	public RecordReader<Long, Object> createRecordReader(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		FileSplit fileSplit = (FileSplit)split;
		return new AvroDataReader(fileSplit.getPath());
	}
	
	@Override
	public boolean isSplitable(JobContext context, Path filename) {
		// Can't split avro file
		return false;
	}

}
