package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.sun.istack.NotNull;

import lombok.SneakyThrows;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.ApplicationConfiguration;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.BaseJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeature;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.AvroDataReader;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.AvroInputFormat;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.MongoData;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.MongoDataWriter;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.MongoOutputFormat;

@Component
public final class JobHelper {

    @Autowired
    private FileSystem hdfs;

    @Autowired
    private Configuration configuration;
    
    @Autowired
    private ApplicationConfiguration appConfig;
    
    @Autowired
    private MongoDbFactory mongoDbFactory;
    
	@SuppressWarnings("unchecked")
	@SneakyThrows
	public @NotNull <InputFormat> Job jobInit(@NotNull BaseJob mapReduceJob) {
		
		Job job = Job.getInstance(this.configuration, mapReduceJob.getJobName());
		
		job.setJarByClass(mapReduceJob.getClass());
		job.setMapperClass(mapReduceJob.getMapperClass());
		job.setReducerClass(mapReduceJob.getReducerClass());
		job.setInputFormatClass(AvroInputFormat.class);
		job.setOutputFormatClass(MongoOutputFormat.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		for(Path path : mapReduceJob.getAvroFilePath()) {
			FileInputFormat.addInputPath(job, path);
		}
		
		// Adding values to reader / writer
		AvroDataReader.setHdfs(this.hdfs);
		// AvroDataReader.setAvroFilePath(mapReduceJob.getAvroFilePath());
		MongoDataWriter.setCastTo(mapReduceJob.getMongoOutputType());
		MongoDataWriter.setCaster(mapReduceJob.getMongoCaster());
		
		MongoDataWriter.setOutputCollection(
			mongoDbFactory	
			.getDb()
			.getCollection(mapReduceJob.getOutpuCollectionName(), DBObject.class)
		);
		
		return job;
	}
    
	@SneakyThrows
	public @NotNull <InputFormat> Job jobInitWithGeoJson(@NotNull BaseJob mapReduceJob) {
		
		AvroDataReader.setCastTo(GeoJSONFeature.class);
		AvroDataReader.setCaster(GeoJSONFeature::geometryCaster);
		
		return jobInit(mapReduceJob);
	}
	
	public static @NotNull Gson getGson() {
		return new GsonBuilder().serializeSpecialFloatingPointValues().create();
	}
	
	public static class HashMapCaster implements MongoDataWriter.DBObjectCaster {

		@Override
		public DBObject castTo(Object o) {
			if(!(o instanceof HashMap)) {
				throw new RuntimeException(
					String.format(
						"Object is not an instance of hashmap (got %s)",
						o.getClass().getCanonicalName()
				));
			}
			DBObject toReturn = new BasicDBObject();
			((HashMap<String, Object>)o).keySet().forEach(
				key -> toReturn.put(
					key,
					m_cast(((HashMap<String, Object>)o).get(key))
			));
			return toReturn;
		}
		
		private Object m_cast(Object o) {
			if (o instanceof Integer) {
				return (int) o;
			} else if(o instanceof Double) {
				return (double) o;
			} else if (o instanceof Float) {
				return (float) o;
			} else if(o instanceof byte[] || o instanceof Byte[]) {
				return new String((byte[])o);
			} else if(o instanceof Iterable) {
				ArrayList<Object> toReturn = new ArrayList<>();
				Iterator it = ((Iterable)o).iterator();
				
				while(it.hasNext()) {
					toReturn.add(m_cast(it.next()));
				}
				return toReturn.toArray();
			} else {
				return o;
			}
		}
	}
	
}
