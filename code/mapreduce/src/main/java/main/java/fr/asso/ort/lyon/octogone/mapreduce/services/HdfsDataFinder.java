package main.java.fr.asso.ort.lyon.octogone.mapreduce.services;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.FileReader;
import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.AvroFSInput;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.ApplicationConfiguration;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Meta;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeature;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.JobRepository;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MetaRepository;

@Component
public class HdfsDataFinder {

	@Autowired
	private ApplicationConfiguration applicationConfiguration;

	@Autowired
    private JobRepository jobRepository;
    
    @Autowired
    private MetaRepository metaRepository;
    
    @Autowired
    private FileSystem hdfs;
    
    @Autowired
    private Configuration hdfsConfiguration;
	
	private static Logger logger = LoggerFactory.getLogger(HdfsDataFinder.class);
	
	@Getter
	private final class PathInfos {
		private final int year;
		private final int month;
		private final int day;
		private final String patternFileName;
		private final LocalDateTime dirDate;
		
		public PathInfos(Matcher pathMatcher) {
			this.year  = Integer.parseInt(pathMatcher.group(1));
			this.month = Integer.parseInt(pathMatcher.group(2));
			this.day   = Integer.parseInt(pathMatcher.group(3));
			this.patternFileName = pathMatcher.group(4);
			this.dirDate = LocalDateTime.of(year, month, day, 0, 0, 0);
		}
	}

	@Getter
	private final class FileInfos {
		private final DateTimeFormatter filenameDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
		private final String fileMetaStr;
		private final String fileNameDateStr;
		private final LocalDateTime fileNameDate;
		
		public FileInfos(Matcher fileNameMatcher) {
			this.fileMetaStr = 
			this.fileNameDateStr = fileNameMatcher.group(2);
			this.fileNameDate = this.filenameDateFormatter.parse(fileNameDateStr, LocalDateTime::from);	
		}
	}
	
	private interface PathComparator {
		public @NotNull Path apply(@NotNull Path path1, @NotNull Path path2);
	}
	
	@FunctionalInterface
	private interface LocalDateTimeFieldGetter {
		int getField();
	}
	
	@FunctionalInterface
	private interface LocalDateTimeSetter {
		@NotNull LocalDateTime apply(@NotNull LocalDateTime baseTime, @NotNull int value);
	}
	
	@FunctionalInterface
	public interface DataCaster {
		@NotNull Object apply(Object toCast, @NotNull String fieldName, @NotNull Object castedObject);
	}
	
	@Data
	@AllArgsConstructor
	private class CompareDirsAnswer {
		LocalDateTime currentDateTime;
		Path path;
	};
	
	public List<Path> getDataSince(@NotNull LocalDateTime date, @NotNull Meta meta) {
		// ex: /data/collectors/adr_voie_lieu.adrecole/2020/01/26/adr_voie_lieu.adrecole-20200126_191310.avro
		
		Pattern fileNamePattern = Pattern.compile("^([a-zA-Z0-9_.]+)-([0-9_]+)\\.avro$");
		DateTimeFormatter filenameDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
		
		LocalDateTimeSetter yearSetter = (baseDate, year) -> LocalDateTime.of(year, 1, 1, 0, 0, 0);
		LocalDateTimeSetter monthSetter = (baseDate, month) -> LocalDateTime.of(baseDate.getYear(), month, 1, 0, 0, 0);
		LocalDateTimeSetter daySetter = (baseDate, day) -> LocalDateTime.of(baseDate.getYear(), baseDate.getMonthValue(), day, 0, 0, 0);
		
		try {
			CompareDirsAnswer initialDirAnswer = new CompareDirsAnswer(null, pathForMeta(meta.getName()));
			Path[] filePaths = Arrays.stream(getCompareDirsIn(initialDirAnswer, yearSetter, date::getYear, date))
				.parallel()
				.flatMap(yearDirPath -> 
					Arrays.stream(getCompareDirsIn(yearDirPath, monthSetter, date::getMonthValue, date))
						.parallel()
						.flatMap(monthDirPath -> 
							Arrays.stream(getCompareDirsIn(monthDirPath, daySetter, date::getDayOfMonth, date))
								.parallel()
								.flatMap(dayDirPath -> 
									Arrays.stream(getCompareFilesIn(dayDirPath, date))
								)
						)
				)
				.map(answer -> answer.getPath())
				.toArray(Path[]::new);
			return Arrays.asList(filePaths);
		} catch (Exception ex) {
			logger.error("An error occured while looking for data", ex);
		}
		
		return new ArrayList<Path>();
	}
	
	@SneakyThrows
	private CompareDirsAnswer[] getCompareDirsIn(CompareDirsAnswer answer, LocalDateTimeSetter setter, LocalDateTimeFieldGetter getter, LocalDateTime dateToCompare) {
		return Arrays.stream(hdfs.listStatus(answer.getPath()))
			.filter(fileStatus -> fileStatus.isDirectory())
	 		.map(fileStatus -> new CompareDirsAnswer(
 					setter.apply(answer.getCurrentDateTime(), Integer.parseInt(fileStatus.getPath().getName())),
 					fileStatus.getPath()
 			))
	 		.filter(fileStatus ->
	 			getter.getField() == Integer.parseInt(fileStatus.getPath().getName())
	 			|| fileStatus.getCurrentDateTime().isAfter(dateToCompare)
	 		)
	 		.toArray(CompareDirsAnswer[]::new);
	}
	
	@SneakyThrows
	private CompareDirsAnswer[] getCompareFilesIn(CompareDirsAnswer answer, LocalDateTime dateToCompare) {
		return Arrays.stream(hdfs.listStatus(answer.getPath()))
	 		.filter(fileStatus -> fileStatus.isFile())
	 		.map(fileStatus -> {
	 			FileInfos fileInfos = getFileInfos(fileStatus.getPath());
	 			return new CompareDirsAnswer(
	 				LocalDateTime.of(
	 						fileInfos.getFileNameDate().getYear(),
	 						fileInfos.getFileNameDate().getMonth(),
	 						fileInfos.getFileNameDate().getDayOfMonth(),
	 						fileInfos.getFileNameDate().getHour(),
	 						fileInfos.getFileNameDate().getMinute(),
	 						fileInfos.getFileNameDate().getSecond()
 					),
	 				fileStatus.getPath()
	 			);
 			})
	 		.filter(fileStatus -> fileStatus.getCurrentDateTime().isAfter(dateToCompare))
	 		.toArray(CompareDirsAnswer[]::new);
	}
	
	public Path getLatestFile(@NotNull Meta meta) {
		
		PathComparator maxComparator = (path1, path2) -> {
			try {
				return (Integer.parseInt(path1.getName()) > Integer.parseInt(path2.getName()))? path1 : path2;
			} catch (NumberFormatException ex) {
				logger.error("An error occured while comparing paths", ex);
				return path1;
			}
		};
		PathComparator fileNameComparator = (path1, path2) -> {
			try {
				FileInfos fileInfos1 = getFileInfos(path1);
				FileInfos fileInfos2 = getFileInfos(path2);
				return fileInfos1.getFileNameDate().isAfter(fileInfos2.getFileNameDate())? path1 : path2;
			} catch (NumberFormatException ex) {
				logger.error("An error occured while comparing paths", ex);
				return path1;
			}
		};
		Predicate<? super FileStatus> directoryChecker = (fileStatus) -> fileStatus.isDirectory();
		Predicate<? super FileStatus> fileChecker = (fileStatus) -> fileStatus.isFile();

		Comparator<? super Path> comparator = (path1, path2) -> 
			getFileInfos(path1)
				.getFileNameDate()
				.isAfter(getFileInfos(path2).getFileNameDate())? 1 : -1;
		
		try {
			Path maxYear = ListComparePath(pathForMeta(meta.getName()), directoryChecker, comparator);
			if(maxYear != null) {
				Path maxMonth = ListComparePath(maxYear, directoryChecker, comparator);
				if(maxMonth != null) {
					Path maxDay = ListComparePath(maxMonth, directoryChecker, comparator);
					if(maxDay != null) {
						return ListComparePath(maxDay, fileChecker, comparator);
					}
				}
			}
			return null;
		} catch (IOException ex) {
			logger.error("An error occured while looking for data", ex);
			return null;
		}
	}
	
	private Path pathForMeta(@NotNull String metaName) {
		return new Path(String.format("%s/%s", applicationConfiguration.getHdfsCollectorsPath(), metaName));
	}
	
	private String sanitize(@NotNull String str) {
		return str.replace("//", "/");
	}
	
	private FileInfos getFileInfos(@NotNull Path path) {
		String pathInfos = path.toUri().getPath();
		return getFileInfos(pathInfos.substring(pathInfos.lastIndexOf('/')+1));
	}
	
	private FileInfos getFileInfos(@NotNull PathInfos path) {
		return getFileInfos(path.getPatternFileName());
	}
	
	private FileInfos getFileInfos(@NotNull String path) {
		Pattern fileNamePattern = Pattern.compile("^([a-zA-Z0-9_.]+)-([0-9_]+)\\.avro$");
		// More precise date
		Matcher fileNameMatcher = fileNamePattern.matcher(path);
		if (fileNameMatcher.matches()) {
			return new FileInfos(fileNameMatcher);
		} else {
			logger.error("getFileInfos: Pattern does not match");
			return null;
		}
	}
	
	private Path ListComparePath(
			@NotNull Path path,
			@NotNull Predicate<? super FileStatus> fileChecker,
			@NotNull Comparator<? super Path> comparator) throws IOException {
		Path toReturn = null;
		
		return Arrays.stream(hdfs.listStatus(path))
				.filter(fileChecker)
				.map(element -> element.getPath())
				.max(comparator)
				.orElse(null);
	}
	
	public Object[] readGeoJson(Path path) {
		return this.readFile(path, GeoJSONFeature.class, GeoJSONFeature::geometryCaster);
	}
	
	@SuppressWarnings("unchecked")
	public Object[] readFile(Path path, Class castTo, DataCaster caster) {
		List<Record> toReturn = new ArrayList<>();
		FSDataInputStream inputStream = null;
		FileReader<Record> fileReader = null;
		try {
			// Getting status
			FileStatus filestatus = hdfs.getFileStatus(path);
			inputStream = hdfs.open(path);
			AvroFSInput input = new AvroFSInput(inputStream, filestatus.getLen());
			GenericDatumReader<Record> avroReader = new GenericDatumReader<>();
			fileReader = DataFileReader.openReader(input, avroReader);
			while(fileReader.hasNext()) {
				toReturn.add(fileReader.next());
			}
		} catch (Exception ex) {
			logger.error("Unable to read file", ex);
		} finally {
			try {
				if(fileReader != null) {
					fileReader.close();
				}
				if(inputStream != null) {
					inputStream.close();
				}
			} catch (Exception ex) {
				// Nothing
			}
		}
		/*
		toReturn
			.parallelStream()
			.map(element -> readCast(element, T.))
			.toArray();
			*/
		return toReturn
			.parallelStream()
			//.stream()
			.map(element -> readCast(element, castTo, caster))
			.toArray();
		//return (T[]) toReturn.toArray();
	}
	
	public static Object readCast(Record element, Class objectType, DataCaster caster) {
		if (objectType == null) {
			return element;
		}
		try {
			Object toReturn = objectType.newInstance();
			Arrays.stream(objectType.getMethods())
				.filter(method -> method.getName().startsWith("set"))
				.forEach(method -> mInvokeSetter(toReturn, method, element, caster));
			return toReturn;
		} catch (InstantiationException ex) {
			logger.error(String.format("Unable to create [%s] object", objectType.getCanonicalName()), ex);
			throw new IllegalArgumentException("objectType");
		} catch (IllegalAccessException ex) {
			logger.error(String.format("Unable to create [%s] object", objectType.getCanonicalName()), ex);
			throw new IllegalArgumentException("objectType");
		} catch (Exception ex) {
			logger.error(String.format("Unable to invoke on [%s] object", objectType.getCanonicalName()), ex);
			throw new IllegalArgumentException("objectType");
		}
	}
	
	@SneakyThrows
	private static Object mInvokeSetter(Object obj, Method method, Record element, DataCaster caster) {
		char[] valueName = method.getName().substring(3).toCharArray();
		valueName[0] = Character.toLowerCase(valueName[0]);
		String finalValueName = new String(valueName);
		
		Object valueToApply = element.get(finalValueName);
		
		if(caster != null) {
			valueToApply = caster.apply(valueToApply, finalValueName, obj);
		}
		
		
		return method.invoke(obj, valueToApply);
	}
}
