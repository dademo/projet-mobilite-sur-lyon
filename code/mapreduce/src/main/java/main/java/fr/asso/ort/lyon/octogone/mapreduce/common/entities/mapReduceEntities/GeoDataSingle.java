package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;
import com.google.gson.Gson;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.JobHelper;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.locationtech.jts.geom.Geometry;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class GeoDataSingle<T> extends GeoData<T> implements WritableComparable<GeoDataSingle<T>> {

	@NotNull
	protected Geometry geometry;
	
	public GeoDataSingle(@NotNull T data, @NotNull SourceGeometryType sourceGeometryType, @NotNull Geometry geometry) {
		super(data, sourceGeometryType);
		this.geometry = geometry;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		Gson gson = JobHelper.getGson();
		out.writeUTF(gson.toJson(this));
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		Gson gson = JobHelper.getGson();
		GeoDataSingle<T> o = gson.fromJson(in.readUTF(), GeoDataSingle.class);
		this.setGeometry(o.getGeometry());
		this.setProperties(o.getProperties());
		this.setSourceGeometryType(o.getSourceGeometryType());
	}

	@Override
	public int compareTo(GeoDataSingle<T> o) {
		return 0;
	}
	
}
