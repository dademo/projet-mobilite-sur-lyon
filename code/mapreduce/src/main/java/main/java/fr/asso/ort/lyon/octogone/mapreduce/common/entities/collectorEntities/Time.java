package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities;

import org.springframework.data.mongodb.core.index.Indexed;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/* Angular Time class: https://angular.io/api/common/Time */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Time {
	private int hours;
	private int minutes;
}
