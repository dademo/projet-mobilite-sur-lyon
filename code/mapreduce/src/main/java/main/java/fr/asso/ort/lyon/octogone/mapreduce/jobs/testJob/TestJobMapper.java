package main.java.fr.asso.ort.lyon.octogone.mapreduce.jobs.testJob;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.geotools.geometry.jts.JTS;
import org.geotools.measure.Measure;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;

import lombok.Getter;
import lombok.Setter;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Meta;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoDataSingle;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.GeoJSONFeature;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.GeoJsonTools;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.JobHelper;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.helpers.MapperHelper;

public class TestJobMapper extends Mapper<Long, Object, Text, Text> {

	private static Logger logger = LoggerFactory.getLogger(TestJobMapper.class);
			
	@Getter
	@Setter
	private static List<GeoDataSingle<Object>> adrEcole = new ArrayList<>();
	
	@Getter
	private static List<Meta> metas;
	
	@Getter
	private static String metaListStr = null;
	
	private static String crsStr = null;
	private static CoordinateReferenceSystem coordinateReferenceSystem = null;
	private static boolean hasCrsWarned = false;
	
	public static void setMetas(List<Meta> metas) {
		TestJobMapper.metas = metas;
		TestJobMapper.metaListStr = String.join("-",
			metas.stream()
			.map(elem -> elem.getName())
			.toArray(String[]::new));
		// Geting crs
		if(!metas.isEmpty() &&
				metas.stream()
				.map(elem -> elem.getCrs())
				.allMatch(metas.get(0).getCrs()::equals)
			) {
			TestJobMapper.crsStr = metas.get(0).getCrs();
			logger.info(String.format("Got crs %s", TestJobMapper.crsStr));
		} else {
			logger.info("No crs or different crs found");
			TestJobMapper.crsStr = null;
		}
	}
	
	private Gson gson = JobHelper.getGson();
	
	
	@Override
	protected void map(Long key, Object value, Mapper<Long, Object, Text, Text>.Context context)
			throws IOException, InterruptedException {
		//HashMap<String, Object> toReturn = new HashMap<>();
		BasicDBObject toReturn = new BasicDBObject();
		
		GeoDataSingle geoValue = GeoJsonTools.fromSingleRecord((GeoJSONFeature)value);
		
		GeoDataSingle<Object> toAdd = TestJobMapper.adrEcole
				.stream()
				.min(Comparator.comparing(val -> geoValue.getGeometry().distance(val.getGeometry())))
				.orElse(null);
		
		if(toAdd != null) {
			/* Merging */
			// Getting the distance in meters
			// src: https://gis.stackexchange.com/questions/156465/calculate-distance-with-jts
			if(TestJobMapper.crsStr != null) {
				if(TestJobMapper.coordinateReferenceSystem == null && !TestJobMapper.hasCrsWarned) {
					try {
						TestJobMapper.coordinateReferenceSystem = CRS.decode(TestJobMapper.crsStr);
					} catch (Exception ex) {
						logger.error("Can't get crs", ex);
						logger.warn("No crs set. Will not get the distance");
						TestJobMapper.hasCrsWarned = true;
					}
				}
				if(TestJobMapper.coordinateReferenceSystem != null) {
					try {
						toReturn.append("distance_meter",
							JTS.orthodromicDistance(
								geoValue.getGeometry().getCoordinate(),
								toAdd.getGeometry().getCoordinate(),
								TestJobMapper.coordinateReferenceSystem
						));
					} catch (TransformException ex) {
						logger.error("Unable to get distance", ex);
					}
				}
			} else {
				if(!TestJobMapper.hasCrsWarned) {
					logger.warn("No crs set. Will not get the distance");
					TestJobMapper.hasCrsWarned = true;
				}
			}
			
			// toReturn.append("distance", geoValue.getGeometry().distance(toAdd.getGeometry()));
			MapperHelper.mergeIntoDBObject(toReturn, geoValue, "pvochantierperturbant");
			MapperHelper.mergeIntoDBObject(toReturn, toAdd, "adrecole");
			// MapperHelper.mergeToHashMap(toReturn, geoValue, "pvochantierperturbant");
			// MapperHelper.mergeToHashMap(toReturn, toAdd, "adrecole");
			
			if(toReturn != null) {
				context.write(new Text(String.format("%s_%d", TestJobMapper.metaListStr, key)), new Text(gson.toJson(toReturn)));
			}
		} else {
			logger.error(String.format("Unable to get a value for [%s] comparison", metas));
		}
	}
}
