package main.java.fr.asso.ort.lyon.octogone.mapreduce.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

/* https://fr.wikibooks.org/wiki/Programmation_Java/Annotations */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface MapReduceJob {
	
	@AliasFor(annotation = Component.class)
	String value() default "";
}
