package main.java.fr.asso.ort.lyon.octogone.mapreduce;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.BaseJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.MapReduceJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MapReduceJobRepository;

@Service
public class MapReduceRunner {

    private static Logger logger = LoggerFactory.getLogger(MapReduceRunner.class);

	@Autowired
	private MapReduceJobRepository mapReduceJobRepository;
	
	
	public int main(BaseJob job, String[] args) {
		
		int retCode = 0;

		LocalDateTime startDate = LocalDateTime.now();
		
		MapReduceJob jobLogger = new MapReduceJob();
		jobLogger.setJobName(job.getJobName());
		jobLogger.setTimestamp(startDate);
		
		try {
			job.main(args);
			jobLogger.setStatus("ok");
		} catch (Exception ex) {
			logger.error(String.format("Sub job returned an error (%s)", job.getClass().getSimpleName()), ex);
			jobLogger.setStatus("ko");
			jobLogger.setErrorType(ex.getClass().getCanonicalName());
			// https://stackoverflow.com/questions/1149703/how-can-i-convert-a-stack-trace-to-a-string
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			jobLogger.setErrorDescription(sw.toString());
			retCode = 1;
		}
		
		LocalDateTime endDate = LocalDateTime.now();
		jobLogger.setBatchDurationSeconds(endDate.until(startDate, ChronoUnit.SECONDS));
		// Pushing stats
		mapReduceJobRepository.save(jobLogger);
		return retCode;
	}
}
