package main.java.fr.asso.ort.lyon.octogone.mapreduce.services;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.BaseJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities.Meta;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities.MapReduceJob;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MapReduceJobRepository;
import main.java.fr.asso.ort.lyon.octogone.mapreduce.common.repositories.MetaRepository;

@Component
public class HdfsMetaDataFinder {

    private static Logger logger = LoggerFactory.getLogger(HdfsMetaDataFinder.class);

	@Autowired
	private MetaRepository metaRepository;
	
	@Autowired
	private MapReduceJobRepository mapReduceJobRepository;
	
	@Autowired
	private HdfsDataFinder hdfsDataFinder;
	

	public Path findLatestByMeta(Meta meta) {
		return hdfsDataFinder.getLatestFile(meta);
	}
	
	public List<Path> findByJobAndMetaSinceLastJob(BaseJob job, Meta meta) {
		return findByJobNameAndMetaSinceLastJob(job.getJobName(), meta);
	}
	
	public List<Path> findByJobNameAndMetaSinceLastJob(String jobName, Meta meta) {
		MapReduceJob job = mapReduceJobRepository.findFirstByJobNameByOrderByTimestampDesc(jobName);
		if(job == null) {
			// Assuming the job has never runned
			LocalDateTime date = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.MAX);
			return hdfsDataFinder.getDataSince(date, meta);
		} else {
			return hdfsDataFinder.getDataSince(job.getTimestamp(), meta);
		}
	}
}
