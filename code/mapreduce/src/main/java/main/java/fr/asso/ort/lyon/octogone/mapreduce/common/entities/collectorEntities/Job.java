package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

//import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta_jobs")
public class Job {

	private String _id;
	@Indexed(unique = true)
	private String jobName;
	private String meta;
	private LocalDateTime timestamp;
	private Long batchDurationSeconds;
	private String module;
	private Long extractedDataSize;
	private Long generatedDataSize;
	private String status;
	private String errorType;
	private String errorDescription;
}
