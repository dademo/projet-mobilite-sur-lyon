package main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter;

import java.time.LocalDateTime;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import lombok.Value;

@Value
public class MongoData<T> {
	private LocalDateTime timestamp;
	private T data;
	
	public MongoData(T data) {
		this.data = data;
		this.timestamp = LocalDateTime.now();
	}
	
	public DBObject toDBObject() {
		DBObject toReturn = new BasicDBObject();
		toReturn.put("timestamp", this.getTimestamp());
		toReturn.put("data", this.getData());
		return toReturn;
	}
}
