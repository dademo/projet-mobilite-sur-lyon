package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SourceGeometryType {
	Point("Point"),
	LineString("LineString"),
	Polygon("Polygon"),
	MultiPoint("MultiPoint"),
	MultiLineString("MultiLineString"),
	MultiPolygon("MultiPolygon");
	
	private String value;
}
