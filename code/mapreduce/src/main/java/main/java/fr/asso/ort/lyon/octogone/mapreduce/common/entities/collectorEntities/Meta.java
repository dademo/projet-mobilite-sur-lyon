package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.collectorEntities;

import org.springframework.data.mongodb.core.mapping.Document;

import com.google.gson.JsonObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "meta")
public class Meta {

	private String _id;
	private String name;
	private String fullname;
    private ServiceType service_type;
    private String url;
    private String crs;
    private Time collect_interval;
    private String module;
    private String data_source;
    private String data_source_ref;
    private String privacy;
    private String license;
    /* https://www.baeldung.com/jackson-mapping-dynamic-object */
    private Map<String, Object> schema;
}
