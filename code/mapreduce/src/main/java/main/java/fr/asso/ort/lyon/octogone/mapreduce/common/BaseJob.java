package main.java.fr.asso.ort.lyon.octogone.mapreduce.common;

import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import com.sun.istack.NotNull;

import main.java.fr.asso.ort.lyon.octogone.mapreduce.readerwriter.MongoDataWriter;

public interface BaseJob {
	
	public @NotNull String getJobName();
	
	@SuppressWarnings("rawtypes")
	public @NotNull Class<? extends Mapper> getMapperClass();

	@SuppressWarnings("rawtypes")
	public @NotNull Class<? extends Reducer> getReducerClass();
	
	public @NotNull Class<? extends Object> getMongoOutputType();
	
	public @NotNull MongoDataWriter.DBObjectCaster getMongoCaster();
	
	public @NotNull List<Path> getAvroFilePath();
	
	public @NotNull String getOutpuCollectionName();
	
	public void main(String[] args);
}
