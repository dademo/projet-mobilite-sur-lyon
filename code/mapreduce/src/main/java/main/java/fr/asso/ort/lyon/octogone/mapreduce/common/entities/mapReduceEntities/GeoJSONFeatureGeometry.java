package main.java.fr.asso.ort.lyon.octogone.mapreduce.common.entities.mapReduceEntities;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GeoJSONFeatureGeometry {
	
	SourceGeometryType type;
	Object coordinates;
	
	public static Object geometryCaster(Object toCast, @NotNull String fieldName, @NotNull Object castedObject) {
		if ("type".equals(fieldName)) {
			return SourceGeometryType.valueOf(toCast.toString());
		} else if (toCast instanceof org.apache.avro.util.Utf8) {
			return toCast.toString();
		} else  {
			return toCast;
		}
	}
}
