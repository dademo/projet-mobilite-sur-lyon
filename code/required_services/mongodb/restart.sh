#!/bin/sh

_dir=`dirname $0`

podman-compose -f "$_dir/docker-compose.yml" down
podman-compose -f "$_dir/docker-compose.yml" up -d
