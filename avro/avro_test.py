#!/bin/env python3

import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter

import requests


__url__ = r'https://download.data.grandlyon.com/wfs/grandlyon?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=pvo_patrimoine_voirie.pvocomptagecriter&SRSNAME=EPSG:4171&outputFormat=application/json;%20subtype=geojson&startIndex=0'
#__url__ = r'https://download.data.grandlyon.com/wfs/grandlyon?SERVICE=WFS&VERSION=2.0.0&request=GetFeature&typename=pvo_patrimoine_voirie.pvocomptagecriter&outputFormat=application/json;%20subtype=geojson&SRSNAME=EPSG:4171&startIndex=6&count=2'

if __name__ == '__main__':
    schema = avro.schema.Parse(open("pvo_patrimoine_voirie.pvocomptagecriter.avsc", "rb").read())

    writer = DataFileWriter(open("pvo_patrimoine_voirie.pvocomptagecriter.avro", "wb"), DatumWriter(), schema)
    writer.append(requests.get(__url__).json())
    writer.close()

    reader = DataFileReader(open("pvo_patrimoine_voirie.pvocomptagecriter.avro", "rb"), DatumReader())
    for data in reader:
        print("-- %s" % data)
    reader.close()